import ruLocale from './locales/ru.json';

const config = {
  buildModules: ['@nuxt/typescript-build', '@nuxtjs/style-resources'],
  css: ['~/assets/style/main.scss'],
  styleResources: {
    scss: ['~/assets/style/variables/variables-main.scss'],
  },
  env: {
    ...{
      API_URL: 'http://api.smth.com/api',
      PROXY_TO_URL: 'http://localhost:2000/',
      PROXY_API_PREFIX: '/api/',
      ENABLE_PROXY: 'true',
    },
  },
  head: {
    title: '🍭 nuxt-typescript-template',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'A boilerplate to start a Nuxt+TS project quickly',
      },
    ],
    link: [],
  },
  loading: { color: '#0c64c1' },
  modules: ['nuxt-i18n', '@nuxtjs/svg-sprite'],
  plugins: [
    { src: '~/plugins/WidthWatcher', ssr: true },
    { src: '~/plugins/RouterHelper', ssr: true },
    { src: '~/plugins/Pluralization', ssr: true },
    { src: '~/plugins/ScrollLock', ssr: true },
    { src: '~/plugins/VueUse', ssr: true },
  ],
  i18n: {
    strategy: 'no_prefix',
    locales: ['ru'],
    defaultLocale: 'ru',
    vueI18n: {
      messages: {
        ru: { ...ruLocale },
      },
    },
  },
  svgSprite: {
    input: '~/assets/svg/sprite/icons',
    output: '~/assets/svg/sprite/output',
  },
  storybook: {
    // webpackFinal(config) {
    //   console.log('wpf');
    //   config.resolve.extensions.push('.ts', '.tsx', '.vue', '.css', '.less', '.scss', '.sass', '.html');
    //   config.module.rules.push(
    //     {
    //       test: /\.tsx?$/,
    //       exclude: /node_modules/,
    //       use: [
    //         'babel-loader',
    //         {
    //           loader: 'ts-loader',
    //           options: {
    //             transpileOnly: true,
    //             appendTsSuffixTo: [/\.vue$/],
    //             appendTsxSuffixTo: [/\.vue$/],
    //           },
    //         },
    //       ],
    //     },
    //     {
    //       test: /\.vue$/,
    //       use: ['vue-loader'],
    //     },
    //   );
    //
    //   config.module.rules.push({
    //     test: /\.scss$/,
    //     use: [
    //       {
    //         loader: 'sass-resources-loader',
    //         options: {
    //           // Provide path to the file with resources
    //           resources: 'assets/style/variables/variables-main.scss',
    //         },
    //       },
    //     ],
    //     include: path.resolve(__dirname, '../'),
    //   });
    //   return config;
    // },
    stories: ['~/components/**/*.stories.tsx'],
    addons: ['@storybook/addon-knobs', '@storybook/addon-actions'],
  },
  typescript: {
    typeCheck: {
      eslint: { files: './**/*.{ts,tsx,js,jsx}' },
    },
  },
};

export default config;
