import { DeclareOnEvents } from 'vue-tsx-support';
import Vue from 'Vue';
import { ComponentOptions } from 'vue';

export type EmptyUnion<Context> = Exclude<Context, Context>;

export type WProps<
  Context,
  OptionalProps extends keyof Context = EmptyUnion<Context>,
  ExcludedFromProps extends keyof Context = EmptyUnion<Context>
> = {
  props: Omit<
    Omit<Context, OptionalProps> & Partial<Pick<Context, OptionalProps>>,
    ExcludedFromProps | '_tsx' | keyof ComponentOptions<Vue> | keyof Vue
  >;
};

export type WEvents<Events> = DeclareOnEvents<Events>;
