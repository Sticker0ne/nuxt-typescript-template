/* eslint-disable */

declare class BadgerAccordion {
  // @ts-ignore
  constructor(element: string | Element, options?: Partial<IAccordionSettings>) {}

  init(): void;
  getState(indexes: number[]): [{ state: AccordionPanelStates }];
  open(index: number): void;
  close(index: number): void;
  togglePanel(animationAction: AccordionAnimationActions, index: number): void;
  openAll(): void;
  closeAll(): void;
  calculatePanelHeight(panel: HTMLElement): string;
  calculateAllPanelsHeight(): void;

  panels?: HTMLElement[];
  headers?: HTMLElement[];
  container?: HTMLElement;

  settings: IAccordionSettings;
}

declare module 'badger-accordion' {
  export = BadgerAccordion;
}

declare interface IAccordionSettings {
  headerClass: string;
  panelClass: string;
  panelInnerClass: string;
  hiddenClass: string;
  initializedClass: string;
  headerDataAttr: string;
  openMultiplePanels: boolean;
  roles: boolean | object;
  addListenersOnInit: boolean;
}

declare enum AccordionPanelStates {
  open = 'open',
  closed = 'closed',
}

declare enum AccordionAnimationActions {
  open = 'open',
  close = 'close',
}
