export interface IElemClasses {
  class?: Record<string, boolean>;
}

export type IElemStyles = Record<string, string | number>;
