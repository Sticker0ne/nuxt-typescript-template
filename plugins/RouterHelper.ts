import { Vue as _Vue } from 'nuxt-property-decorator';
import { Plugin } from '@nuxt/types';
import { DevelopOnlyRouteNames, RouteNames } from '~/router/router';

export interface IRouterHelper {
  routeNames: typeof RouteNames;
  developOnlyRouteNames: typeof DevelopOnlyRouteNames;
  showDevelopOnlyRoutes: boolean;
}

declare module 'vue/types/vue' {
  interface Vue {
    $rh: IRouterHelper;
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $rh: IRouterHelper;
  }
}

declare module 'vuex/types/index' {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Store<S> {
    $rh: IRouterHelper;
  }
}

const routerHelperPlugin = new _Vue({
  data: { routeNames: RouteNames, developOnlyRouteNames: DevelopOnlyRouteNames },
  computed: {
    showDevelopOnlyRoutes(): boolean {
      return process.env.NODE_ENV === 'development';
    },
  },
});

/* eslint-disable */
const routerHelper: Plugin = (context, inject) => {
  inject('rh', routerHelperPlugin as IRouterHelper);
  ((context as unknown) as { $rh: IRouterHelper }).$rh = routerHelperPlugin;
};

export default routerHelper;
