import { Vue as _Vue } from 'nuxt-property-decorator';
import { Plugin } from '@nuxt/types';
import { hasKey } from '~/utils/CommonUtils';

export interface IWidthWatcher {
  actualWidth: number;
  bpWidth: number;

  clientSide: boolean;
  serverSide: boolean;

  sm: boolean;
  md: boolean;
  lg: boolean;
  xl: boolean;
  xxl: boolean;

  mdAndUp: boolean;
  lgAndUp: boolean;
  xlAndUp: boolean;

  mdAndDown: boolean;
  lgAndDown: boolean;
  xlAndDown: boolean;
}

declare module 'vue/types/vue' {
  interface Vue {
    $ww: IWidthWatcher;
    $startWatchWidth(): void;
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $ww: IWidthWatcher;
    $startWatchWidth(): void;
  }

  interface Context {
    $ww: IWidthWatcher;
  }
}

declare module 'vuex/types/index' {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Store<S> {
    $ww: IWidthWatcher;
    $startWatchWidth(): void;
  }
}

const breakpoints = {
  sm: { start: 320, end: 767 },
  md: { start: 768, end: 1023 },
  lg: { start: 1024, end: 1279 },
  xl: { start: 1280, end: 1439 },
  xxl: { start: 1440, end: 999999 },
};

const breakpointsArray: { start: number; end: number }[] = [];
Object.keys(breakpoints).forEach(key => {
  if (hasKey(breakpoints, key)) breakpointsArray.push(breakpoints[key]);
});

const widthWatcherPlugin = new _Vue({
  data: { actualWidth: 0, bpWidth: 0, clientSide: false, serverSide: true },
  computed: {
    sm(): boolean {
      return this.bpWidth <= breakpoints.sm.end;
    },
    md(): boolean {
      return this.bpWidth >= breakpoints.md.start && this.bpWidth <= breakpoints.md.end;
    },
    lg(): boolean {
      return this.bpWidth >= breakpoints.lg.start && this.bpWidth <= breakpoints.lg.end;
    },
    xl(): boolean {
      return this.bpWidth >= breakpoints.xl.start && this.bpWidth <= breakpoints.xl.end;
    },
    xxl(): boolean {
      return this.bpWidth >= breakpoints.xxl.start;
    },

    mdAndUp(): boolean {
      return this.bpWidth >= breakpoints.md.start;
    },
    lgAndUp(): boolean {
      return this.bpWidth >= breakpoints.lg.start;
    },
    xlAndUp(): boolean {
      return this.bpWidth >= breakpoints.xl.start;
    },

    mdAndDown(): boolean {
      return this.bpWidth <= breakpoints.md.end;
    },
    lgAndDown(): boolean {
      return this.bpWidth <= breakpoints.lg.end;
    },
    xlAndDown(): boolean {
      return this.bpWidth <= breakpoints.xl.end;
    },
  },
});

function startWatchWidth(): void {
  window.addEventListener('resize', updateActualWidth);
  createAndSubscribeMediaQueryLists();

  setTimeout(() => {
    updateActualWidth();
    updateBpWidth();
    changeSide('client');
  });
}

function createAndSubscribeMediaQueryLists(): void {
  const minQueryLists = breakpointsArray.map(bp => window.matchMedia(`(min-width: ${bp.start}px)`));
  const maxQueryLists = breakpointsArray.map(bp => window.matchMedia(`(max-width: ${bp.start}px)`));
  [...minQueryLists, ...maxQueryLists].forEach(ql => {
    ql.addEventListener ? ql.addEventListener('change', updateBpWidth) : ql.addListener(updateBpWidth);
  });
}

function updateActualWidth(): void {
  widthWatcherPlugin.actualWidth = window.innerWidth;
}

function updateBpWidth(): void {
  widthWatcherPlugin.bpWidth = window.innerWidth;
}

function changeSide(side: string): void {
  if (side === 'client') {
    widthWatcherPlugin.clientSide = true;
    widthWatcherPlugin.serverSide = false;
  } else if (side === 'server') {
    widthWatcherPlugin.clientSide = false;
    widthWatcherPlugin.serverSide = true;
  }
}

/* eslint-disable */
const widthWatcher: Plugin = (context, inject) => {
  inject('ww', widthWatcherPlugin as IWidthWatcher);
  inject('startWatchWidth', startWatchWidth);
  ((context as unknown) as { $bp: IWidthWatcher }).$bp = widthWatcherPlugin;
};

export default widthWatcher;
