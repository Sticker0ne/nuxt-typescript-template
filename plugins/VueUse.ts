import { Vue } from 'nuxt-property-decorator';
import VueTheMask from 'vue-the-mask';
import vClickOutside from 'v-click-outside';
import { Plugin } from 'vue-fragment';

Vue.use(VueTheMask);
Vue.use(vClickOutside);
Vue.use(Plugin);
