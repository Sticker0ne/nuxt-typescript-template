import { Vue } from 'nuxt-property-decorator';
import { Plugin } from '@nuxt/types';

export interface IScrollLock {
  lockScroll: () => void;
  unlockScroll: () => void;
}

declare module 'vue/types/vue' {
  interface Vue {
    $sl: IScrollLock;
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $sl: IScrollLock;
  }
}

declare module 'vuex/types/index' {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Store<S> {
    $sl: IScrollLock;
  }
}

const scrollLockPlugin = new Vue({
  data(): Record<string, any> {
    return {
      scrollPosition: 0,
      locked: false,
    };
  },
  computed: {},
  methods: {
    lockScroll(): void {
      if (!process.client) throw new Error('ScrollLock plugin used on server side');

      if (this.locked) return;
      this.locked = true;

      this.scrollPosition = window.pageYOffset;
      document.body.style.overflow = 'hidden';
      document.body.style.position = 'fixed';
      document.body.style.top = `-${this.scrollPosition}px`;
      document.body.style.width = '100%';
    },
    unlockScroll(): void {
      if (!process.client) throw new Error('ScrollLock plugin used on server side');

      if (!this.locked) return;
      this.locked = false;

      document.body.style.removeProperty('overflow');
      document.body.style.removeProperty('position');
      document.body.style.removeProperty('top');
      document.body.style.removeProperty('width');
      window.scrollTo(0, this.scrollPosition);
    },
  },
});

/* eslint-disable */
const scrollLock: Plugin = (context, inject) => {
  inject('sl', scrollLockPlugin as IScrollLock);
  ((context as unknown) as { $sl: IScrollLock }).$sl = scrollLockPlugin;
};

export default scrollLock;
