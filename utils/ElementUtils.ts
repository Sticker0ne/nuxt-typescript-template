const focusableElementsString =
  'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]';

export function getFocusableElementsInside(element: Element): HTMLElement[] {
  const focusableElements: NodeListOf<HTMLElement> = element.querySelectorAll(focusableElementsString);
  const focusableElementsArray: HTMLElement[] = Array.prototype.slice.call(focusableElements);

  return focusableElementsArray.length ? focusableElementsArray : [];
}

export function catchTabInsideElement(element: HTMLElement): boolean {
  if (process.server) throw new Error('catchTabInModel might be called only on client side');

  element.addEventListener('keydown', trapTabKey);

  const focusableElementsArray: HTMLElement[] = getFocusableElementsInside(element);
  if (!focusableElementsArray.length) return false;

  const firstTabStop = focusableElementsArray[0];
  const lastTabStop = focusableElementsArray[focusableElementsArray.length - 1];

  // Focus first child
  firstTabStop.focus();

  function trapTabKey(event: KeyboardEvent): void {
    // Check for TAB key press
    if (event.code.toLowerCase() !== 'tab') return;
    if (event.shiftKey) {
      // SHIFT + TAB
      if (document.activeElement !== firstTabStop) return;
      event.preventDefault();
      lastTabStop.focus();
    } else if (document.activeElement === lastTabStop) {
      // JUST TAB
      event.preventDefault();
      firstTabStop.focus();
    }
  }

  return true;
}
