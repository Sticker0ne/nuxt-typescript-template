import type { IziToast } from 'izitoast';
import { nanoid } from 'nanoid';

let iziToastInstance: IziToast;

async function getIziToastInstanse(): Promise<IziToast> {
  if (!process.client) throw new Error('Toast cant be called on server side');

  if (iziToastInstance) return iziToastInstance;
  iziToastInstance = await import('izitoast').then(module => module.default);
  return iziToastInstance;
}

const defaultToastOptions = {
  animateInside: false,
  drag: false,
  close: false,
  closeOnClick: true,
  timeout: false,
  progressBar: false,
};

const TOAST_ID_PREFIX = 'toast-id-';

interface IShowToastOptions {
  message: string;
  timeout?: number;
  id?: string;
}

interface IShowPendingToastOptions extends IShowToastOptions {
  id: string;
}

interface IShowRejectedImportToastOptions extends IShowToastOptions {
  url: string;
  title: string;
}

export class ToastUtils {
  public static async showDefaultToast(options: IShowToastOptions): Promise<void> {
    const iziToast = await getIziToastInstanse();

    const id = options.id || nanoid();

    iziToast.show({
      ...defaultToastOptions,
      id: `${TOAST_ID_PREFIX}${id}`,
      message: options.message,
      timeout: options.timeout || 0,
      class: 'default-toast',
    });
  }

  public static async showErrorToast(options: IShowToastOptions): Promise<void> {
    const iziToast = await getIziToastInstanse();

    const id = options.id || nanoid();

    iziToast.show({
      ...defaultToastOptions,
      id: `${TOAST_ID_PREFIX}${id}`,
      message: options.message,
      timeout: options.timeout || 0,
      class: 'error-toast',
    });
  }

  public static async showPendingToast(options: IShowPendingToastOptions): Promise<void> {
    const iziToast = await getIziToastInstanse();

    const loader =
      '<div class="loader-wrap"> <div class=" small small__points-white loader`"> <div class="points" /> </div> </div>';

    iziToast.show({
      ...defaultToastOptions,
      id: `${TOAST_ID_PREFIX}${options.id}`,
      message: `${options.message} ${loader}`,
      timeout: options.timeout || 0,
      class: 'pending-toast',
      closeOnClick: false,
    });
  }

  public static async showRejectedImportToast(options: IShowRejectedImportToastOptions): Promise<void> {
    const iziToast = await getIziToastInstanse();

    const id = options.id || nanoid();

    iziToast.show({
      ...defaultToastOptions,
      id: `${TOAST_ID_PREFIX}${id}`,
      title: options.title,
      message: options.message,
      timeout: options.timeout || 0,
      class: 'rejected-toast',
      closeOnClick: false,
      buttons: [
        [
          `<a href="#"  class="button button_big">Скачать</a>`,
          function (instance, toast): void {
            window.open(options.url, '_blank');
            instance.hide({}, toast);
          },
          false,
        ],
        [
          '<button class=" button button_big-simple ">Закрыть</button>',
          (instance, toast): void => {
            instance.hide({}, toast);
          },
          false,
        ],
      ],
    });
  }

  public static async hideToastById(id: string): Promise<void> {
    const iziToast = await getIziToastInstanse();

    const toast = document.getElementById(`toast-id-${id}`);
    if (toast) iziToast.hide({}, toast as HTMLDivElement);
  }
}
