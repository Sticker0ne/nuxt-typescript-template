import fs from 'fs';
import path from 'path';
import { AxiosOptions } from '@nuxtjs/axios';

export function buildAxiosConfig(processEnv: Record<string, string>): AxiosOptions {
  const enableProxy = (JSON.parse(processEnv.ENABLE_PROXY) as boolean) || false;

  const result = enableProxy
    ? { proxy: true, prefix: processEnv.PROXY_API_PREFIX }
    : { baseURL: processEnv.API_URL || 'http://localhost:3000' };

  /* eslint-disable-next-line */
  console.info('axios config is', JSON.stringify(result, null, 4));

  return result;
}

export function buildProxyConfig(processEnv: Record<string, string>): Record<string, string> {
  const enableProxy = (JSON.parse(processEnv.ENABLE_PROXY) as boolean) || false;

  const result = enableProxy ? { [processEnv.PROXY_API_PREFIX]: processEnv.PROXY_TO_URL } : {};

  /* eslint-disable-next-line */
  console.info('proxy config is', JSON.stringify(result, null, 4));

  return result;
}

export function createTestPageIfNecessary(): void {
  const testPageAlreadyExist = fs.existsSync(path.resolve('pages/TestPage/TestPage.tsx'));
  if (testPageAlreadyExist) return;

  if (!fs.existsSync('pages/TestPage')) fs.mkdirSync('pages/TestPage');

  fs.appendFileSync(
    'pages/TestPage/TestPage.tsx',
    `import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
@Component({})
export default class TestPage extends Vue {
  private render(): VNode {
    return <div class="TestPageWrapper">Test page</div>;
  }
}
`,
  );
}
