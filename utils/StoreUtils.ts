import * as t from 'io-ts';
import { fold } from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import prettyReporter from 'io-ts-reporters';

export function optional<RT extends t.Any>(
  type: RT,
  name = `${type.name} | undefined`,
): t.UnionType<
  [RT, t.UndefinedType],
  t.TypeOf<RT> | undefined,
  t.OutputOf<RT> | undefined,
  t.InputOf<RT> | undefined
> {
  return t.union<[RT, t.UndefinedType]>([type, t.undefined], name);
}

// Apply a validator and get the result in a `Promise`
export function decodeToPromise<T, O, I>(
  validator: t.Type<T, O, I>,
  input: I,
  onErrorMessage?: string,
): Promise<T> {
  const result = validator.decode(input);
  return pipe(
    result,
    fold(
      () => {
        const messages = prettyReporter.report(result);
        const errorString = `${
          onErrorMessage ? 'Extra error message: ' + onErrorMessage + '.' : ''
        } Validation error at ${validator.name}.\n  ${messages.join('\n')}`;
        return Promise.reject(new Error(errorString));
      },
      value => Promise.resolve(value),
    ),
  );
}

/**
 * Проверяет является ли enum с цифровыми значениями или со строками.
 * @param e enum который нужно проверить
 */
function isConstEnum(e: any): boolean {
  const values = Object.values(e);
  const firstType = typeof values[0];
  let result = false;

  values.forEach(value => {
    if (typeof value !== firstType) result = true;
  });

  return result;
}

/**
 * Приведит enum к массиву строк или цифр. Если enum числовой - то строки, иначе цифры
 * @param e enum который нужно привести к массиву
 */
function enumToArray<T>(e: T): [T[keyof T]] {
  if (isConstEnum(e)) {
    const keys = Object.keys(e).filter(k => typeof (e as any)[k as any] === 'number');
    const values = keys.map(k => (e as any)[k as any]);
    return values as any;
  }

  return Object.values(e) as any;
}

type IEnum = {
  [key in string | number]: string | number;
};

export function validatorFromEnum<T extends IEnum>(e: T, name?: string): t.Type<T[keyof T]> {
  const arr = enumToArray(e);
  if (arr.length < 2) throw new Error('Enum min length is 2');

  if (typeof arr[0] === 'string') {
    const reducedArr = arr.reduce((acc, value: string | number) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      acc[value] = null;
      return acc;
    }, {});

    return t.keyof(reducedArr, name) as any;
  }

  const arrayOfLiteral: t.LiteralC<T[keyof T]>[] = [];
  arr.forEach((value, index) => {
    arrayOfLiteral[index] = t.literal(value);
  });

  const union = t.union(
    arrayOfLiteral as [t.LiteralC<T[keyof T]>, t.LiteralC<T[keyof T]>, ...t.LiteralC<T[keyof T]>[]],
    name,
  );

  return union;
}
