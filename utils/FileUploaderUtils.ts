import {
  IFileUploaderFileResponse,
  IFileUploaderNormalizedFile,
  IDropzoneFileExtended,
  FileUploaderFileExtensions,
} from '~/components/common/fileUploader/FileUploader/FileUploader.types';
import {
  FileTemplateStatuses,
  FileTemplateTypes,
} from '~/components/common/fileUploader/FileTemplate/FileTemplate.types';
import { addFirstSlashSafe, getApiUrlByProxyStatus, isAbsoluteUrl } from '~/utils/URLUtils';

export function convertDropzoneFileToFile(dzFile: IDropzoneFileExtended): IFileUploaderNormalizedFile {
  const { name, size, status, dzCustomId } = dzFile;
  const progressRaw = dzFile?.upload?.progress || 0;

  const parsedType = getParsedFileType(name);
  const parsedStatus = getParsedFileStatus(status);
  const progress = Number.parseInt((progressRaw as unknown) as string);

  const result = { name, size, type: parsedType, progress, status: parsedStatus, dzCustomId };

  if (!dzFile.xhr?.responseText) return result;
  let parsedResponse: IFileUploaderFileResponse;

  try {
    parsedResponse = JSON.parse(dzFile.xhr?.responseText) as IFileUploaderFileResponse;
    return { ...result, url: parsedResponse.url, id: parsedResponse.id };
  } catch {
    // eslint-disable-next-line no-console
    console.error('response on upload file is not in json');
  }

  return result;
}

function getParsedFileType(type: string): FileTemplateTypes {
  if (type.match(/(.png)$/)) return FileTemplateTypes.png;
  if (type.match(/(.jp(e)?g)$/)) return FileTemplateTypes.jpg;
  if (type.match(/(.xls(x)?)$/)) return FileTemplateTypes.xlsx;
  if (type.match(/(.doc(x))$/)) return FileTemplateTypes.doc;
  if (type.match(/(.txt)$/)) return FileTemplateTypes.txt;
  if (type.match(/(.pdf)$/)) return FileTemplateTypes.pdf;

  return FileTemplateTypes.unknown;
}

function getParsedFileStatus(status: string): FileTemplateStatuses {
  if (status.match(/^(success)$/)) return FileTemplateStatuses.success;
  if (status.match(/^(error)$/)) return FileTemplateStatuses.error;

  return FileTemplateStatuses.unknown;
}

export function buildAcceptedFilesString(acceptedFiles: FileUploaderFileExtensions[]): string {
  let result = '';

  acceptedFiles.forEach(extension => {
    if (!result) result += `.${extension}`;
    else result += `, .${extension}`;
  });

  return result;
}

export function buildUploadUrl(url: string): string {
  if (isAbsoluteUrl(url)) return url;
  return `${getApiUrlByProxyStatus()}${addFirstSlashSafe(url)}`;
}

export function buildReadableSizeFromBytes(bytes: number): string {
  const i = Math.floor(Math.log(bytes) / Math.log(1024));
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const calculatedBytes = bytes / Math.pow(1024, i);
  const resultString = calculatedBytes.toFixed(2);

  return resultString.replace(/(.00)$/, '') + ' ' + sizes[i];
}
