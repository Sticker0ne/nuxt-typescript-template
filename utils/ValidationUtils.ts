interface IErrorItem {
  error: string;
  show: boolean;
}
export function buildErrorList(items: IErrorItem[]): string[] {
  return items.reduce((acc: string[], item) => {
    if (item.show) acc.push(item.error);
    return acc;
  }, []);
}

/* Strings */
export function stringIsNotEmpty(value: string): boolean {
  return !!value.length;
}

export function stringMinLength(value: string, minLength: number): boolean {
  return value.length >= minLength;
}

export function stringMaxLength(value: string, maxLength: number): boolean {
  return value.length <= maxLength;
}

export function stringLengthIsBetween(value: string, minLength: number, maxLength: number): boolean {
  return value.length <= maxLength && value.length >= minLength;
}

/* Number */
export function numberMinValue(value: number, minValue: number): boolean {
  return value >= minValue;
}

export function numberMaxValue(value: number, maxValue: number): boolean {
  return value <= maxValue;
}

export function numberIsBetween(value: number, minValue: number, maxValue: number): boolean {
  return value <= maxValue && value >= minValue;
}
