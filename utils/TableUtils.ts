import {
  IBaseTableColumnSettings,
  IBaseTableItem,
} from '~/components/common/table/BaseTable/BaseTable.types';
import { jsonClone } from '~/utils/CommonUtils';

export class TableDataWrapper {
  public columnSettings: IBaseTableColumnSettings[];
  public items: IBaseTableItem[];
  public initItems: IBaseTableItem[];
  public itemUniqueKey: string;

  public columnNamesInEditMode: string[] = [];
  public columnNamesExpanded: string[] = [];

  public onBlurMiddleware:
    | ((
        newItems: IBaseTableItem[],
        oldItems: IBaseTableItem[],
        columnSettings: IBaseTableColumnSettings[],
        itemUniqueKey: string,
      ) => IBaseTableItem[])
    | null = null;

  public constructor(
    columnSettings: IBaseTableColumnSettings[],
    items: IBaseTableItem[],
    itemUniqueKey = 'id',
  ) {
    this.columnSettings = jsonClone(columnSettings);
    this.items = jsonClone(items);
    this.initItems = jsonClone(items);
    this.itemUniqueKey = itemUniqueKey;
  }

  public toggleEditColumn = (columnName: string): void => {
    if (this.columnNamesInEditMode.includes(columnName))
      this.columnNamesInEditMode = this.columnNamesInEditMode.filter(name => name !== columnName);
    else this.columnNamesInEditMode = [...this.columnNamesInEditMode, columnName];
  };

  public toggleExpandColumn = (columnName: string): void => {
    if (this.columnNamesExpanded.includes(columnName))
      this.columnNamesExpanded = this.columnNamesExpanded.filter(name => name !== columnName);
    else this.columnNamesExpanded = [...this.columnNamesExpanded, columnName];
  };

  public onTableItemsChange = (newItems: IBaseTableItem[]): void => {
    this.items = newItems;
  };

  public onTableBlur = (): void => {
    if (this.onBlurMiddleware)
      this.items = this.onBlurMiddleware(this.items, this.initItems, this.columnSettings, this.itemUniqueKey);
  };
}

export function forbidZeroBlurMiddleware(
  items: IBaseTableItem[],
  oldItems: IBaseTableItem[],
  columnsSettings: IBaseTableColumnSettings[],
  itemUniqueKey: string,
): IBaseTableItem[] {
  return items.map(item => {
    const clonedItem = jsonClone(item);
    columnsSettings.forEach(columnSettings => {
      if (columnSettings.forbidZero && clonedItem[columnSettings.key] === 0) {
        const oldItem = oldItems.find(oldItem => oldItem[itemUniqueKey] === item[itemUniqueKey]);
        if (!oldItem) return clonedItem;
        clonedItem[columnSettings.key] = oldItem[columnSettings.key];
      }
    });
    return clonedItem;
  });
}
