export function removeFirstSlash(str: string): string {
  if (str[0] === '/') return str.replace('/', '');
  return str;
}

export function addFirstSlashSafe(str: string): string {
  if (str[0] === '/') return str;
  return `/${str}`;
}

export function removeSlashAtLastIndex(str: string): string {
  if (str[str.length - 1] === '/') return str.slice(0, str.length - 1);
  return str;
}

export function isAbsoluteUrl(url: string): boolean {
  return !!url.match(/^(http(s)?):\/\//);
}

export function getApiUrlByProxyStatus(): string {
  const enableProxy = JSON.parse(process.env.ENABLE_PROXY || 'false');
  if (!enableProxy) return process.env.API_URL || '';

  const resultRaw = `${removeSlashAtLastIndex(process.env.PROXY_TO_URL || '')}${addFirstSlashSafe(
    process.env.PROXY_API_PREFIX || '',
  )}`;

  return removeSlashAtLastIndex(resultRaw);
}
