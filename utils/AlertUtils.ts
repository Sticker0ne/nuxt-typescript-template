import type { SweetAlertResult } from 'sweetalert2';

export interface IShowAlertOptions {
  title: string;
  confirmButtonTitle: string;
  cancelButtonTitle?: string;
  html?: string;
}

const defaultAlertOptions = {
  showCloseButton: true,
  focusConfirm: false,
  allowOutsideClick: false,
  allowEscapeKey: false,
  buttonsStyling: false,
  reverseButtons: true,
  customClass: {
    confirmButton: 'button button_big',
    cancelButton: 'button button_big-simple',
  },
};

export class AlertUtils {
  public static async showDefaultAlert(options: IShowAlertOptions): Promise<SweetAlertResult> {
    const swal = await import('sweetalert2');
    if (process.server) throw new Error('Alert cant be called on server side');
    return swal.default.fire({
      ...defaultAlertOptions,
      title: options.title,
      html: options.html,
      confirmButtonText: options.confirmButtonTitle,
      cancelButtonText: options.cancelButtonTitle,
      showCancelButton: !!options.cancelButtonTitle,
      customClass: {
        ...defaultAlertOptions.customClass,
        popup: 'default-alert',
      },
    });
  }
}
