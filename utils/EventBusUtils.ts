import { Vue } from 'nuxt-property-decorator';

const eventBus: Vue = new Vue();

function getEventBusInstance(): Vue {
  if (process.server) throw new Error('eventBus cannot be used on serverSide');
  return eventBus;
}

export enum EventPrintMessageColors {
  red = 'red',
  blue = 'blue',
}
export interface IEventPrintMessage {
  message: string;
  color: EventPrintMessageColors;
}
export class EventPrintMessage {
  public static $emit(payload: IEventPrintMessage): void {
    const eventBus = getEventBusInstance();
    eventBus.$emit(this.name, payload);
  }

  public static $subscribe(handler: (payload: IEventPrintMessage) => any): void {
    const eventBus = getEventBusInstance();
    eventBus.$on(this.name, handler);
  }

  public static $unsubscribe(handler: (payload: IEventPrintMessage) => any): void {
    const eventBus = getEventBusInstance();
    eventBus.$off(this.name, handler);
  }
}
