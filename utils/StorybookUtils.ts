import VueI18n from 'vue-i18n';
import ruLocale from '~/locales/ru.json';

export function getI18nForStorybook(): any {
  return new VueI18n({
    locale: 'ja',
    messages: {
      ru: {
        message: { ...ruLocale },
      },
    },
  });
}
