export function hasKey<O>(obj: O, key: keyof any): key is keyof O {
  return key in obj;
}

export function jsonClone<T>(value: T): T {
  return JSON.parse(JSON.stringify(value));
}

/* eslint-disable-next-line */
export function castTo<T>(value: any): T {
  return (value as unknown) as T;
}

export function uniqueByKey<T>(items: Array<T>, key: string): boolean {
  const valueArr = items.map(function (item) {
    if (!hasKey(item, key)) return false;
    return item[key];
  });
  const isDuplicate = valueArr.some((item, idx) => {
    return valueArr.indexOf(item) !== idx;
  });

  return !isDuplicate;
}

/**
 * Проверяет является ли enum с цифровыми значениями или со строками.
 * @param e enum который нужно проверить
 */
function isConstEnum(e: any): boolean {
  const values = Object.values(e);
  const firstType = typeof values[0];
  let result = false;

  values.forEach(value => {
    if (typeof value !== firstType) result = true;
  });

  return result;
}

/**
 * Приведит enum к массиву строк или цифр. Если enum числовой - то строки, иначе цифры
 * @param e enum который нужно привести к массиву
 */
export function enumToArray<T>(e: T): T[] {
  if (isConstEnum(e)) {
    const keys = Object.keys(e).filter(k => typeof (e as any)[k as any] === 'number');
    const values = keys.map(k => (e as any)[k as any]);
    return values;
  }

  return Object.values(e);
}

export function stringToEnumValue<T>(enumeration: T, value: string): T[keyof T] | null {
  const enumerationValues = enumToArray(enumeration);
  if (!enumerationValues.includes(value as any)) return null;

  return castTo<T[keyof T]>(value);
}
