import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';

@Component({})
export default class IndexPage extends Vue {
  private render(): VNode {
    return <div class="page">Index page</div>;
  }
}
