import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import { Context } from '@nuxt/types';
import { UserModule } from '~/store/modules/user/user.store';

@Component
export default class UserPage extends Vue {
  // StoreModules
  private userModule = UserModule.context(this.$store);

  private async asyncData({ store }: Context): Promise<void> {
    const userModule = UserModule.context(store);
    await userModule.actions.getUsers();
  }

  private get userList(): string {
    return JSON.stringify(this.userModule.state.users, null, 4);
  }

  private render(): VNode {
    return (
      <div>
        <div class="users">{this.userList}</div>
      </div>
    );
  }
}
