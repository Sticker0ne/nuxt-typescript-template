import type { NuxtConfig } from '@nuxt/types';
import ruLocale from './locales/ru.json';
import { buildDotenvConfig } from './env/env.builder';
import { buildAxiosConfig, buildProxyConfig, createTestPageIfNecessary } from './utils/NuxtConfigUtils';

const builtDotenvConfig = buildDotenvConfig();
createTestPageIfNecessary();

const config: NuxtConfig = {
  build: {},
  buildModules: ['@nuxt/typescript-build', '@nuxtjs/router', '@nuxtjs/style-resources'],
  css: ['~/assets/style/main.scss'],
  styleResources: {
    scss: ['~/assets/style/variables/variables-main.scss'],
  },
  env: {
    ...builtDotenvConfig,
  },
  head: {
    title: '🍭 nuxt-typescript-template',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'A boilerplate to start a Nuxt+TS project quickly',
      },
    ],
    link: [],
  },
  loading: { color: '#0c64c1' },
  modules: ['nuxt-i18n', '@nuxtjs/svg-sprite', '@nuxtjs/axios', '@nuxtjs/proxy'],
  plugins: [
    { src: '~/plugins/WidthWatcher', ssr: true },
    { src: '~/plugins/RouterHelper', ssr: true },
    { src: '~/plugins/Pluralization', ssr: true },
    { src: '~/plugins/ScrollLock', ssr: true },
    { src: '~/plugins/VueUse', ssr: true },
  ],
  routerModule: {
    fileName: 'router/router.ts',
  },
  i18n: {
    strategy: 'no_prefix',
    locales: ['ru'],
    defaultLocale: 'ru',
    vueI18n: {
      messages: {
        ru: { ...ruLocale },
      },
    },
  },
  svgSprite: {
    input: '~/assets/svg/sprite/icons',
    output: '~/assets/svg/sprite/output',
  },
  axios: buildAxiosConfig(builtDotenvConfig),
  proxy: buildProxyConfig(builtDotenvConfig),
  typescript: {
    typeCheck: {
      eslint: { files: './**/*.{ts,tsx,js,jsx}' },
    },
  },
};

export default config;
