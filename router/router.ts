import { Vue } from 'nuxt-property-decorator';
import Router, { RouterOptions } from 'vue-router';

import { RouteConfig } from '@nuxt/types/config/router';

Vue.use(Router);

export enum RouteNames {
  index = 'index',
  user = 'user',
}

export enum DevelopOnlyRouteNames {
  test = 'test',
}

function interopDefault(promise: Promise<any>): Promise<any> {
  return promise.then(m => m.default || m);
}

const UserPage = (): Promise<any> => interopDefault(import('~/pages/UserPage/UserPage'));
const IndexPage = (): Promise<any> => interopDefault(import('~/pages/IndexPage/IndexPage'));
const TestPage = (): Promise<any> => interopDefault(import('~/pages/TestPage/TestPage'));

function getDevelopmentExtraRoute(): RouteConfig[] {
  return [{ path: '/test', component: TestPage, name: DevelopOnlyRouteNames.test }];
}

export const routerOptions = (): RouterOptions => {
  const routes: RouteConfig[] = [
    { path: '/', component: IndexPage, name: RouteNames.index },
    { path: '/user', component: UserPage, name: RouteNames.user },
  ];

  if (process.env.NODE_ENV === 'development') routes.push(...getDevelopmentExtraRoute());

  return {
    mode: 'history',
    routes,
  };
};

export function createRouter(): Router {
  return new Router(routerOptions());
}
