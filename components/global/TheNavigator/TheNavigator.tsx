import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import './TheNavigator.scss';

@Component
export default class TheNavigator extends Vue {
  private get routeNames(): string[] {
    return Object.values(this.$rh.routeNames);
  }

  private get developOnlyRouteNames(): string[] {
    return Object.values(this.$rh.developOnlyRouteNames);
  }

  private render(): VNode {
    return (
      <div class="navigator">
        <h4>{this.$i18n.t('layout.navigator')}</h4>
        <ul>
          {this.routeNames.map(routeName => (
            <li key={`route-link-key-${routeName}`} class="navigator__item">
              <nuxt-link class="navigator__link" to={{ name: routeName }}>
                {routeName}
              </nuxt-link>
            </li>
          ))}
        </ul>
        {this.$rh.showDevelopOnlyRoutes ? (
          <div>
            <div>develop only routes</div>
            <ul>
              {this.developOnlyRouteNames.map(routeName => (
                <li key={`route-link-key-${routeName}`} class="navigator__item">
                  <nuxt-link class="navigator__link" to={{ name: routeName }}>
                    {routeName}
                  </nuxt-link>
                </li>
              ))}
            </ul>
          </div>
        ) : null}
      </div>
    );
  }
}
