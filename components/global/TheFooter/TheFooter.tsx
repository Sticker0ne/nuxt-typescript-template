import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import './TheFooter.scss';

@Component
export default class TheFooter extends Vue {
  private render(): VNode {
    return (
      <footer class="footer">
        <div class="container">
          <h2>Im footer</h2>
        </div>
      </footer>
    );
  }
}
