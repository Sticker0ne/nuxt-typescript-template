import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import './TheHeader.scss';

@Component
export default class TheHeader extends Vue {
  private render(): VNode {
    return (
      <header class="header">
        <div class="container">
          <h2>Im header</h2>
        </div>
      </header>
    );
  }
}
