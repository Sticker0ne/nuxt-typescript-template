import './BaseTableRow.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { IElemClasses, IElemStyles } from '~/types/global-types';
import type { IBaseTableColumnSettings } from '~/components/common/table/BaseTable/BaseTable.types';

@Component
export default class BaseTableRow extends Vue {
  // Настройки столбцов
  @Prop({ required: false, default: () => [] })
  public readonly columnsSettings!: IBaseTableColumnSettings[];

  // Массив ширин
  @Prop({ required: true })
  public readonly columnWidthList!: number[];

  // Список ключей колонок, которые в режиме редактирования в данный момент
  @Prop({ required: false, default: () => [] })
  public readonly columnKeysOnEdit!: string[];

  // Список ключей родительских колонок, которые в раскрытом состоянии
  @Prop({ required: false, default: () => [] })
  public readonly columnKeysExpanded!: string[];

  // список индексов родителей. Для каждой скрытой колонки есть индекс родителя
  @Prop({ required: true, default: () => [] })
  public readonly parentsIndexList!: number[];

  // список родителей, которые раскрыты в данный момент
  @Prop({ required: true, default: () => [] })
  public readonly expandedParentList!: boolean[];

  // список ключей родителей. Для каждой скрытой колонки есть ключ родителя
  @Prop({ required: true, default: () => [] })
  public readonly parentsKeyList!: (string | null)[];

  // список колонок которые являются первыми раскрытыми в своем скрываемом блоке (для тенюшки)
  @Prop({ required: true, default: () => [] })
  public readonly firstExpandedColumnsInsideHiddenBlockList!: boolean[];

  // список колонок которые являются последними раскрытыми в своем скрываемом блоке (для тенюшки)
  @Prop({ required: true, default: () => [] })
  public readonly lastExpandedColumnsInsideHiddenBlockList!: boolean[];

  // список отрицательных маргинов для первых скрытых колонок
  @Prop({ required: true, default: () => [] })
  public readonly negativeMarginForHiddenColumnList!: number[];

  // список классов для каждой колонки
  protected get builtColumnClasses(): IElemClasses[] {
    return this.columnsSettings.map((columnSetting, index) => {
      const parentIndex = this.parentsIndexList[index];
      return {
        class: {
          'edit-mode': this.columnKeysOnEdit.includes(columnSetting.key),
          hidden: parentIndex !== -1 && !this.expandedParentList[parentIndex],
          'hidden-expand': parentIndex !== -1 && this.expandedParentList[parentIndex],
          'first-item-expand': this.firstExpandedColumnsInsideHiddenBlockList[index],
          'last-item-expand': this.lastExpandedColumnsInsideHiddenBlockList[index],
        },
      };
    });
  }

  // список стилей для каждой колонки
  protected get builtColumnStyle(): IElemStyles[] {
    return this.columnsSettings.map((columnSettings, index) => {
      return {
        width: `${this.columnWidthList[index]}px`,
        'margin-left': `-${this.negativeMarginForHiddenColumnList[index]}px`,
      };
    });
  }
}
