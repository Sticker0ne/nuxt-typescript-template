import './TableHeader.scss';
import { Component, Ref } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import type { WEvents, WProps } from '~/types/tsx-element-types';
import type { ITableHeaderEvents } from '~/components/common/table/TableHeader/TableHeader.types';
import { AppButtonColors, AppButtonSlots } from '~/components/ui/AppButton/AppButton.types';
import { SVGNames } from '~/assets/ts/constants';
import AppButton from '~/components/ui/AppButton/AppButton';
import BaseTableRow from '~/components/common/table/BaseTableRow/BaseTableRow';
import { IElemClasses } from '~/types/global-types';

@Component
export default class TableHeader extends BaseTableRow {
  public _tsx!: WProps<this, 'columnKeysOnEdit' | 'columnKeysExpanded'> & WEvents<ITableHeaderEvents>;

  @Ref() private readonly column!: HTMLElement[];

  // обработчик клика по кнопке редактирования
  private emitEditButtonClick(columnName: string): void {
    emitOn(this, 'onEditButtonClick', columnName);
  }

  // обработчик клика по кнопке расширения/сужения
  private emitExpandCollapseButtonClick(columnName: string): void {
    emitOn(this, 'onExpandCollapseButtonClick', columnName);
  }

  // функция проверяющая, нужно ли отрисовать кнопку редактирования
  private checkShowOpenButton(index: number): boolean {
    if (index >= this.columnsSettings.length - 1) return false;
    if (this.columnsSettings[index].hidden) return false;

    return this.columnsSettings[index + 1].hidden || false;
  }

  private buildExpandButtonClasses(columnKey: string): IElemClasses {
    return {
      class: {
        'table-header__show-hidden-columns--expand': this.columnKeysExpanded.includes(columnKey),
      },
    };
  }

  private render(): VNode {
    return (
      <div class="table-header">
        {this.columnsSettings.map((column, index) => (
          <div
            class="table-header__column table-column"
            ref="column"
            refInFor={true}
            style={this.builtColumnStyle[index]}
            {...this.builtColumnClasses[index]}
          >
            <div class="table-header__column-title table-column__content">{column.title}</div>
            <div class="table-header__column-actions">
              {column.editable ? (
                <AppButton
                  color={AppButtonColors.transparent}
                  class="table-header__edit-button"
                  onClick={() => this.emitEditButtonClick(column.key)}
                >
                  <svg-icon slot={AppButtonSlots.iconLeft} name={SVGNames.edit} />
                </AppButton>
              ) : null}
              {this.checkShowOpenButton(index) ? (
                <AppButton
                  color={AppButtonColors.transparent}
                  class="table-header__show-hidden-columns"
                  {...this.buildExpandButtonClasses(column.key)}
                  onClick={() => this.emitExpandCollapseButtonClick(column.key)}
                />
              ) : null}
            </div>
          </div>
        ))}
      </div>
    );
  }
}
