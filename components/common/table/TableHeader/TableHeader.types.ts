export interface ITableHeaderEvents {
  onEditButtonClick: string;
  onExpandCollapseButtonClick: string;
}
