import { AppInputModes, IAppInputMask } from '~/components/ui/AppInput/AppInput.types';

export type IBaseTableItem = Record<string, string | number>;

export interface IBaseTableEvents {
  onEditButtonClick: string;
  onExpandCollapseButtonClick: string;
  onItemsChange: IBaseTableItem[];
  onAnyInputBlur: void;
}

export interface IBaseTableInputSettings {
  mode: AppInputModes;
  floatPrecision: number;
  maxLength: number;
  disabled: boolean;
  showClearButton: boolean;
  placeholder: string;
  mask?: IAppInputMask;
}

export interface IBaseTableColumnSettings {
  title: string;
  key: string;
  initWidth: number;
  minWidth?: number;
  editable?: boolean;
  hidden?: boolean;
  forbidZero?: boolean;
  inputSettings?: Partial<IBaseTableInputSettings>;
}
