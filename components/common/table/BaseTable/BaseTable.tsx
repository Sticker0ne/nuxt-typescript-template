import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { WProps } from '~/types/tsx-element-types';

import { jsonClone } from '~/utils/CommonUtils';
import type {
  IBaseTableColumnSettings,
  IBaseTableItem,
} from '~/components/common/table/BaseTable/BaseTable.types';

@Component
export default class BaseTable extends Vue {
  public _tsx!: WProps<this, 'columnKeysOnEdit' | 'columnKeysExpanded' | 'itemUniqueKey'>;

  @Prop({ required: true })
  public readonly items!: IBaseTableItem[];

  @Prop({ required: false, default: () => [] })
  public readonly columnsSettings!: IBaseTableColumnSettings[];

  @Prop({ required: false, default: () => [] })
  public readonly columnKeysOnEdit!: string[];

  @Prop({ required: false, default: () => [] })
  public readonly columnKeysExpanded!: string[];

  @Prop({ required: false, default: () => 'id' })
  public readonly itemUniqueKey!: string;

  // data для ресайза колонок
  protected resizedColumnIndex: number | null = null;
  protected columnWidthList: number[] = [];

  protected get resizeIsInProcess(): boolean {
    return this.resizedColumnIndex !== null;
  }

  // список индексов родителей. Для каждой скрытой колонки есть индекс родителя
  protected get parentsIndexList(): number[] {
    return this.columnsSettings.map((columnSetting, index) => {
      if (!columnSetting.hidden) return -1;

      let parentColumnIndex = -1;

      for (let i = index; i >= 0; i--)
        if (!this.columnsSettings[i].hidden) {
          parentColumnIndex = i;
          break;
        }

      return parentColumnIndex;
    });
  }

  // список родителей, которые раскрыты в данный момент
  protected get expandedParentList(): boolean[] {
    return this.columnsSettings.map(column => {
      if (column.hidden) return false;
      return this.columnKeysExpanded.includes(column.key);
    });
  }

  // список ключей родителей. Для каждой скрытой колонки есть ключ родителя
  protected get parentsKeyList(): (string | null)[] {
    return this.parentsIndexList.map(index => {
      if (index === -1) return null;
      return this.columnsSettings[index].key;
    });
  }

  protected get shownColumnList(): boolean[] {
    return this.columnsSettings.map((columnSettings, index) => {
      if (!columnSettings.hidden) return true;

      const parentIndex = this.parentsIndexList[index];
      return this.expandedParentList[parentIndex];
    });
  }

  // список колонок которые являются первыми раскрытыми в своем скрываемом блоке (для тенюшки)
  protected get firstExpandedColumnsInsideHiddenBlockList(): boolean[] {
    return this.columnsSettings.map((column, index) => {
      if (!column.hidden) return false;
      const parentIndex = this.parentsIndexList[index];

      const firstIndex = this.parentsIndexList.findIndex(
        parentIndexInList => parentIndexInList === parentIndex,
      );
      return index === firstIndex && this.expandedParentList[parentIndex];
    });
  }

  // список колонок которые являются последними раскрытыми в своем скрываемом блоке (для тенюшки)
  protected get lastExpandedColumnsInsideHiddenBlockList(): boolean[] {
    return this.columnsSettings.map((column, index) => {
      if (!column.hidden) return false;
      const parentIndex = this.parentsIndexList[index];

      const lastIndex = this.parentsIndexList.lastIndexOf(parentIndex);
      return index === lastIndex && this.expandedParentList[parentIndex];
    });
  }

  // список отрицательных маргинов для первых скрытых колонок
  protected get negativeMarginForHiddenColumnList(): number[] {
    return this.columnsSettings.map((columnSettings, index) => {
      if (!columnSettings.hidden) return 0;

      let resultWidth = 0;
      const parentIndex = this.parentsIndexList[index];

      if (parentIndex === -1) return 0;
      if (index !== parentIndex + 1) return 0;

      for (let i = parentIndex + 1; i < this.columnsSettings.length; i++) {
        const column = this.columnsSettings[i];
        if (!column.hidden) break;
        resultWidth += this.columnWidthList[i];
      }

      return resultWidth;
    });
  }

  // отслеживаем нажатие клавиши мыши
  protected onColumnBorderMouseDown(columnIndex: number): void {
    this.resizedColumnIndex = columnIndex;
  }

  // отслеживаем отпускание клавиши мыши
  protected onSelectText(event: Event): void {
    if (this.resizeIsInProcess) event.preventDefault();
  }

  // отслеживаем отпускание клавиши мыши
  protected onMouseUp(): void {
    this.resizedColumnIndex = null;
  }

  // отслеживаем перемещение мыши для ресайза колонок
  protected onMouseMove(event: MouseEvent): void {
    if (this.resizedColumnIndex === null) return;

    const clonedWidthArray = jsonClone(this.columnWidthList);
    const newWidth = clonedWidthArray[this.resizedColumnIndex] + event.movementX;

    clonedWidthArray[this.resizedColumnIndex] = Math.max(newWidth, 100);
    this.columnWidthList = clonedWidthArray;
  }

  protected assignColumnWidthList(): void {
    this.columnWidthList = this.columnsSettings.map(column => column.initWidth);
  }
}
