import './Table.scss';
import { Component, Prop, Ref, Watch } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import TableRow from '~/components/common/table/TableRow/TableRow';
import type { WEvents, WProps } from '~/types/tsx-element-types';
import AppScrollbar from '~/components/ui/AppScrollbar/AppScrollbar';
import TableHeader from '~/components/common/table/TableHeader/TableHeader';
import type { IElemStyles } from '~/types/global-types';
import BaseTable from '~/components/common/table/BaseTable/BaseTable';
import type { IBaseTableEvents } from '~/components/common/table/BaseTable/BaseTable.types';
import { IBaseTableItem } from '~/components/common/table/BaseTable/BaseTable.types';
import { jsonClone } from '~/utils/CommonUtils';

@Component
export default class Table extends BaseTable {
  public _tsx!: WProps<this, 'columnKeysOnEdit' | 'columnKeysExpanded' | 'itemUniqueKey'> &
    WEvents<IBaseTableEvents>;

  @Ref() private readonly tableContent!: HTMLElement;
  @Ref() private readonly numberingColumn!: HTMLElement;
  @Ref() private readonly appScrollbar!: AppScrollbar;

  @Prop({ default: () => 1, required: true })
  public readonly numberingOffset!: number;

  private emitEditButtonClick(columnName: string): void {
    emitOn(this, 'onEditButtonClick', columnName);
  }

  private emitExpandCollapseButtonClick(columnName: string): void {
    emitOn(this, 'onExpandCollapseButtonClick', columnName);
  }

  private emitItemsChange(newItems: IBaseTableItem[]): void {
    emitOn(this, 'onItemsChange', newItems);
  }

  private emitBlur(): void {
    emitOn(this, 'onAnyInputBlur');
  }

  private expandAnimationDuration = 350;

  // data для установки стилек для тени
  private tableContentHeight = 0;

  // ширина колонки с нумерацией
  private numberingColumnWidth = 0;

  // нижний отступ для позиционирования горизонтального скролла
  private scrollBarPaddingBottom = 40;

  private scrollOptions = {
    swipeEasing: false,
    suppressScrollY: true,
  };

  // для диапазона нумерации
  private get numberingArray(): number[] {
    return this.items.map((item, index) => index + this.numberingOffset);
  }

  private get shadowBlockHeight(): number {
    return this.tableContentHeight - 12;
  }

  private get scrollBarStyles(): IElemStyles {
    return {
      'margin-left': `${this.numberingColumnWidth}px`,
      'padding-bottom': `${this.scrollBarPaddingBottom}px`,
    };
  }

  private get dividerStyleList(): IElemStyles[] {
    return this.columnsSettings.map((columnSettings, index) => {
      const isShown = this.shownColumnList[index];

      let resultLeft = 0;
      for (let i = 0; i <= index; i++) if (this.shownColumnList[i]) resultLeft += this.columnWidthList[i];

      return {
        height: isShown ? `${this.tableContentHeight - 12}px` : '0px',
        left: `${resultLeft}px`,
      };
    });
  }

  private assignNumberingColumnWidth(): void {
    this.numberingColumnWidth = this.numberingColumn.clientWidth;
  }

  private assignTableContentHeight(): void {
    this.tableContentHeight = this.tableContent.clientHeight;
  }

  protected onItemChange(newItem: IBaseTableItem): void {
    const newItems = jsonClone(this.items);
    const itemIdx = newItems.findIndex(item => item[this.itemUniqueKey] === newItem[this.itemUniqueKey]);
    if (itemIdx === -1) return;

    newItems.splice(itemIdx, 1, newItem);
    this.emitItemsChange(newItems);
  }

  private created(): void {
    this.assignColumnWidthList();
  }

  private mounted(): void {
    window.addEventListener('mouseup', this.onMouseUp);
    window.addEventListener('mouseleave', this.onMouseUp);
    window.addEventListener('mousemove', this.onMouseMove);
    window.addEventListener('selectstart', this.onSelectText);

    this.assignNumberingColumnWidth();
    this.assignTableContentHeight();
  }

  private beforeDestroy(): void {
    window.removeEventListener('mouseup', this.onMouseUp);
    window.removeEventListener('mouseleave', this.onMouseUp);
    window.removeEventListener('mousemove', this.onMouseMove);
    window.removeEventListener('selectstart', this.onSelectText);
  }

  @Watch('columnKeysExpanded')
  private onExpandedColumnsChange(): void {
    setTimeout(this.appScrollbar.updatePSTwice, this.expandAnimationDuration);
  }

  private render(): VNode {
    return (
      <div class="table">
        <AppScrollbar
          class="table__scrollbar"
          style={this.scrollBarStyles}
          options={this.scrollOptions}
          ref="appScrollbar"
        >
          <div class="table__scroll" ref="tableContent">
            <TableHeader
              columnsSettings={this.columnsSettings}
              columnWidthList={this.columnWidthList}
              columnKeysOnEdit={this.columnKeysOnEdit}
              columnKeysExpanded={this.columnKeysExpanded}
              onEditButtonClick={this.emitEditButtonClick}
              onExpandCollapseButtonClick={this.emitExpandCollapseButtonClick}
              parentsIndexList={this.parentsIndexList}
              expandedParentList={this.expandedParentList}
              parentsKeyList={this.parentsKeyList}
              firstExpandedColumnsInsideHiddenBlockList={this.firstExpandedColumnsInsideHiddenBlockList}
              lastExpandedColumnsInsideHiddenBlockList={this.lastExpandedColumnsInsideHiddenBlockList}
              negativeMarginForHiddenColumnList={this.negativeMarginForHiddenColumnList}
            />
            {this.items.map(row => (
              <TableRow
                item={row}
                columnWidthList={this.columnWidthList}
                columnKeysOnEdit={this.columnKeysOnEdit}
                columnKeysExpanded={this.columnKeysExpanded}
                shadowBlockHeight={this.shadowBlockHeight}
                columnsSettings={this.columnsSettings}
                parentsIndexList={this.parentsIndexList}
                expandedParentList={this.expandedParentList}
                parentsKeyList={this.parentsKeyList}
                firstExpandedColumnsInsideHiddenBlockList={this.firstExpandedColumnsInsideHiddenBlockList}
                lastExpandedColumnsInsideHiddenBlockList={this.lastExpandedColumnsInsideHiddenBlockList}
                negativeMarginForHiddenColumnList={this.negativeMarginForHiddenColumnList}
                onItemChange={this.onItemChange}
                onAnyInputBlur={this.emitBlur}
              />
            ))}
          </div>
          {this.columnsSettings.map((columnSetting, index) => (
            <div
              class="table__divider"
              style={this.dividerStyleList[index]}
              onMousedown={() => this.onColumnBorderMouseDown(index)}
            />
          ))}
        </AppScrollbar>
        <div ref="numberingColumn" class="table__numbering numbering">
          <div class="numbering__header">{this.$t('symbols.number')}</div>
          <ul class="numbering__list">
            {this.numberingArray.map(number => (
              <li class="numbering__item">{number}</li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}
