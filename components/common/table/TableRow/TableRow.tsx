import './TableRow.scss';
import { Component, Prop } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import type { WEvents, WProps } from '~/types/tsx-element-types';
import type { IElemStyles } from '~/types/global-types';
import type { ITableRowEvents } from '~/components/common/table/TableRow/TableRow.types';
import type {
  IBaseTableColumnSettings,
  IBaseTableItem,
} from '~/components/common/table/BaseTable/BaseTable.types';
import BaseTableRow from '~/components/common/table/BaseTableRow/BaseTableRow';
import AppInput from '~/components/ui/AppInput/AppInput';
import { AppInputModes } from '~/components/ui/AppInput/AppInput.types';

@Component
export default class TableRow extends BaseTableRow {
  public _tsx!: WProps<this, 'columnKeysOnEdit' | 'columnKeysExpanded' | 'shadowBlockHeight'> &
    WEvents<ITableRowEvents>;

  @Prop({ required: true })
  public readonly item!: IBaseTableItem;

  @Prop({ required: true })
  public readonly shadowBlockHeight!: number;

  private emitChange(columnSettings: IBaseTableColumnSettings, value: string): void {
    const mode = columnSettings.inputSettings?.mode || AppInputModes.text;
    let newValue: string | number = '';
    if (mode === AppInputModes.text) newValue = value;
    if (mode === AppInputModes.integer || mode === AppInputModes.positiveInteger)
      newValue = Number.parseInt(value) || 0;
    if (mode === AppInputModes.float || mode === AppInputModes.positiveFloat)
      newValue = Number.parseFloat(value) || 0.0;

    emitOn(this, 'onItemChange', { ...this.item, ...{ [columnSettings.key]: newValue } });
  }

  private emitBlur(): void {
    emitOn(this, 'onAnyInputBlur');
  }

  private get shadowBlockStyles(): IElemStyles {
    return {
      height: `${this.shadowBlockHeight}px`,
    };
  }

  private render(): VNode {
    return (
      <div class="table-row">
        {this.columnsSettings.map((columnSettings, index) => (
          <div
            class="table-row__cell table-row-column table-column"
            {...this.builtColumnClasses[index]}
            style={this.builtColumnStyle[index]}
          >
            {this.columnKeysOnEdit.includes(columnSettings.key) ? (
              <div class="table-row-column__input">
                <AppInput
                  value={this.item[columnSettings.key].toString()}
                  onInput={value => this.emitChange(columnSettings, value)}
                  onBlur={this.emitBlur}
                  floatPrecision={columnSettings.inputSettings?.floatPrecision}
                  maxLength={columnSettings.inputSettings?.maxLength}
                  disabled={columnSettings.inputSettings?.disabled}
                  showClearButton={columnSettings.inputSettings?.showClearButton || false}
                  placeholder={columnSettings.inputSettings?.placeholder}
                  mask={columnSettings.inputSettings?.mask}
                />
              </div>
            ) : (
              <div class="table-column__content">{this.item[columnSettings.key]}</div>
            )}
            <div class="table-row-column__shadow" style={this.shadowBlockStyles} />
          </div>
        ))}
      </div>
    );
  }
}
