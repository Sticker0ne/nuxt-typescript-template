import { IBaseTableItem } from '~/components/common/table/BaseTable/BaseTable.types';

export interface ITableRowEvents {
  onItemChange: IBaseTableItem;
  onAnyInputBlur: void;
}
