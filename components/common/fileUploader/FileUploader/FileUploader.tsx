import './FileUploader.scss';
import { nanoid } from 'nanoid';
import { Component, Model, Prop, Vue, Watch } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import Dropzone from 'dropzone';
import {
  buildAcceptedFilesString,
  buildReadableSizeFromBytes,
  buildUploadUrl,
  convertDropzoneFileToFile,
} from '~/utils/FileUploaderUtils';
import { jsonClone } from '~/utils/CommonUtils';
import FileTemplate from '~/components/common/fileUploader/FileTemplate/FileTemplate';
import {
  FileUploaderFileExtensions,
  IDropzoneFileExtended,
  IFileUploaderEvents,
  IFileUploaderNormalizedFile,
} from '~/components/common/fileUploader/FileUploader/FileUploader.types';
import type { IElemClasses } from '~/types/global-types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class FileUploader extends Vue {
  public _tsx!: WProps<
    this,
    'disabled' | 'timeout' | 'uploadUrl' | 'acceptedFiles' | 'maxFiles' | 'maxFileSizeMb' | 'paramName'
  > &
    WEvents<IFileUploaderEvents>;

  @Model('change', { default: () => [] })
  public readonly files!: IFileUploaderNormalizedFile[];

  private get safeFiles(): IFileUploaderNormalizedFile[] {
    return jsonClone(this.files || []);
  }

  private set safeFiles(newFiles: IFileUploaderNormalizedFile[]) {
    const newFilesString = JSON.stringify(newFiles);
    const oldFilesString = JSON.stringify(this.safeFiles);

    if (newFilesString !== oldFilesString) emitOn(this, 'onChange', newFiles);
    setTimeout(this.syncDzFiles);
  }

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({
    default: () => [FileUploaderFileExtensions.png, FileUploaderFileExtensions.jpeg],
    required: false,
  })
  public readonly acceptedFiles!: FileUploaderFileExtensions[];

  @Prop({ default: () => 0.5 * 60 * 1000, required: false })
  public readonly timeout!: number;

  @Prop({ default: () => '/upload', required: false })
  public readonly uploadUrl!: string;

  @Prop({ default: () => 'file', required: false })
  public readonly paramName!: string;

  @Prop({ default: () => 2, required: false })
  public readonly maxFiles!: number;

  @Prop({ default: () => 5, required: false })
  public readonly maxFileSizeMb!: number;

  private dropzoneFiles: IFileUploaderNormalizedFile[] = [];
  private dropzone: Dropzone = null as any;
  private uploaderId = '';

  private dropzoneDragEventCounter = 0;
  private dropzoneInProgress = false;
  private dropzoneState = true;

  private syncDzFiles(): void {
    const safeFiles = this.safeFiles;
    const safeFilesDzIds = safeFiles
      .filter(safeFile => safeFile.dzCustomId)
      .map(safeFile => safeFile.dzCustomId);

    this.dropzone.files = this.dropzone.files.filter(file => {
      const normalizedFile = (file as any) as IFileUploaderNormalizedFile;
      return safeFilesDzIds.includes(normalizedFile.dzCustomId);
    });

    this.convertDropzoneFilesToNormalizedFiles();
    this.setDropzoneState(!this.dzShouldBeDisabled || this.dropzoneInProgress);
  }

  private setUploaderId(): void {
    if (!this.uploaderId.length) this.uploaderId = nanoid();
  }

  private convertDropzoneFilesToNormalizedFiles(): void {
    this.dropzoneFiles = jsonClone(this.dropzone.files.map(rawFile => convertDropzoneFileToFile(rawFile)));
  }

  private emitNewFilesFromDropzone(): void {
    this.dropzoneFiles = jsonClone(this.dropzone.files.map(rawFile => convertDropzoneFileToFile(rawFile)));
    const safeFiles = jsonClone(this.safeFiles.filter(safeFile => !safeFile.dzCustomId));
    this.safeFiles = [...this.dropzoneFiles, ...safeFiles];
  }

  private async createDzInstance(): Promise<void> {
    const DzInstance = await import('dropzone').then(module => module.default);
    this.dropzone = new DzInstance(`div#dropzone-container-${this.uploaderId}`, {
      clickable: [`span#dropzone-text-${this.uploaderId}`, `div#dropzone-container-${this.uploaderId}`],
      url: buildUploadUrl(this.uploadUrl),
      acceptedFiles: buildAcceptedFilesString(this.acceptedFiles),
      timeout: this.timeout,
      maxFiles: this.maxFiles,
      maxFilesize: this.maxFileSizeMb,
      paramName: this.paramName,

      addedfile: (file: IDropzoneFileExtended) => {
        const parentFiles = this.safeFiles.filter(parentFile => !parentFile.dzCustomId);
        const selfFiles = this.dropzone.files;

        const totalLength = parentFiles.length + selfFiles.length - 1;

        if (totalLength < this.maxFiles) {
          file.dzCustomId = nanoid();
          this.dropzoneInProgress = true;
          this.emitNewFilesFromDropzone();
        } else this.dropzone.removeFile(file);
      },
      removedfile: this.emitNewFilesFromDropzone,
      uploadprogress: this.convertDropzoneFilesToNormalizedFiles,
      error: this.emitNewFilesFromDropzone,
      canceled: this.emitNewFilesFromDropzone,
      success: this.emitNewFilesFromDropzone,
      complete: this.emitNewFilesFromDropzone,
      queuecomplete: () => {
        this.emitNewFilesFromDropzone();
        this.dropzoneInProgress = false;
        if (this.dzShouldBeDisabled) this.setDropzoneState(false);
      },
      dragenter: () => {
        this.dropzoneDragEventCounter++;
      },
      dragleave: () => {
        this.dropzoneDragEventCounter--;
      },
      drop: () => {
        this.dropzoneDragEventCounter = 0;
      },
    });
  }

  private onFileRemove(removingFile: IFileUploaderNormalizedFile): void {
    const filteredFiles = this.safeFiles.filter(
      safeFile =>
        safeFile.id !== (removingFile.id || -1) && safeFile.dzCustomId !== (removingFile.dzCustomId || -1),
    );

    if (removingFile.dzCustomId) {
      const filteredDropzoneFiles = this.dropzone.files.filter(
        dzFile => (dzFile as IDropzoneFileExtended).dzCustomId !== removingFile.dzCustomId,
      );

      this.dropzone.files = filteredDropzoneFiles;
      this.convertDropzoneFilesToNormalizedFiles();
    }

    this.safeFiles = filteredFiles;

    setTimeout(() => {
      if (!this.disabled && !this.maxFilesRiched) this.setDropzoneState(true);
    });
  }

  private get filesForRender(): IFileUploaderNormalizedFile[] {
    return this.safeFiles.map(clonedFile => {
      if (!clonedFile.dzCustomId) return clonedFile;
      const dropzoneFiles = this.dropzoneFiles || [];
      const dzFile = dropzoneFiles.find(dzFile => dzFile.dzCustomId === clonedFile.dzCustomId);
      if (!dzFile) return clonedFile;
      return { ...clonedFile, progress: dzFile.progress };
    });
  }

  private get maxFilesRiched(): boolean {
    return this.safeFiles.length >= this.maxFiles;
  }

  private get dzShouldBeDisabled(): boolean {
    return this.maxFilesRiched || this.disabled;
  }

  private get containerClasses(): IElemClasses {
    return {
      class: {
        'file-uploader__dropzone--drag-state': this.dropzoneDragEventCounter > 0,
        'file-uploader__dropzone--disabled': !this.dropzoneState,
      },
    };
  }

  private get readableMaxFileSize(): string {
    return buildReadableSizeFromBytes(this.maxFileSizeMb * 1024 * 1024);
  }

  private get readableExtensions(): string {
    return this.acceptedFiles.join(', ');
  }

  private async mounted(): Promise<void> {
    this.setUploaderId();
    await this.createDzInstance();
    this.setDropzoneState(!(this.disabled || this.maxFilesRiched));
  }

  private setDropzoneState(state: boolean): void {
    if (state === this.dropzoneState) return;
    this.dropzoneState = state;
    return state ? this.dropzone.enable() : this.dropzone.disable();
  }

  @Watch('disabled')
  private onDisabledPropChange(disabled: boolean): void {
    if (disabled && !this.dropzoneInProgress) this.setDropzoneState(false);
    if (!disabled) this.setDropzoneState(true);
  }

  @Watch('safeFiles')
  private onSafeFilePropChange(): void {
    this.syncDzFiles();
  }

  private render(): VNode {
    return (
      <div class="file-uploader">
        <div
          id={`dropzone-container-${this.uploaderId}`}
          class="file-uploader__dropzone"
          {...this.containerClasses}
        >
          {this.dropzoneDragEventCounter ? (
            <span id={`dropzone-text-${this.uploaderId}`}>
              {this.$i18n.tc('components.fileUploader.putFiles', this.maxFiles)}
            </span>
          ) : (
            <span id={`dropzone-text-${this.uploaderId}`}>
              <span class="file-uploader__text file-uploader__text--blue">
                {this.$i18n.tc('components.fileUploader.chooseFile.firstPart', this.maxFiles)}
              </span>
              &nbsp;
              <span class="file-uploader__text">
                {this.$i18n.tc('components.fileUploader.chooseFile.secondPart', this.maxFiles)}
              </span>
            </span>
          )}
        </div>
        {!this.safeFiles.length ? (
          <div class="file-uploader__tips">
            {this.$i18n.tc('components.fileUploader.description.firstPart', this.maxFiles, {
              size: this.readableMaxFileSize,
            })}
            &nbsp;
            {this.$i18n.tc('components.fileUploader.description.secondPart', this.acceptedFiles.length, {
              formats: this.readableExtensions,
            })}
          </div>
        ) : null}

        <ul class="file-uploader__files">
          {this.filesForRender.map(file => (
            <li class="file-uploader__file-template">
              <FileTemplate
                fileName={file.name}
                type={file.type}
                size={file.size}
                progress={file.progress}
                downloadUrl={file.url}
                status={file.status}
                showRemoveBtn={!this.disabled}
                disabled={!this.dropzoneState}
                onRemove={() => this.onFileRemove(file)}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
