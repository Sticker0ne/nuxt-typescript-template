import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, number, object, button } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import {
  FileUploaderFileExtensions,
  IFileUploaderNormalizedFile,
} from '~/components/common/fileUploader/FileUploader/FileUploader.types';
import FileUploader from '~/components/common/fileUploader/FileUploader/FileUploader';
import {
  FileTemplateStatuses,
  FileTemplateTypes,
} from '~/components/common/fileUploader/FileTemplate/FileTemplate.types';
import { getI18nForStorybook } from '~/utils/StorybookUtils';

storiesOf('common/FileUploader', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    i18n: getI18nForStorybook(),
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      acceptedFiles: {
        type: Array,
        default: () =>
          object('acceptedFiles', [FileUploaderFileExtensions.png, FileUploaderFileExtensions.jpeg]),
      },
      timeout: {
        type: Number,
        default: () => number('timeout', 0.5 * 60 * 1000),
      },
      uploadUrl: {
        type: String,
        default: () => text('uploadUrl', '/upload'),
      },
      paramName: {
        type: String,
        default: () => text('paramName', 'file'),
      },
      maxFiles: {
        type: Number,
        default: () => number('maxFiles', 3),
      },
      maxFileSizeMb: {
        type: Number,
        default: () => number('maxFileSizeMb', 5),
      },
    },
    data() {
      return {
        showFileUploader: false,
        files: [],
      };
    },
    methods: {
      ...StoryContextAny,
      onResetBtnClick() {
        this.showFileUploader = false;
        this.files = [];

        setTimeout(() => {
          this.showFileUploader = true;
        }, 100);
      },
      onChange(files: IFileUploaderNormalizedFile) {
        this.files = files;
        action('onChange')(files);
      },
    },
    render() {
      button('Reload FileUploader', this.onResetBtnClick);
      return (
        <div>
          {!this.showFileUploader ? (
            <span>Click on Reload FileUploader in Knobs pannel to begin and to change props</span>
          ) : null}
          {this.showFileUploader ? (
            <FileUploader
              files={this.files}
              onChange={this.onChange}
              disabled={this.disabled}
              acceptedFiles={this.acceptedFiles}
              timeout={this.timeout}
              uploadUrl={this.uploadUrl}
              paramName={this.paramName}
              maxFiles={this.maxFiles}
              maxFileSizeMb={this.maxFileSizeMb}
            />
          ) : null}
        </div>
      );
    },
  }))
  .add('With preset files', () => ({
    ...StoryContextAny,
    i18n: getI18nForStorybook(),
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      acceptedFiles: {
        type: Array,
        default: () =>
          object('acceptedFiles', [FileUploaderFileExtensions.png, FileUploaderFileExtensions.jpeg]),
      },
      timeout: {
        type: Number,
        default: () => number('timeout', 0.5 * 60 * 1000),
      },
      uploadUrl: {
        type: String,
        default: () => text('uploadUrl', '/upload'),
      },
      paramName: {
        type: String,
        default: () => text('paramName', 'file'),
      },
      maxFiles: {
        type: Number,
        default: () => number('maxFiles', 3),
      },
      maxFileSizeMb: {
        type: Number,
        default: () => number('maxFileSizeMb', 5),
      },
    },
    data() {
      return {
        showFileUploader: false,
        files: [],
      };
    },
    methods: {
      ...StoryContextAny,
      onResetBtnClick() {
        this.showFileUploader = false;
        this.files = [
          {
            name: 'firstFileFromServer.jpeg',
            size: 45695,
            type: FileTemplateTypes.jpg,
            status: FileTemplateStatuses.success,
            url: 'https://i.pinimg.com/originals/84/9b/63/849b636ec25a4f8badbf66acfff2d0c7.jpg',
            id: 'qwe',
          },
          {
            name: 'secondFileFromServer.xlsx',
            size: 451695,
            type: FileTemplateTypes.xlsx,
            status: FileTemplateStatuses.success,
            url: 'https://i.pinimg.com/originals/84/9b/63/849b636ec25a4f8badbf66acfff2d0c7.jpg',
            id: 'asd',
          },
        ];

        setTimeout(() => {
          this.showFileUploader = true;
        }, 100);
      },
      onChange(files: IFileUploaderNormalizedFile) {
        this.files = files;
        action('onChange')(files);
      },
    },
    render() {
      button('Reload FileUploader', this.onResetBtnClick);
      return (
        <div>
          {!this.showFileUploader ? (
            <span>Click on Reload FileUploader in Knobs pannel to begin and to change props</span>
          ) : null}
          {this.showFileUploader ? (
            <FileUploader
              files={this.files}
              onChange={this.onChange}
              disabled={this.disabled}
              acceptedFiles={this.acceptedFiles}
              timeout={this.timeout}
              uploadUrl={this.uploadUrl}
              paramName={this.paramName}
              maxFiles={this.maxFiles}
              maxFileSizeMb={this.maxFileSizeMb}
            />
          ) : null}
        </div>
      );
    },
  }))
  .add('One way binding with preset files', () => ({
    ...StoryContextAny,
    i18n: getI18nForStorybook(),
    props: {
      files: {
        type: Array,
        default: () =>
          object('files', [
            {
              name: 'firstFileFromServer.jpeg',
              size: 45695,
              type: FileTemplateTypes.jpg,
              status: FileTemplateStatuses.success,
              url: 'https://i.pinimg.com/originals/84/9b/63/849b636ec25a4f8badbf66acfff2d0c7.jpg',
              id: 'qwe',
            },
            {
              name: 'secondFileFromServer.xlsx',
              size: 451695,
              type: FileTemplateTypes.xlsx,
              status: FileTemplateStatuses.success,
              url: 'https://i.pinimg.com/originals/84/9b/63/849b636ec25a4f8badbf66acfff2d0c7.jpg',
              id: 'asd',
            },
          ]),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      acceptedFiles: {
        type: Array,
        default: () =>
          object('acceptedFiles', [FileUploaderFileExtensions.png, FileUploaderFileExtensions.jpeg]),
      },
      timeout: {
        type: Number,
        default: () => number('timeout', 0.5 * 60 * 1000),
      },
      uploadUrl: {
        type: String,
        default: () => text('uploadUrl', '/upload'),
      },
      paramName: {
        type: String,
        default: () => text('paramName', 'file'),
      },
      maxFiles: {
        type: Number,
        default: () => number('maxFiles', 3),
      },
      maxFileSizeMb: {
        type: Number,
        default: () => number('maxFileSizeMb', 5),
      },
    },
    data() {
      return {
        showFileUploader: false,
      };
    },
    methods: {
      ...StoryContextAny,
      onResetBtnClick() {
        this.showFileUploader = false;
        setTimeout(() => {
          this.showFileUploader = true;
        }, 100);
      },
      onChange(files: IFileUploaderNormalizedFile) {
        action('onChange')(files);
      },
    },
    render() {
      button('Reload FileUploader', this.onResetBtnClick);
      return (
        <div>
          {!this.showFileUploader ? (
            <span>Click on Reload FileUploader in Knobs pannel to begin and to change props</span>
          ) : null}
          {this.showFileUploader ? (
            <FileUploader
              files={this.files}
              onChange={this.onChange}
              disabled={this.disabled}
              acceptedFiles={this.acceptedFiles}
              timeout={this.timeout}
              uploadUrl={this.uploadUrl}
              paramName={this.paramName}
              maxFiles={this.maxFiles}
              maxFileSizeMb={this.maxFileSizeMb}
            />
          ) : null}
        </div>
      );
    },
  }));
