import type { DropzoneFile } from 'dropzone';
import {
  FileTemplateStatuses,
  FileTemplateTypes,
} from '~/components/common/fileUploader/FileTemplate/FileTemplate.types';

export interface IFileUploaderFileResponse {
  id: string;
  name: string;
  size: number;
  url: string;
}

export enum FileUploaderFileExtensions {
  css = 'css',
  csv = 'csv',
  doc = 'doc',
  docx = 'docx',
  jpeg = 'jpeg',
  jpg = 'jpg',
  p7s = 'p7s',
  pdf = 'pdf',
  png = 'png',
  psd = 'psd',
  rtf = 'rtf',
  tiff = 'tiff',
  txt = 'txt',
  xls = 'xls',
  xlsx = 'xlsx',
  xml = 'xml',
  zip = 'zip',
}

export interface IFileUploaderNormalizedFile {
  name: string;
  size: number;
  type: FileTemplateTypes;
  status: FileTemplateStatuses;
  progress: number;
  url?: string;
  id?: string;
  dzCustomId?: string;
}

export interface IDropzoneFileExtended extends DropzoneFile {
  dzCustomId?: string;
}

export interface IFileUploaderEvents {
  onChange: IFileUploaderNormalizedFile[];
}
