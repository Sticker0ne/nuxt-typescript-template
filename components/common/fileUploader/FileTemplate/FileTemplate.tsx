import './FileTemplate.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import {
  FileTemplateStatuses,
  FileTemplateTypes,
  IFileTemplateEvents,
} from '~/components/common/fileUploader/FileTemplate/FileTemplate.types';
import AppButton from '~/components/ui/AppButton/AppButton';
import { buildReadableSizeFromBytes } from '~/utils/FileUploaderUtils';
import { AppButtonColors, AppButtonSlots } from '~/components/ui/AppButton/AppButton.types';
import { SVGNames } from '~/assets/ts/constants';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class FileTemplate extends Vue {
  public _tsx!: WProps<
    this,
    'disabled' | 'progress' | 'fileName' | 'downloadUrl' | 'type' | 'size' | 'showRemoveBtn' | 'status'
  > &
    WEvents<IFileTemplateEvents>;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({ default: () => FileTemplateStatuses.unknown, required: false })
  public readonly status!: FileTemplateStatuses;

  @Prop({ type: Boolean, default: () => true, required: false })
  public readonly showRemoveBtn!: boolean;

  @Prop({ default: () => 0, required: false })
  public readonly progress!: number;

  @Prop({ default: () => '', required: false })
  public readonly fileName!: string;

  @Prop({ default: () => '', required: false })
  public readonly downloadUrl!: string;

  @Prop({ default: () => FileTemplateTypes.xlsx, required: false })
  public readonly type!: FileTemplateTypes;

  @Prop({ default: () => 0, required: false })
  public readonly size!: number;

  private get showError(): boolean {
    return this.status === FileTemplateStatuses.error;
  }

  private get readableSize(): string {
    return buildReadableSizeFromBytes(this.size);
  }

  private onDownloadClick(): void {
    if (!this.downloadUrl) return;
    window.open(this.downloadUrl, '_blank');
  }

  private onRemoveClick(): void {
    emitOn(this, 'onRemove');
  }

  private get loadedFile(): boolean {
    return this.status === FileTemplateStatuses.success;
  }

  private get progressStyles(): string {
    return `width:${this.progress}%`;
  }

  private render(): VNode {
    return (
      <div class="file-template">
        <div class="file-template__row">
          <div class="file-template__icon">{this.type}</div>
          <div class="file-template__info">
            <div class="file-template__name">{this.fileName}</div>
            <div class="file-template__row">
              {this.loadedFile ? <div class="file-template__size">{this.readableSize}</div> : null}
              {!this.showError && !this.loadedFile ? (
                <div class="file-template__progress">
                  <div class="load-progress">
                    <span class="load-progress__progress" style={this.progressStyles} />
                  </div>
                </div>
              ) : null}
              {this.showError ? (
                <div class="file-template__error-wrapper">
                  <div class="file-template__size">{this.readableSize}</div>
                  <div class="file-template__error">{this.$i18n.tc('components.fileTemplate.error')}</div>
                </div>
              ) : null}
            </div>
          </div>
          <div class="file-template__actions">
            {this.downloadUrl.length ? (
              <AppButton
                onClick={this.onDownloadClick}
                color={AppButtonColors.transparent}
                class="file-template__button"
              >
                <svg-icon
                  class="file-template__button-icon"
                  slot={AppButtonSlots.iconLeft}
                  name={SVGNames.download}
                />
              </AppButton>
            ) : null}
            {this.showRemoveBtn ? (
              <AppButton
                onClick={this.onRemoveClick}
                color={AppButtonColors.transparent}
                class="file-template__button file-template__button--delete"
              >
                <svg-icon
                  class="file-template__button-icon"
                  slot={AppButtonSlots.iconLeft}
                  name={SVGNames.cross}
                />
              </AppButton>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
