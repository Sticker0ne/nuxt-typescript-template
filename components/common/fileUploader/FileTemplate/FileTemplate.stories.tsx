import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, number, select } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import {
  FileTemplateStatuses,
  FileTemplateTypes,
} from '~/components/common/fileUploader/FileTemplate/FileTemplate.types';
import FileTemplate from '~/components/common/fileUploader/FileTemplate/FileTemplate';
import { getI18nForStorybook } from '~/utils/StorybookUtils';

storiesOf('common/FileTemplate', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    ...StoryContextAny,
    i18n: getI18nForStorybook(),
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      fileName: {
        type: String,
        default: () => text('fileName', 'file.jpg'),
      },
      status: {
        type: String,
        default: () => select('status', FileTemplateStatuses, FileTemplateStatuses.success),
      },
      showRemoveBtn: {
        type: Boolean,
        default: () => boolean('showRemoveBtn', true),
      },
      progress: {
        type: Number,
        default: () => number('progress', 0),
      },
      downloadUrl: {
        type: String,
        default: () => text('downloadUrl', 'http://www.snowbd.ru/fs/a_desctop/1410_pic.jpg'),
      },
      type: {
        type: String,
        default: () => select('type', FileTemplateTypes, FileTemplateTypes.jpg),
      },
      size: {
        type: Number,
        default: () => number('size', 12345),
      },
    },
    methods: {
      ...StoryContextAny,
      onRemove() {
        action('onRemove');
      },
    },
    render() {
      return (
        <div>
          <FileTemplate
            disabled={this.disabled}
            fileName={this.fileName}
            status={this.status}
            showRemoveBtn={this.showRemoveBtn}
            progress={this.progress}
            downloadUrl={this.downloadUrl}
            type={this.type}
            size={this.size}
            onRemove={this.onRemove}
          />
        </div>
      );
    },
  }));
