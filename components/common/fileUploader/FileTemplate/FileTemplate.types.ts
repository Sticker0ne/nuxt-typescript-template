export interface IFileTemplateEvents {
  onRemove: void;
}

export enum FileTemplateTypes {
  jpg = 'jpg',
  png = 'png',
  xlsx = 'xlsx',
  doc = 'doc',
  pdf = 'pdf',
  txt = 'txt',
  unknown = 'unknown',
}

export enum FileTemplateStatuses {
  success = 'success',
  error = 'error',
  unknown = 'unknown',
}
