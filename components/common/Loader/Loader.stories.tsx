import { storiesOf } from '@storybook/vue';
import { withKnobs, boolean, select, object } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';

import {
  ILoaderSpinnerSize,
  LoaderSpinnerColors,
  LoaderSpinnerSizes,
} from '~/components/common/Loader/Loader.types';
import Loader from '~/components/common/Loader/Loader';
import AppButton from '~/components/ui/AppButton/AppButton';
import { AppButtonColors } from '~/components/ui/AppButton/AppButton.types';

storiesOf('common/Loader', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    ...StoryContextAny,
    props: {
      showLoader: {
        type: Boolean,
        default: () => boolean('showLoader', true),
      },
      spinnerSize: {
        type: String,
        default: () => select('spinnerSize', LoaderSpinnerSizes, LoaderSpinnerSizes.medium),
      },
      spinnerColor: {
        type: String,
        default: () => select('spinnerColor', LoaderSpinnerColors, LoaderSpinnerColors.red),
      },
      fullscreen: {
        type: Boolean,
        default: () => boolean('fullscreen', false),
      },
      showOverlay: {
        type: Boolean,
        default: () => boolean('showOverlay', true),
      },
    },
    render() {
      return (
        <div style="height: 200px; background-color: rgba(0, 0, 0, .2)">
          {this.showLoader ? (
            <Loader
              fullscreen={this.fullscreen}
              showOverlay={this.showOverlay}
              spinnerSize={this.spinnerSize}
              spinnerColor={this.spinnerColor}
            />
          ) : null}
        </div>
      );
    },
  }))
  .add('Custom Spinner Size', () => ({
    ...StoryContextAny,
    props: {
      showLoader: {
        type: Boolean,
        default: () => boolean('showLoader', true),
      },

      customSpinnerSize: {
        type: Object,
        default: () => object('customSpinnerSize', { width: 100, height: 100 }),
      },
    },
    computed: {
      ...StoryContextAny,
      computedCustomSpinnerSize(): ILoaderSpinnerSize | undefined {
        if (!this.customSpinnerSize) return undefined;
        return this.customSpinnerSize;
      },
    },
    render() {
      return (
        <div style="height: 200px; background-color: rgba(0, 0, 0, .2)">
          {this.showLoader ? <Loader customSpinnerSize={this.computedCustomSpinnerSize} /> : null}
        </div>
      );
    },
  }))
  .add('Catch Focus', () => ({
    ...StoryContextAny,
    props: {
      noCatchFocus: {
        type: Boolean,
        default: () => boolean('noCatchFocus', false),
      },
    },
    data() {
      return {
        showLoader: false,
      };
    },
    methods: {
      ...StoryContextAny,
      onClickToggleLoaderButton() {
        this.showLoader = true;
        setTimeout(() => {
          this.showLoader = false;
        }, 1000);
      },
    },
    render() {
      return (
        <div>
          <AppButton color={AppButtonColors.red} onClick={this.onClickToggleLoaderButton}>
            Показать лоадер
          </AppButton>
          <div style="height: 100vh; background-color: rgba(0, 0, 0, .2)">
            {this.showLoader ? <Loader noCatchFocus={this.noCatchFocus} /> : null}
          </div>
        </div>
      );
    },
  }));
