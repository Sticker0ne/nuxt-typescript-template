export interface ILoaderSpinnerSize {
  width: number;
  height?: number;
}

export enum LoaderSpinnerSizes {
  small = 'small',
  medium = 'medium',
  large = 'large',
}

export enum LoaderSpinnerColors {
  red = 'red',
  black = 'black',
  green = 'green',
}
