import './Loader.scss';
import { Component, Prop, Ref, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import {
  ILoaderSpinnerSize,
  LoaderSpinnerSizes,
  LoaderSpinnerColors,
} from '~/components/common/Loader/Loader.types';
import type { IElemClasses, IElemStyles } from '~/types/global-types';
import type { WProps } from '~/types/tsx-element-types';

@Component
export default class Loader extends Vue {
  public _tsx!: WProps<
    this,
    'spinnerSize' | 'spinnerColor' | 'fullscreen' | 'customSpinnerSize' | 'showOverlay' | 'noCatchFocus'
  >;

  @Ref() private readonly loaderRef!: HTMLElement;

  @Prop({ default: () => LoaderSpinnerSizes.medium, required: false })
  public spinnerSize!: LoaderSpinnerSizes;

  @Prop({ default: () => LoaderSpinnerColors.red, required: false })
  public spinnerColor!: LoaderSpinnerColors;

  @Prop({ default: () => null })
  public customSpinnerSize!: ILoaderSpinnerSize | null;

  @Prop({ type: Boolean, default: () => false })
  public fullscreen!: boolean;

  @Prop({ type: Boolean, default: () => true })
  public showOverlay!: boolean;

  @Prop({ type: Boolean, default: () => false })
  public noCatchFocus!: boolean;

  private lastFocusedElement: HTMLElement | null = null;

  private spinnerSizeMap: Record<LoaderSpinnerSizes, ILoaderSpinnerSize> = {
    [LoaderSpinnerSizes.small]: { width: 20 },
    [LoaderSpinnerSizes.medium]: { width: 40 },
    [LoaderSpinnerSizes.large]: { width: 60 },
  };

  private spinnerColorMap: Record<LoaderSpinnerColors, string> = {
    [LoaderSpinnerColors.red]: '#e30611',
    [LoaderSpinnerColors.black]: '#001424',
    [LoaderSpinnerColors.green]: '#04aa42',
  };

  private get safeSpinnerSize(): ILoaderSpinnerSize {
    return this.customSpinnerSize ? this.customSpinnerSize : this.spinnerSizeMap[this.spinnerSize];
  }

  private get spinnerSizeStyles(): IElemStyles {
    return {
      width: `${this.safeSpinnerSize.width}px`,
      height: this.safeSpinnerSize.height
        ? `${this.safeSpinnerSize.height}px`
        : `${this.safeSpinnerSize.width}px`,
    };
  }

  private get loaderStyles(): IElemStyles {
    return {
      color: this.spinnerColorMap[this.spinnerColor],
    };
  }

  private get loaderClasses(): IElemClasses {
    return {
      class: {
        'loader--fullscreen': this.fullscreen,
      },
    };
  }

  private storeLastActiveElement(): void {
    this.lastFocusedElement = document.activeElement as HTMLElement;
  }

  private focusLastActiveElement(): void {
    if (this.lastFocusedElement) this.lastFocusedElement.focus();
  }

  private catchFocus(): void {
    this.loaderRef.focus();
  }

  private mounted(): void {
    if (this.fullscreen) this.$sl.lockScroll();
    if (!this.noCatchFocus) {
      this.storeLastActiveElement();
      this.catchFocus();
    }
  }

  private beforeDestroy(): void {
    if (this.fullscreen) this.$sl.unlockScroll();
    this.$nextTick(this.focusLastActiveElement);
  }

  private render(): VNode {
    return (
      <div class="loader" {...this.loaderClasses} ref="loaderRef" style={this.loaderStyles} tabindex={0}>
        {this.showOverlay ? <div class="loader__overlay" /> : null}
        <div class="loader__spinner" style={this.spinnerSizeStyles} />
      </div>
    );
  }
}
