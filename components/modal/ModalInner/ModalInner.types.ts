export enum ModalInnerSlots {
  header = 'header',
  content = 'content',
  footer = 'footer',
}
