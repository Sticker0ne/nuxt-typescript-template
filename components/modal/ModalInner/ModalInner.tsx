import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import './ModalInner.scss';
import AppScrollbar from '~/components/ui/AppScrollbar/AppScrollbar';
import { ModalInnerSlots } from '~/components/modal/ModalInner/ModalInner.types';

@Component
export default class ModalInner extends Vue {
  private render(): VNode {
    return (
      <div class="modal-inner">
        <div class="modal-inner__header">{this.$slots[ModalInnerSlots.header]}</div>
        <AppScrollbar class="modal-inner__scroll-container">
          <div class="modal-inner__content">{this.$slots[ModalInnerSlots.content]}</div>
        </AppScrollbar>
        <div class="modal-inner__footer">{this.$slots[ModalInnerSlots.footer]}</div>
      </div>
    );
  }
}
