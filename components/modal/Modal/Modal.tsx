import './Modal.scss';
import { Component, Prop, Ref, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import { IModalEvents, ModalSizes } from '~/components/modal/Modal/Modal.types';
import { catchTabInsideElement } from '~/utils/ElementUtils';
import { SVGNames } from '~/assets/ts/constants';
import type { IElemClasses, IElemStyles } from '~/types/global-types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class Modal extends Vue {
  public _tsx!: WProps<this, 'closeOutside' | 'closeButton' | 'closeOnEsc' | 'size' | 'customHeight'> &
    WEvents<IModalEvents>;

  @Ref() private readonly overlayRef!: HTMLElement;

  @Prop({ type: Boolean, default: () => true })
  public readonly closeOutside!: boolean;

  @Prop({ type: Boolean, default: () => true })
  public readonly closeButton!: boolean;

  @Prop({ type: Boolean, default: () => true })
  public readonly closeOnEsc!: boolean;

  @Prop({ default: () => ModalSizes.medium })
  public readonly size!: ModalSizes;

  @Prop({ default: () => 0 })
  public readonly customHeight!: number;

  private lastFocusedElement: HTMLElement | null = null;

  private emitClose(): void {
    emitOn(this, 'onClose');
  }

  private emitOpen(): void {
    emitOn(this, 'onOpen');
  }

  private get overlayClasses(): IElemClasses {
    return {
      class: {
        'modal-wrapper__overlay-clickable': this.closeOutside,
      },
    };
  }

  private sizeClassMap: Record<ModalSizes, string> = {
    [ModalSizes.small]: 'modal__container--small',
    [ModalSizes.medium]: 'modal__container--medium',
    [ModalSizes.large]: 'modal__container--large',
  };

  private get containerClasses(): IElemClasses {
    return {
      class: {
        [this.sizeClassMap[this.size]]: true,
      },
    };
  }

  private get containerStyles(): IElemStyles {
    return {
      height: this.customHeight > 0 ? `${this.customHeight}px` : 'auto',
    };
  }

  private onOverlayKeydown(event: KeyboardEvent): void {
    const code = event.code.toLowerCase();
    if (this.closeOnEsc && code === 'escape') this.emitClose();
  }

  private onOverlayClick(): void {
    if (this.closeOutside) this.emitClose();
  }

  private onCloseButton(): void {
    if (this.closeButton) this.emitClose();
  }

  private storeLastActiveElement(): void {
    this.lastFocusedElement = document.activeElement as HTMLElement;
  }

  private focusLastActiveElement(): void {
    if (this.lastFocusedElement) this.lastFocusedElement.focus();
  }

  private catchModalFocus(): void {
    const modalContainsFocusableElement = catchTabInsideElement(this.overlayRef);
    this.overlayRef.tabIndex = 0;
    if (!modalContainsFocusableElement) this.overlayRef.focus();
  }

  private mounted(): void {
    this.$sl.lockScroll();
    this.storeLastActiveElement();
    this.catchModalFocus();
    setTimeout(this.emitOpen);
  }

  private beforeDestroy(): void {
    this.$sl.unlockScroll();
    this.$nextTick(this.focusLastActiveElement);
  }

  private render(): VNode {
    return (
      <div class="modal">
        <div
          class="modal__overlay"
          {...this.overlayClasses}
          onKeydown={this.onOverlayKeydown}
          onClick={this.onOverlayClick}
          ref="overlayRef"
        >
          <div
            class="modal__container"
            {...this.containerClasses}
            style={this.containerStyles}
            onClick={event => event.stopImmediatePropagation()}
          >
            {this.$slots.default}
            {this.closeButton ? (
              <button class="modal__close" onClick={this.onCloseButton}>
                <svg-icon name={SVGNames.cross} />
              </button>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
