export interface IModalEvents {
  onClose: void;
  onOpen: void;
}

export enum ModalSizes {
  small = 'small',
  medium = 'medium',
  large = 'large',
}
