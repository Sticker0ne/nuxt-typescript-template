import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean, select, number } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import { ModalSizes } from '~/components/modal/Modal/Modal.types';
import Modal from '~/components/modal/Modal/Modal';
import AppButton from '~/components/ui/AppButton/AppButton';
import ModalInner from '~/components/modal/ModalInner/ModalInner';
import { ModalInnerSlots } from '~/components/modal/ModalInner/ModalInner.types';

storiesOf('Modals/Modal component', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    ...StoryContextAny,
    props: {
      closeOutside: {
        type: Boolean,
        default: () => boolean('closeOutside', true),
      },
      closeButton: {
        type: Boolean,
        default: () => boolean('closeButton', true),
      },
      closeOnEsc: {
        type: Boolean,
        default: () => boolean('closeOnEsc', true),
      },
      size: {
        type: String,
        default: () => select('size', ModalSizes, ModalSizes.medium),
      },
      customHeight: {
        default: () => number('customHeight', 400),
      },
    },
    data() {
      return {
        showModal: false,
      };
    },
    methods: {
      ...StoryContextAny,
      onClose() {
        action('onClose')();
        this.showModal = false;
      },
      onOpen() {
        action('onOpen')();
      },
      onShowModalBtnClick() {
        this.showModal = true;
      },
    },
    render() {
      return (
        <div>
          <AppButton onClick={this.onShowModalBtnClick}>ShowModal</AppButton>
          {this.showModal ? (
            <Modal
              closeOutside={this.closeOutside}
              closeButton={this.closeButton}
              closeOnEsc={this.closeOnEsc}
              customHeight={this.customHeight}
              size={this.size}
              onClose={this.onClose}
              onOpen={this.onOpen}
            >
              <ModalInner>
                <h2 slot={ModalInnerSlots.header}>Modal Header</h2>
                <div slot={ModalInnerSlots.content}>
                  <h4>Modal Content</h4>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                  labore et dolore magna aliqua. Posuere lorem ipsum dolor sit. Eget nunc lobortis mattis
                  aliquam faucibus purus in massa tempor. Libero id faucibus nisl tincidunt eget nullam non.
                  Tristique risus nec feugiat in fermentum posuere urna nec tincidunt. Nulla facilisi cras
                  fermentum odio. Dignissim suspendisse in est ante in nibh. Auctor eu augue ut lectus arcu.
                  Neque convallis a cras semper. Nunc id cursus metus aliquam eleifend mi in nulla. Leo vel
                  fringilla est ullamcorper eget. Cras tincidunt lobortis feugiat vivamus at augue eget arcu
                  dictum. Maecenas pharetra convallis posuere morbi leo urna molestie. Pulvinar mattis nunc
                  sed blandit libero volutpat. Mattis aliquam faucibus purus in. Sodales neque sodales ut
                  etiam sit amet nisl purus in. Aenean vel elit scelerisque mauris pellentesque pulvinar. Quis
                  risus sed vulputate odio ut. Justo eget magna fermentum iaculis eu non diam. Pellentesque
                  nec nam aliquam sem et tortor. Et egestas quis ipsum suspendisse ultrices. Tellus elementum
                  sagittis vitae et leo duis ut diam quam. Adipiscing elit pellentesque habitant morbi
                  tristique senectus et netus. Vel orci porta non pulvinar neque laoreet. Tincidunt vitae
                  semper quis lectus nulla at volutpat. Dignissim suspendisse in est ante in nibh. Duis
                  convallis convallis tellus id interdum velit laoreet id donec. Quam adipiscing vitae proin
                  sagittis nisl. Sit amet aliquam id diam maecenas ultricies mi eget. Sed vulputate odio ut
                  enim. A lacus vestibulum sed arcu. Eu feugiat pretium nibh ipsum consequat nisl. Egestas
                  diam in arcu cursus euismod. Dolor sed viverra ipsum nunc aliquet. Eu non diam phasellus
                  vestibulum lorem sed risus ultricies. Sagittis nisl rhoncus mattis rhoncus urna neque
                  viverra justo. Tincidunt lobortis feugiat vivamus at augue eget. Arcu felis bibendum ut
                  tristique et egestas. In hac habitasse platea dictumst quisque sagittis purus. Vestibulum
                  sed arcu non odio euismod. Adipiscing elit pellentesque habitant morbi tristique. Lacus
                  vestibulum sed arcu non odio euismod lacinia. Pellentesque diam volutpat commodo sed
                  egestas. Faucibus ornare suspendisse sed nisi lacus. Cras ornare arcu dui vivamus arcu.
                  Platea dictumst vestibulum rhoncus est pellentesque elit. Sapien eget mi proin sed libero
                  enim sed. Diam sit amet nisl suscipit adipiscing bibendum est ultricies. Pellentesque nec
                  nam aliquam sem et tortor. In fermentum posuere urna nec tincidunt praesent semper feugiat
                  nibh. Amet aliquam id diam maecenas. Adipiscing enim eu turpis egestas. Diam in arcu cursus
                  euismod quis. Arcu vitae elementum curabitur vitae. Et malesuada fames ac turpis egestas
                  maecenas pharetra convallis. Id porta nibh venenatis cras sed felis eget. Varius vel
                  pharetra vel turpis nunc eget lorem dolor. Viverra accumsan in nisl nisi scelerisque.
                </div>
                <div slot={ModalInnerSlots.footer}>
                  <AppButton>some btn</AppButton>
                </div>
              </ModalInner>
            </Modal>
          ) : null}
        </div>
      );
    },
  }));
