import { storiesOf } from '@storybook/vue';
import { withKnobs, text, number, button } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import { ToastUtils } from '~/utils/ToastUtils';

storiesOf('Other stories/Toasts', module)
  .addDecorator(withKnobs)
  .add('Default', () => ({
    ...StoryContextAny,
    props: {
      message: {
        type: String,
        default: () => text('message', 'default message'),
      },
      timeout: {
        type: Number,
        default: () => number('timeout', 1500),
      },
    },
    data() {
      return {
        showModal: false,
      };
    },
    methods: {
      ...StoryContextAny,
      onShowBtnClick() {
        ToastUtils.showDefaultToast({ message: this.message, timeout: this.timeout });
      },
    },
    render() {
      button('show toast', this.onShowBtnClick);
      return (
        <div>
          <div>{this.message}</div>
          <div>{this.timeout}</div>
        </div>
      );
    },
  }))
  .add('Error', () => ({
    ...StoryContextAny,
    props: {
      message: {
        type: String,
        default: () => text('message', 'default message'),
      },
      timeout: {
        type: Number,
        default: () => number('timeout', 1500),
      },
    },
    data() {
      return {
        showModal: false,
      };
    },
    methods: {
      ...StoryContextAny,
      onShowBtnClick() {
        ToastUtils.showErrorToast({ message: this.message, timeout: this.timeout });
      },
    },
    render() {
      button('show toast', this.onShowBtnClick);
      return (
        <div>
          <div>{this.message}</div>
          <div>{this.timeout}</div>
        </div>
      );
    },
  }))
  .add('Pending', () => ({
    ...StoryContextAny,
    props: {
      id: {
        type: String,
        default: () => text('id', 'default-id'),
      },
      message: {
        type: String,
        default: () => text('message', 'default message'),
      },
      timeout: {
        type: Number,
        default: () => number('timeout', 0),
      },
    },
    data() {
      return {
        showModal: false,
      };
    },
    methods: {
      ...StoryContextAny,
      onShowBtnClick() {
        ToastUtils.showPendingToast({ message: this.message, timeout: this.timeout, id: this.id });
      },
      onHideToastBtnClick() {
        ToastUtils.hideToastById(this.id);
      },
    },
    render() {
      button('show toast', this.onShowBtnClick);
      button('hide toast', this.onHideToastBtnClick);
      return (
        <div>
          <div>{this.id}</div>
          <div>{this.message}</div>
          <div>{this.timeout}</div>
        </div>
      );
    },
  }))
  .add('Rejected', () => ({
    ...StoryContextAny,
    props: {
      message: {
        type: String,
        default: () => text('message', 'default message'),
      },
      url: {
        type: String,
        default: () => text('url', 'https://miro.medium.com/max/3938/1*7pq_kbyKVX7bX6pPROuvHQ.png'),
      },
      title: {
        type: String,
        default: () => text('title', 'title'),
      },
      timeout: {
        type: Number,
        default: () => number('timeout', 1500),
      },
    },
    methods: {
      ...StoryContextAny,
      onShowBtnClick() {
        ToastUtils.showRejectedImportToast({
          message: this.message,
          timeout: this.timeout,
          url: this.url,
          title: this.title,
        });
      },
    },
    render() {
      button('show toast', this.onShowBtnClick);
      return (
        <div>
          <div>{this.message}</div>
          <div>{this.timeout}</div>
        </div>
      );
    },
  }));
