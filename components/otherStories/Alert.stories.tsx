import { storiesOf } from '@storybook/vue';
import { withKnobs, text, button } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { StoryContextAny } from '~/utils/StoryUtils';
import { AlertUtils } from '~/utils/AlertUtils';

storiesOf('Other stories/Alert', module)
  .addDecorator(withKnobs)
  .add('Default', () => ({
    ...StoryContextAny,
    props: {
      title: {
        type: String,
        default: () => text('title', 'default title'),
      },
      confirmButtonTitle: {
        type: String,
        default: () => text('confirmButtonTitle', 'confirm'),
      },
      cancelButtonTitle: {
        type: String,
        default: () => text('cancelButtonTitle', 'cancel'),
      },
      html: {
        type: String,
        default: () => text('html', `<span>some html</span>`),
      },
    },
    methods: {
      ...StoryContextAny,
      decodeHtml(codedHtml: string) {
        const doc = new DOMParser().parseFromString(codedHtml, 'text/html');
        return doc.documentElement.textContent;
      },
      onShowBtnClick() {
        console.log(this.html);
        AlertUtils.showDefaultAlert({
          title: this.title,
          confirmButtonTitle: this.confirmButtonTitle,
          cancelButtonTitle: this.cancelButtonTitle,
          html: this.decodeHtml(this.html),
        }).then(data => action('result')(data));
      },
    },
    render() {
      button('show alert', this.onShowBtnClick);
      return (
        <div>
          <div>{this.title}</div>
          <div>{this.confirmButtonTitle}</div>
          <div>{this.cancelButtonTitle}</div>
          <div>coded html from knob: {this.html}</div>
          <div>
            decoded html from knob this string will be passed to function: {this.decodeHtml(this.html)}
          </div>
        </div>
      );
    },
  }));
