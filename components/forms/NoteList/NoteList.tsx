import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import type { WProps } from '~/types/tsx-element-types';

@Component
export default class NoteList extends Vue {
  public _tsx!: WProps<this>;

  @Prop({ type: Array, default: () => [] })
  public notes!: string[];

  private render(): VNode {
    return (
      <ul class="notes-list">
        {this.notes.map((item: string, index: number) => (
          <li key={index} class="notes-list__item">
            {item}
          </li>
        ))}
      </ul>
    );
  }
}
