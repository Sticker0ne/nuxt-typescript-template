import { storiesOf } from '@storybook/vue';
import { withKnobs, object } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import NoteList from '~/components/forms/NoteList/NoteList';

storiesOf('Forms/NoteList', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    ...StoryContextAny,
    props: {
      notes: {
        type: Array,
        default: () => object('notes', ['Ошибка один', 'Подсказка один', 'Что-то еще']),
      },
    },
    render() {
      return <NoteList notes={this.notes} />;
    },
  }));
