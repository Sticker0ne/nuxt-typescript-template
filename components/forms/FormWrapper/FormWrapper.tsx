import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import './FormWrapper.scss';

@Component
export default class FormWrapper extends Vue {
  private render(): VNode {
    return <div class="form">{this.$slots.default}</div>;
  }
}
