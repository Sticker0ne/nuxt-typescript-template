import { Component, Vue } from 'nuxt-property-decorator';

@Component
export default class BaseForm extends Vue {
  protected touched = false;
  protected touch(): Promise<void> {
    this.touched = true;

    return new Promise<void>(resolve => {
      setTimeout(resolve, 10);
    });
  }

  protected get errorGetters(): any[][] {
    return [];
  }

  protected get formContainsErrors(): boolean {
    return this.errorGetters.some(getter => getter.length > 0);
  }
}
