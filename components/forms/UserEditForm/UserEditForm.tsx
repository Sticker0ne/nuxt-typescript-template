import { Component, Model, Prop } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import type {
  IUserEditFormEvents,
  IUserEditFormValue,
} from '~/components/forms/UserEditForm/UserEditForm.types';
import FormWrapper from '~/components/forms/FormWrapper/FormWrapper';
import Field from '~/components/forms/Field/Field';
import { FieldSlots } from '~/components/forms/Field/Field.types';
import AppInput from '~/components/ui/AppInput/AppInput';
import { AppInputModes } from '~/components/ui/AppInput/AppInput.types';
import AppRadioButtonsGroup from '~/components/ui/AppRadioButtonsGroup/AppRadioButtonsGroup';
import { IAppRadioButtonItem } from '~/components/ui/AppRadioButtonsGroup/AppRadioButtonsGroup.types';
import { stringToEnumValue } from '~/utils/CommonUtils';
import AppCheckbox from '~/components/ui/AppCheckbox/AppCheckbox';
import { UserEditFormGender } from '~/components/forms/UserEditForm/UserEditForm.types';
import NoteList from '~/components/forms/NoteList/NoteList';
import AppButton from '~/components/ui/AppButton/AppButton';
import { buildErrorList, numberIsBetween, stringIsNotEmpty, stringMinLength } from '~/utils/ValidationUtils';
import BaseForm from '~/components/forms/BaseForm/BaseForm';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class UserEditForm extends BaseForm {
  public _tsx!: WProps<this, 'disabled'> & WEvents<IUserEditFormEvents>;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Model('change', {
    default: () => ({}),
    required: true,
  })
  public value!: IUserEditFormValue;

  private emitChange(value: IUserEditFormValue): void {
    if (this.disabled) return;
    emitOn(this, 'onChange', value);
  }

  private get safeValue(): IUserEditFormValue {
    return this.value;
  }

  private set safeValue(newValue: IUserEditFormValue) {
    this.emitChange(newValue);
  }

  private emitSubmit(): void {
    if (this.disabled) return;
    emitOn(this, 'onSubmit', this.safeValue);
  }

  private get safeAge(): string {
    return (this.safeValue.age || '').toString();
  }

  private get genderItems(): IAppRadioButtonItem[] {
    return [
      {
        value: UserEditFormGender.male,
        title: this.$i18n.t('forms.userEditForm.fields.gender.radioButtonItems.male').toString(),
      },
      {
        value: UserEditFormGender.female,
        title: this.$i18n.t('forms.userEditForm.fields.gender.radioButtonItems.female').toString(),
      },
    ];
  }

  private get nameErrors(): string[] {
    if (!this.touched) return [];

    return buildErrorList([
      {
        error: this.$i18n.t('forms.userEditForm.fields.name.errors.required').toString(),
        show: !stringIsNotEmpty(this.safeValue.name),
      },
      {
        error: this.$i18n.tc('forms.userEditForm.fields.name.errors.minLength', 3, { length: 3 }),
        show: !stringMinLength(this.safeValue.name, 3),
      },
    ]);
  }

  private get lastNameErrors(): string[] {
    if (!this.touched) return [];

    return buildErrorList([
      {
        error: this.$i18n.t('forms.userEditForm.fields.lastName.errors.required').toString(),
        show: !stringIsNotEmpty(this.safeValue.lastName),
      },
      {
        error: this.$i18n.tc('forms.userEditForm.fields.lastName.errors.minLength', 3, { length: 3 }),
        show: !stringMinLength(this.safeValue.lastName, 3),
      },
    ]);
  }

  private get ageErrors(): string[] {
    if (!this.touched) return [];

    return buildErrorList([
      {
        error: this.$i18n.t('forms.userEditForm.fields.age.errors.required').toString(),
        show: !stringIsNotEmpty(this.safeAge),
      },
      {
        error: this.$i18n
          .t('forms.userEditForm.fields.age.errors.incorrectAge', { minValue: 5, maxValue: 100 })
          .toString(),
        show: !numberIsBetween(Number.parseInt(this.safeAge), 5, 100),
      },
    ]);
  }

  protected get errorGetters(): string[][] {
    return [this.nameErrors, this.lastNameErrors, this.ageErrors];
  }

  private get submitButtonDisabled(): boolean {
    return this.formContainsErrors || this.disabled;
  }

  private onNameChange(value: string): void {
    this.safeValue = { ...this.safeValue, name: value };
  }

  private onLastNameChange(value: string): void {
    this.safeValue = { ...this.safeValue, lastName: value };
  }

  private onAgeChange(value: string): void {
    const age = Number.parseInt(value);
    this.safeValue = { ...this.safeValue, age };
  }

  private onGenderChange(value: string): void {
    const parsedGender = stringToEnumValue(UserEditFormGender, value);
    if (parsedGender) this.safeValue = { ...this.safeValue, gender: parsedGender };
  }

  private onIsProgrammerChange(value: boolean): void {
    this.safeValue = { ...this.safeValue, isProgrammer: value };
  }

  private async onSubmitClick(): Promise<void> {
    await this.touch();
    if (!this.formContainsErrors) this.emitSubmit();
  }

  protected render(): VNode {
    return (
      <FormWrapper>
        <Field showErrors={!!this.nameErrors.length}>
          <h4 slot={FieldSlots.title}>{this.$i18n.t('forms.userEditForm.fields.name.title')}</h4>
          <AppInput
            disabled={this.disabled}
            slot={FieldSlots.content}
            value={this.safeValue.name}
            onInput={this.onNameChange}
            placeholder={this.$i18n.t('forms.userEditForm.fields.name.placeholder').toString()}
          />
          <NoteList slot={FieldSlots.errors} notes={this.nameErrors} />
        </Field>
        <Field showErrors={!!this.lastNameErrors.length}>
          <h4 slot={FieldSlots.title}>{this.$i18n.t('forms.userEditForm.fields.lastName.title')}</h4>
          <AppInput
            disabled={this.disabled}
            slot={FieldSlots.content}
            value={this.safeValue.lastName}
            onInput={this.onLastNameChange}
            placeholder={this.$i18n.t('forms.userEditForm.fields.lastName.placeholder').toString()}
          />
          <NoteList slot={FieldSlots.errors} notes={this.lastNameErrors} />
        </Field>
        <Field showErrors={!!this.ageErrors.length}>
          <h4 slot={FieldSlots.title}>{this.$i18n.t('forms.userEditForm.fields.age.title')}</h4>
          <AppInput
            disabled={this.disabled}
            slot={FieldSlots.content}
            value={this.safeAge}
            onInput={this.onAgeChange}
            placeholder={this.$i18n.t('forms.userEditForm.fields.age.placeholder').toString()}
            mode={AppInputModes.positiveInteger}
          />
          <NoteList slot={FieldSlots.errors} notes={this.ageErrors} />
        </Field>
        <Field showTips>
          <h4 slot={FieldSlots.title}>{this.$i18n.t('forms.userEditForm.fields.gender.title')}</h4>
          <AppRadioButtonsGroup
            disabled={this.disabled}
            slot={FieldSlots.content}
            value={this.safeValue.gender}
            onChange={this.onGenderChange}
            items={this.genderItems}
          />
          <NoteList slot={FieldSlots.tips} notes={['Если пол не подходит обратитесь в тех поддержку']} />
        </Field>
        <Field required={false} showTips>
          <AppCheckbox
            disabled={this.disabled}
            slot={FieldSlots.content}
            value={this.safeValue.isProgrammer}
            onChange={this.onIsProgrammerChange}
          >
            {this.$i18n.t('forms.userEditForm.fields.programmer.title')}
          </AppCheckbox>
          <NoteList slot={FieldSlots.tips} notes={['Если программист тыкните']} />
        </Field>
        <AppButton onClick={this.onSubmitClick} disabled={this.submitButtonDisabled}>
          {this.$i18n.t('forms.userEditForm.submitButtonTitle')}
        </AppButton>
      </FormWrapper>
    );
  }
}
