export enum UserEditFormGender {
  male = 'male',
  female = 'female',
}
export interface IUserEditFormValue {
  name: string;
  lastName: string;
  age: number;
  gender: UserEditFormGender;
  isProgrammer: boolean;
}

export interface IUserEditFormEvents {
  onChange: IUserEditFormValue;
  onSubmit: IUserEditFormValue;
}
