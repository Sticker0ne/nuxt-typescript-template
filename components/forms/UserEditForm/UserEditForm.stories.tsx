import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import { UserEditFormGender } from '~/components/forms/UserEditForm/UserEditForm.types';
import UserEditForm from '~/components/forms/UserEditForm/UserEditForm';
import { getI18nForStorybook } from '~/utils/StorybookUtils';

storiesOf('Forms/UserEditForm', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
    },
    data() {
      return {
        value: {
          name: '',
          lastName: '',
          age: 0,
          gender: UserEditFormGender.male,
          isProgrammer: false,
        },
      };
    },
    methods: {
      ...StoryContextAny,
      onChange(value: any) {
        this.value = value;
        action('onChange')(value);
      },
      onSubmit(value: any) {
        action('onSubmit')(value);
      },
    },
    render() {
      return (
        <UserEditForm
          value={this.value}
          onChange={this.onChange}
          onSubmit={this.onSubmit}
          disabled={this.disabled}
        />
      );
    },
  }))
  .add('One way binding', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
    },
    data() {
      return {
        value: {
          name: '',
          lastName: '',
          age: 0,
          gender: UserEditFormGender.male,
          isProgrammer: false,
        },
      };
    },
    methods: {
      ...StoryContextAny,
      onChange(value: any) {
        action('onChange')(value);
      },
      onSubmit(value: any) {
        action('onSubmit')(value);
      },
    },
    render() {
      return (
        <UserEditForm
          value={this.value}
          onChange={this.onChange}
          onSubmit={this.onSubmit}
          disabled={this.disabled}
        />
      );
    },
  }))
  .add('Filled data', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
    },
    data() {
      return {
        value: {
          name: 'Иван',
          lastName: 'Иванов',
          age: 25,
          gender: UserEditFormGender.male,
          isProgrammer: true,
        },
      };
    },
    methods: {
      ...StoryContextAny,
      onChange(value: any) {
        this.value = value;
        action('onChange')(value);
      },
      onSubmit(value: any) {
        action('onSubmit')(value);
      },
    },
    render() {
      return (
        <UserEditForm
          value={this.value}
          onChange={this.onChange}
          onSubmit={this.onSubmit}
          disabled={this.disabled}
        />
      );
    },
  }));
