import './Field.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import type { IElemClasses } from '~/types/global-types';
import { FieldSlots } from '~/components/forms/Field/Field.types';
import { getFocusableElementsInside } from '~/utils/ElementUtils';
import type { WProps } from '~/types/tsx-element-types';

@Component
export default class Field extends Vue {
  public _tsx!: WProps<this, 'showErrors' | 'showTips' | 'disabled' | 'required'>;

  @Prop({ type: Boolean, default: () => false })
  public showErrors!: boolean;

  @Prop({ type: Boolean, default: () => false })
  public showTips!: boolean;

  @Prop({ type: Boolean, default: () => false })
  public disabled!: boolean;

  @Prop({ type: Boolean, default: () => true })
  public required!: boolean;

  private get fieldClasses(): IElemClasses {
    return {
      class: {
        'field--disabled': this.disabled,
        'field--required': this.required,
        'field--has-error': this.showErrors,
      },
    };
  }

  private focusFirstElement(): void {
    const focusableElements = getFocusableElementsInside(this.$el);
    if (!focusableElements.length) return;
    focusableElements[0].focus();
  }

  private onTitleClick(): void {
    this.focusFirstElement();
  }

  private render(): VNode {
    return (
      <div class="field" {...this.fieldClasses}>
        <div class="field__title" onClick={this.onTitleClick}>
          {this.$slots[FieldSlots.title]}
        </div>
        <div class="field__content">{this.$slots[FieldSlots.content]}</div>
        <div class="field__tips">{this.showTips ? this.$slots[FieldSlots.tips] : null}</div>
        <div class="field__errors">{this.showErrors ? this.$slots[FieldSlots.errors] : null}</div>
      </div>
    );
  }
}
