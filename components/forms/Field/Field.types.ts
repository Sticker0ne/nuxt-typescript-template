export enum FieldSlots {
  title = 'title',
  content = 'content',
  tips = 'tips',
  errors = 'errors',
}
