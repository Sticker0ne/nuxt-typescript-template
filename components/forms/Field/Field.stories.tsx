import { storiesOf } from '@storybook/vue';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { StoryContextAny } from '~/utils/StoryUtils';
import Field from '~/components/forms/Field/Field';
import AppInput from '~/components/ui/AppInput/AppInput';
import { getI18nForStorybook } from '~/utils/StorybookUtils';

storiesOf('Forms/Field', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      showErrors: {
        type: Boolean,
        default: () => boolean('showErrors', false),
      },
      showTips: {
        type: Boolean,
        default: () => boolean('showTips', false),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      required: {
        type: Boolean,
        default: () => boolean('required', false),
      },
    },
    methods: {
      ...StoryContextAny,
      onFirstInput(value: boolean) {
        this.firstInputValue = value;
        action('onInput')(value);
      },
      onSecondInput(value: boolean) {
        this.secondInputValue = value;
        action('onInput')(value);
      },
      onThirdInput(value: boolean) {
        this.thirdInputValue = value;
        action('onInput')(value);
      },
    },
    data() {
      return {
        firstInputValue: '',
        secondInputValue: '',
        thirdInputValue: '',
        errorItems: ['Неверный формат', 'Заполните это поле', 'Нет такого значения'],
        tipsItems: ['Номер должен начинаться с +7 или 8', 'Пароль должен состоять из латинских букв и цифр'],
      };
    },
    render() {
      return (
        <Field
          showErrors={this.showErrors}
          showTips={this.showTips}
          disabled={this.disabled}
          required={this.required}
          class="form-section"
        >
          <h4 slot="title" class="form-section__title">
            Заголовок поля
          </h4>
          <div slot="content" class="form-section__content">
            <div class="form-section__row">
              <AppInput
                value={this.secondInputValue}
                onInput={this.onSecondInput}
                disabled={this.disabled}
                placeholder="Введите что-нибудь"
                class="form-section__element"
              />
              <AppInput
                value={this.thirdInputValue}
                onInput={this.onThirdInput}
                disabled={this.disabled}
                placeholder="Введите что-нибудь"
                class="form-section__element"
              />
            </div>
            <AppInput
              value={this.firstInputValue}
              onInput={this.onFirstInput}
              disabled={this.disabled}
              placeholder="Введите что-нибудь"
              class="form-section__element"
            />
          </div>
          <div slot="errors" class="form-section__errors">
            <ul class="notes-list">
              {this.errorItems.map((item: string, index: number) => (
                <li key={index} class="notes-list__item">
                  {item}
                </li>
              ))}
            </ul>
          </div>
          <div slot="tips" class="form-section__tips">
            <ul class="notes-list">
              {this.tipsItems.map((item: string, index: number) => (
                <li key={index} class="notes-list__item">
                  {item}
                </li>
              ))}
            </ul>
          </div>
        </Field>
      );
    },
  }));
