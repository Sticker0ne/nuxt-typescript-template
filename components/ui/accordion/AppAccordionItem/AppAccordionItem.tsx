import './AppAccordionItem.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import AppAccordionList from '~/components/ui/accordion/AppAccordionList/AppAccordionList';
import { AppAccordionItemSlots } from '~/components/ui/accordion/AppAccordionItem/AppAccordionItem.types';
import { SVGNames } from '~/assets/ts/constants';
import type { IElemStyles } from '~/types/global-types';
import type { WProps } from '~/types/tsx-element-types';

@Component
export default class AppAccordionItem extends Vue {
  public _tsx!: WProps<this, 'autoCalculateHeight' | 'defaultOpen'>;

  @Prop({ type: Boolean, default: () => true })
  public autoCalculateHeight!: boolean;

  @Prop({ type: Boolean, default: () => false })
  public defaultOpen!: boolean;

  private callParentRecalculateHeight(): void {
    if (!this.autoCalculateHeight) return;

    const accordionList = this.$parent as AppAccordionList;
    if (!accordionList || !accordionList.calculatePanelsHeightDebounced) return;
    accordionList.calculatePanelsHeightDebounced();
  }

  private updated(): void {
    if (this.autoCalculateHeight) setTimeout(this.callParentRecalculateHeight);
  }

  private get itemStyles(): IElemStyles {
    return !this.defaultOpen && !(this.$parent as AppAccordionList).initialized
      ? {
          'max-height': '0 !important',
          overflow: 'hidden',
        }
      : {};
  }

  private render(): VNode {
    const icon = this.$slots[AppAccordionItemSlots.icon] || <svg-icon name={SVGNames.arrow} />;
    return (
      <fragment>
        <dt class="accordion-item-header">
          <button class="accordion-item-header__button js-badger-accordion-header">
            <span class="accordion-item-header__title">{this.$slots[AppAccordionItemSlots.header]}</span>
            <span class="accordion-item-header__icon">{icon}</span>
          </button>
        </dt>
        <dd class="accordion-item-content js-badger-accordion-panel" style={this.itemStyles}>
          <div class="accordion-item-content__panel-inner js-badger-accordion-panel-inner">
            {this.$slots[AppAccordionItemSlots.content]}
          </div>
        </dd>
      </fragment>
    );
  }
}
