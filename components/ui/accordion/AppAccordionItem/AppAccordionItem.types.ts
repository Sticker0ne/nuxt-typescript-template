export enum AppAccordionItemSlots {
  header = 'header',
  content = 'content',
  icon = 'icon',
}
