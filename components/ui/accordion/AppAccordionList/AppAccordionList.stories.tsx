import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean, number, button } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppAccordionList from '~/components/ui/accordion/AppAccordionList/AppAccordionList';
import AppAccordionItem from '~/components/ui/accordion/AppAccordionItem/AppAccordionItem';
import { AppAccordionItemSlots } from '~/components/ui/accordion/AppAccordionItem/AppAccordionItem.types';
import { SVGNames } from '~/assets/ts/constants';
import AppScrollbar from '~/components/ui/AppScrollbar/AppScrollbar';

storiesOf('ui components/AppAccordion', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    ...StoryContextAny,
    props: {
      openMultiplePanels: {
        type: Boolean,
        default: () => boolean('openMultiplePanels', false),
      },
      calculateOnSelfUpdate: {
        type: Boolean,
        default: () => boolean('calculateOnSelfUpdate', true),
      },
      calculateOnResize: {
        type: Boolean,
        default: () => boolean('calculateOnResize', true),
      },
      calculateDebounceTime: {
        type: Number,
        default: () => number('calculateDebounceTime', 50),
      },
    },
    data() {
      return {
        showAccordion: true,
      };
    },
    mounted() {
      setTimeout(this.$startWatchWidth, 200);
    },
    methods: {
      ...StoryContextAny,
      onResetBtnClick() {
        this.showAccordion = false;
        setTimeout(() => {
          this.showAccordion = true;
        }, 100);
      },
      onCalculate() {
        action('onCalculatePanelsHeight')();
      },
      onInit() {
        action('onInit')();
      },
    },
    render() {
      button('Reload Accordion', this.onResetBtnClick);
      return (
        <div>
          {this.showAccordion ? (
            <AppAccordionList
              style="width: 400px;"
              openMultiplePanels={this.openMultiplePanels}
              calculateHeightOnSelfUpdate={this.calculateOnSelfUpdate}
              calculateHeightOnResize={this.calculateOnResize}
              calculateHeightDebounceTime={this.calculateDebounceTime}
              onInit={this.onInit}
              onCalculatePanelsHeight={this.onCalculate}
            >
              <AppAccordionItem>
                <span slot={AppAccordionItemSlots.header}>first header</span>
                <div slot={AppAccordionItemSlots.content}>first content</div>
              </AppAccordionItem>
              <AppAccordionItem defaultOpen>
                <span slot={AppAccordionItemSlots.header}>second header</span>
                <span slot={AppAccordionItemSlots.icon}>
                  <svg-icon name={SVGNames.plus} />
                </span>
                <div slot={AppAccordionItemSlots.content}>second content</div>
              </AppAccordionItem>
              <AppAccordionItem>
                <span slot={AppAccordionItemSlots.header}>third header</span>
                <div slot={AppAccordionItemSlots.content}>
                  <div>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
                    sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                    vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                    Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
                    elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
                    consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat
                    a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean
                    imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget
                    dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper
                    libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus
                    pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae
                    sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros
                    faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis
                    magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,
                  </div>
                </div>
              </AppAccordionItem>
              <AppAccordionItem>
                <span slot={AppAccordionItemSlots.header}>fourth header</span>
                <div slot={AppAccordionItemSlots.content}>
                  <AppScrollbar style="height: 100px">
                    <div>
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                      dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                      nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
                      sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                      vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                      Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
                      elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor
                      eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis,
                      feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                      Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                      Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam
                      semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel,
                      luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec
                      vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget
                      eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales
                      sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,
                    </div>
                  </AppScrollbar>
                </div>
              </AppAccordionItem>
              <AppAccordionItem>
                <span slot={AppAccordionItemSlots.header}>fifth header</span>
                <div slot={AppAccordionItemSlots.content}>fifth content</div>
              </AppAccordionItem>
            </AppAccordionList>
          ) : null}
        </div>
      );
    },
  }));
