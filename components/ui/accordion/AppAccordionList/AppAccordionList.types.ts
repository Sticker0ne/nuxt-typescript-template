export interface IAppAccordionListEvents {
  onCalculatePanelsHeight: void;
  onInit: void;
}
