import './AppAccordionList.scss';
import { Component, Prop, Vue, Watch } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import debounce from 'lodash.debounce';
import { castTo } from '~/utils/CommonUtils';
import type { IAppAccordionListEvents } from '~/components/ui/accordion/AppAccordionList/AppAccordionList.types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppAccordionList extends Vue {
  public _tsx!: WProps<
    this,
    | 'openMultiplePanels'
    | 'calculateHeightOnSelfUpdate'
    | 'calculateHeightOnResize'
    | 'calculateHeightDebounceTime',
    'initialized' | 'accordion' | 'calculatePanelsHeightDebounced'
  > &
    WEvents<IAppAccordionListEvents>;

  @Prop({ type: Boolean, default: () => false })
  public openMultiplePanels!: boolean;

  @Prop({ type: Boolean, default: () => true })
  public calculateHeightOnSelfUpdate!: boolean;

  @Prop({ type: Boolean, default: () => true })
  public calculateHeightOnResize!: boolean;

  @Prop({ default: () => 50 })
  public calculateHeightDebounceTime!: number;

  private emitInit(): void {
    emitOn(this, 'onInit');
  }

  private emitCalculatePanelsHeight(): void {
    emitOn(this, 'onCalculatePanelsHeight');
  }

  public accordion: BadgerAccordion = castTo(null);
  private BadgerAccordionImported: typeof BadgerAccordion | null = null;
  public initialized = false;

  private async initAccordionInstance(): Promise<void> {
    this.initialized = false;
    const { openMultiplePanels } = this;
    if (!this.BadgerAccordionImported)
      this.BadgerAccordionImported = await import('badger-accordion').then(module => module.default);

    const BadgerAccordion = this.BadgerAccordionImported;
    if (BadgerAccordion) this.accordion = new BadgerAccordion(this.$el, { openMultiplePanels });

    setTimeout(this.openDefaultOpenPanels);
    setTimeout(this.emitInit);
    setTimeout(() => {
      this.initialized = true;
    }, 1);
  }

  public calculatePanelsHeightDebounced: (() => any) | null = null;

  private calculatePanelsHeight(): void {
    if (!this.accordion || !this.accordion.calculateAllPanelsHeight) return;
    this.accordion.calculateAllPanelsHeight();
    setTimeout(this.emitCalculatePanelsHeight);
  }

  private openDefaultOpenPanels(): void {
    let firstPanelWasOpened = false;
    const children = this.$children as any[];

    children.forEach((child, index) => {
      if (!child.defaultOpen) return;
      if (!firstPanelWasOpened) {
        this.accordion.open(index);
        firstPanelWasOpened = true;
      } else if (this.openMultiplePanels) {
        this.accordion.open(index);
      }
    });
  }

  private mounted(): void {
    this.$nextTick(this.initAccordionInstance);
    this.calculatePanelsHeightDebounced = debounce(
      this.calculatePanelsHeight,
      this.calculateHeightDebounceTime,
    );
  }

  @Watch('openMultiplePanels')
  private onOpenMultiplePanelsChange(): void {
    if (!this.accordion) return;
    this.initAccordionInstance();
  }

  @Watch('$ww.actualWidth')
  private onActualWidthChange(): void {
    if (!this.calculateHeightOnResize) return;
    if (!this.calculatePanelsHeightDebounced) return;
    this.calculatePanelsHeightDebounced();
  }

  private render(): VNode {
    return <dl class="accordion-list js-badger-accordion">{this.$slots.default}</dl>;
  }
}
