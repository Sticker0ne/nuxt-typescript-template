export interface IAppTabBarItem {
  title: string;
  value: string;
  href?: string;
}

export interface IAppTabBarEvents {
  onChange: string;
}
