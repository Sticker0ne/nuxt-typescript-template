import './AppTabBar.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import type { IAppTabBarEvents, IAppTabBarItem } from '~/components/ui/AppTabBar/AppTabBar.types';
import type { IElemClasses } from '~/types/global-types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppTabBar extends Vue {
  public _tsx!: WProps<this, 'value'> & WEvents<IAppTabBarEvents>;

  @Prop({ default: () => [], required: true })
  public readonly items!: IAppTabBarItem[];

  @Prop({ default: () => '', required: false })
  public readonly value!: string;

  private emitChange(value: string): void {
    emitOn(this, 'onChange', value);
  }

  private buildItemClasses(item: IAppTabBarItem): IElemClasses {
    return {
      class: {
        'tab-bar__item--active': this.value === item.value,
      },
    };
  }

  private onItemClick(item: IAppTabBarItem): void {
    this.emitChange(item.value);
  }

  private render(): VNode {
    return (
      <div class="tab-bar">
        {this.items.map(item => {
          const link = item.href ? <a href={item.href} onClick={e => e.preventDefault()} /> : null;
          return (
            <div
              class="tab-bar__item"
              {...this.buildItemClasses(item)}
              onClick={() => this.onItemClick(item)}
            >
              {item.title}
              {link}
            </div>
          );
        })}
      </div>
    );
  }
}
