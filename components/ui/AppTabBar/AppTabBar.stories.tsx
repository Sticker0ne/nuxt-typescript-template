import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, object, text } from '@storybook/addon-knobs';
import AppTabBar from './AppTabBar';
import { StoryContextAny } from '~/utils/StoryUtils';

storiesOf('UI components/AppTabBar', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    props: {
      items: {
        type: Array,
        default: () =>
          object('items', [
            { title: 'First tab', value: '1', href: '/tab1', show: true },
            { title: 'Second tab', value: '2', href: '/tab2', show: true },
            { title: 'Third tab', value: '3', href: '/tab3', show: true },
          ]),
      },
    },
    data() {
      return {
        value: '2',
      };
    },
    methods: {
      ...StoryContextAny,
      onChange(value: string) {
        this.value = value;
        action('onChange')(value);
      },
    },
    render() {
      return <AppTabBar items={this.items} value={this.value} onChange={this.onChange} />;
    },
  }))
  .add('One way binding', () => ({
    ...StoryContextAny,
    props: {
      items: {
        type: Array,
        default: () =>
          object('items', [
            { title: 'First tab', value: '1', href: '/tab1' },
            { title: 'Second tab', value: '2', href: '/tab2' },
            { title: 'Third tab', value: '3', href: '/tab3' },
          ]),
      },
      value: {
        type: String,
        default: () => text('value', '2'),
      },
    },
    methods: {
      ...StoryContextAny,
      onChange(value: string) {
        action('onChange')(value);
      },
    },
    render() {
      return <AppTabBar items={this.items} value={this.value} onChange={this.onChange} />;
    },
  }));
