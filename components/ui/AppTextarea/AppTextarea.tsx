import { Component } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import { SVGNames } from '~/assets/ts/constants';
import AppInput from '~/components/ui/AppInput/AppInput';

@Component
export default class AppTextarea extends AppInput {
  protected render(): VNode {
    return (
      <div class="form-element form-element--textarea">
        <textarea
          value={this.safeValue}
          onInput={this.onInput}
          ref="inputElement"
          disabled={this.disabled}
          placeholder={this.placeholder}
          class="form-element__field"
          onBlur={this.onInputBlur}
          {...{ directives: this.inputDynamicDirectives }}
        />
        {this.shouldShowClearButton ? (
          <div class="form-element__button-clear" onClick={this.clearValue}>
            <svg-icon name={SVGNames.cross} class="form-element__clear-icon" />
          </div>
        ) : null}
      </div>
    );
  }
}
