import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, button, number } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppTextarea from '~/components/ui/AppTextarea/AppTextarea';

storiesOf('UI components/AppTextArea', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    props: {
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        this.value = value;
        action('onInput')(value);
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppTextarea
          value={this.value}
          onInput={this.onInput}
          disabled={this.disabled}
          placeholder={this.placeholder}
          showClearButton={this.showClearButton}
        />
      );
    },
  }))
  .add('One way binding', () => ({
    ...StoryContextAny,
    props: {
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        action('onInput')(value);
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppTextarea
          value={this.value}
          onInput={this.onInput}
          disabled={this.disabled}
          placeholder={this.placeholder}
          showClearButton={this.showClearButton}
        />
      );
    },
  }))
  .add('Max length', () => ({
    ...StoryContextAny,
    props: {
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
      maxLength: {
        type: Number,
        default: () => number('maxLength', 0),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        this.value = value;
        action('onInput')(value);
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppTextarea
          value={this.value}
          onInput={this.onInput}
          disabled={this.disabled}
          placeholder={this.placeholder}
          showClearButton={this.showClearButton}
          maxLength={this.maxLength}
        />
      );
    },
  }));
