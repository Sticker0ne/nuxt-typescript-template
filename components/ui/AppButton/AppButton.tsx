import './AppButton.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import type { IAppButtonEvents } from '~/components/ui/AppButton/AppButton.types';
import { AppButtonColors, AppButtonSizes, AppButtonSlots } from '~/components/ui/AppButton/AppButton.types';
import type { IElemClasses } from '~/types/global-types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppButton extends Vue {
  public _tsx!: WProps<this, 'color' | 'isLoading' | 'disabled' | 'size'> & WEvents<IAppButtonEvents>;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly isLoading!: boolean;

  @Prop({ default: () => AppButtonColors.red, required: false })
  public readonly color!: AppButtonColors;

  @Prop({ default: () => AppButtonSizes.default, required: false })
  public readonly size!: AppButtonSizes;

  private emitClick(event: MouseEvent): void {
    if (!this.disabled) emitOn(this, 'onClick', event);
  }

  private colorClassMap: Record<AppButtonColors, string> = {
    [AppButtonColors.red]: 'button--red',
    [AppButtonColors.white]: 'button--white',
    [AppButtonColors.blue]: 'button--blue',
    [AppButtonColors.transparent]: 'button--transparent',
  };

  private sizeClassMap: Record<AppButtonSizes, string> = {
    [AppButtonSizes.default]: '',
    [AppButtonSizes.large]: 'button--large',
  };

  private get buttonClasses(): IElemClasses {
    return {
      class: {
        [this.colorClassMap[this.color]]: true,
        [this.sizeClassMap[this.size]]: true,
        'button--icon': this.isOnlyIconBtn,
      },
    };
  }

  private get isOnlyIconBtn(): boolean {
    return !this.$slots[AppButtonSlots.default];
  }

  private get buttonInnerClasses(): IElemClasses {
    return {
      class: {
        'button__inner--hidden': this.isLoading,
      },
    };
  }

  private get iconClasses(): IElemClasses {
    return {
      class: {
        'button__icon--left': !this.isOnlyIconBtn && !!this.$slots[AppButtonSlots.iconLeft],
        'button__icon--right': !this.isOnlyIconBtn && !!this.$slots[AppButtonSlots.iconRight],
      },
    };
  }

  private render(): VNode {
    return (
      <button class="button" {...this.buttonClasses} disabled={this.disabled} onClick={this.emitClick}>
        {this.isLoading ? <span class="button__loader" /> : null}
        <span class="button__inner" {...this.buttonInnerClasses}>
          {this.$slots[AppButtonSlots.iconLeft] ? (
            <span class="button__icon" {...this.iconClasses}>
              {this.$slots[AppButtonSlots.iconLeft]}
            </span>
          ) : null}
          <span class="button__text">{this.$slots[AppButtonSlots.default]}</span>
          {this.$slots[AppButtonSlots.iconRight] ? (
            <span class="button__icon" {...this.iconClasses}>
              {this.$slots[AppButtonSlots.iconRight]}
            </span>
          ) : null}
        </span>
      </button>
    );
  }
}
