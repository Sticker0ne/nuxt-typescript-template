export interface IAppButtonEvents {
  onClick: MouseEvent;
}
export enum AppButtonSlots {
  default = 'default',
  iconLeft = 'iconLeft',
  iconRight = 'iconRight',
}

export enum AppButtonColors {
  red = 'red',
  white = 'white',
  blue = 'blue',
  transparent = 'transparent',
}

export enum AppButtonSizes {
  default = 'default',
  large = 'large',
}
