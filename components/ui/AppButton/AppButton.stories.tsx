import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppButton from '~/components/ui/AppButton/AppButton';
import { AppButtonColors, AppButtonSizes, AppButtonSlots } from '~/components/ui/AppButton/AppButton.types';
import { SVGNames } from '~/assets/ts/constants';

storiesOf('UI components/AppButton', module)
  .addDecorator(withKnobs)
  .add('Default', () => ({
    ...StoryContextAny,
    props: {
      title: {
        type: String,
        default: () => text('title', 'AppButton'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      loading: {
        type: Boolean,
        default: () => boolean('loading', false),
      },
      color: {
        type: String,
        default: () => select('color', AppButtonColors, AppButtonColors.blue),
      },
      size: {
        type: String,
        default: () => select('size', AppButtonSizes, AppButtonSizes.default),
      },
    },
    methods: {
      ...StoryContextAny,
      onClick(event: MouseEvent) {
        action('onChange')(event);
      },
    },
    render() {
      return (
        <AppButton
          disabled={this.disabled}
          isLoading={this.loading}
          onClick={this.onClick}
          color={this.color}
          size={this.size}
        >
          {this.title}
        </AppButton>
      );
    },
  }))
  .add('Icon', () => ({
    ...StoryContextAny,
    props: {
      title: {
        type: String,
        default: () => text('title', 'AppButton'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      loading: {
        type: Boolean,
        default: () => boolean('loading', false),
      },
      color: {
        type: String,
        default: () => select('color', AppButtonColors, AppButtonColors.transparent),
      },
      size: {
        type: String,
        default: () => select('size', AppButtonSizes, AppButtonSizes.default),
      },
    },
    methods: {
      ...StoryContextAny,
      onClick(event: MouseEvent) {
        action('onChange')(event);
      },
    },
    render() {
      return (
        <AppButton
          disabled={this.disabled}
          isLoading={this.loading}
          onClick={this.onClick}
          color={this.color}
          size={this.size}
        >
          <svg-icon slot={AppButtonSlots.iconLeft} name={SVGNames.cross} style="width:20px; height: 20px;" />
        </AppButton>
      );
    },
  }))
  .add('Text + Icon', () => ({
    ...StoryContextAny,
    props: {
      title: {
        type: String,
        default: () => text('title', 'AppButton'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      loading: {
        type: Boolean,
        default: () => boolean('loading', false),
      },
      color: {
        type: String,
        default: () => select('color', AppButtonColors, AppButtonColors.red),
      },
      size: {
        type: String,
        default: () => select('size', AppButtonSizes, AppButtonSizes.default),
      },
    },
    methods: {
      ...StoryContextAny,
      onClick(event: MouseEvent) {
        action('onChange')(event);
      },
    },
    render() {
      return (
        <AppButton
          disabled={this.disabled}
          isLoading={this.loading}
          onClick={this.onClick}
          color={this.color}
          size={this.size}
        >
          {this.title}
          <svg-icon slot={AppButtonSlots.iconRight} name={SVGNames.cross} style="width:16px; height: 16px;" />
        </AppButton>
      );
    },
  }));
