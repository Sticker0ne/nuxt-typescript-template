import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, button } from '@storybook/addon-knobs';
import AppCheckbox from '~/components/ui/AppCheckbox/AppCheckbox';
import { StoryContextAny } from '~/utils/StoryUtils';

storiesOf('UI components/AppCheckbox', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    props: {
      title: {
        type: String,
        default: () => text('title', 'AppCheckbox'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      switch: {
        type: Boolean,
        default: () => boolean('switch', true),
      },
    },
    data() {
      return {
        value: true,
      };
    },
    methods: {
      ...StoryContextAny,
      onChange(value: boolean) {
        this.value = value;
        action('onChange')(value);
      },
    },
    render() {
      button('set to false', () => {
        this.value = false;
      });
      button('set to true', () => {
        this.value = true;
      });
      return (
        <AppCheckbox
          value={this.value}
          onChange={this.onChange}
          disabled={this.disabled}
          switch={this.switch}
        >
          {this.title}
        </AppCheckbox>
      );
    },
  }))
  .add('One way binding', () => ({
    ...StoryContextAny,
    onChange(value: boolean) {
      action('onChange')(value);
    },
    props: {
      title: {
        type: String,
        default: () => text('title', 'AppCheckbox'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      switch: {
        type: Boolean,
        default: () => boolean('switch', true),
      },
    },
    data() {
      return {
        value: true,
      };
    },
    render() {
      button('set to false', () => {
        this.value = false;
      });
      button('set to true', () => {
        this.value = true;
      });
      return (
        <AppCheckbox
          value={this.value}
          disabled={this.disabled}
          switch={this.switch}
          onChange={this.onChange}
        >
          {this.title}
        </AppCheckbox>
      );
    },
  }));
