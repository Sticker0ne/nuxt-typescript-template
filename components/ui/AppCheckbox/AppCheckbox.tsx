import './AppCheckbox.scss';
import { Component, Model, Prop, Ref, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import type { IAppCheckboxEvents } from '~/components/ui/AppCheckbox/AppCheckbox.types';
import type { IElemClasses } from '~/types/global-types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppCheckbox extends Vue {
  public _tsx!: WProps<this, 'disabled' | 'switch'> & WEvents<IAppCheckboxEvents>;

  @Ref() private readonly checkboxInput!: HTMLInputElement;

  @Model('change', {
    type: Boolean,
    required: true,
  })
  public readonly value!: boolean;

  private emitChange(value: boolean): void {
    emitOn(this, 'onChange', value);
  }

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly switch!: boolean;

  private get checkboxClasses(): IElemClasses {
    return {
      class: {
        'checkbox--simple': !this.switch,
        'checkbox--switch': this.switch,
      },
    };
  }

  private get safeValue(): boolean {
    return this.value;
  }

  private set safeValue(newValue: boolean) {
    if (!this.disabled) this.emitChange(newValue);
    this.$nextTick(this.synchronizeValue);
  }

  private synchronizeValue(): void {
    this.checkboxInput.checked = this.safeValue;
  }

  private render(): VNode {
    return (
      <div class="checkbox" {...this.checkboxClasses}>
        <label class="checkbox__label">
          <input ref="checkboxInput" v-model={this.safeValue} type="checkbox" disabled={this.disabled} />
          <span class="checkbox__wrapper">
            <span class="checkbox__place">{!this.switch ? <span class="checkbox__place-icon" /> : null}</span>
            <span class="checkbox__info">{this.$slots.default}</span>
          </span>
        </label>
      </div>
    );
  }
}
