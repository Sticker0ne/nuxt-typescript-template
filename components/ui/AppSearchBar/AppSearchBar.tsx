import './AppSearchBar.scss';
import { Component, Model, Prop, Ref, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import debounce from 'lodash.debounce';
import AppInput from '~/components/ui/AppInput/AppInput';
import AppButton from '~/components/ui/AppButton/AppButton';
import type { IAppSearchBarEvents } from '~/components/ui/AppSearchBar/AppSearchBar.types';
import type { WEvents, WProps } from '~/types/tsx-element-types';
import type { IElemClasses } from '~/types/global-types';
import { SVGNames } from '~/assets/ts/constants';
import { AppButtonSlots } from '~/components/ui/AppButton/AppButton.types';

@Component
export default class AppSearchBar extends Vue {
  public _tsx!: WProps<
    this,
    'placeholder' | 'debounceTime' | 'showClearButton' | 'showSearchButton' | 'isButtonLoading'
  > &
    WEvents<IAppSearchBarEvents>;

  @Ref() private appInputElement!: AppInput;

  @Prop({ default: () => '', required: false })
  public readonly placeholder!: string;

  @Prop({ type: Boolean, default: () => true, required: false })
  public readonly showClearButton!: boolean;

  @Prop({ type: Boolean, default: () => true, required: false })
  public readonly showSearchButton!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly isButtonLoading!: boolean;

  @Prop({ default: () => 800, required: false })
  public readonly debounceTime!: number;

  @Model('input', {
    default: () => '',
    required: true,
  })
  public readonly value!: string;

  private emitInput(value: string): void {
    emitOn(this, 'onInput', value);
  }

  private emitSearch(value: string): void {
    emitOn(this, 'onSearch', value);
  }

  private emitEnter(value: string): void {
    emitOn(this, 'onEnter', value);
  }

  private emitButtonClick(value: string): void {
    emitOn(this, 'onSearchButtonClick', value);
  }

  private get searchBarClasses(): IElemClasses {
    return {
      class: {
        'search-bar--with-button': this.showSearchButton,
      },
    };
  }

  private lastEmitFromInput = false;
  private emitSearchFromInputDebounced = debounce(this.emitSearchFromInput, this.debounceTime);
  private emitSearchFromButtonAndEnterDebounced = debounce(
    this.emitSearchFromButtonAndEnter,
    this.debounceTime,
    {
      leading: true,
      trailing: false,
    },
  );

  private emitSearchFromInput(value: string): void {
    if (this.lastEmitFromInput) this.emitSearch(value);
  }

  private emitSearchFromButtonAndEnter(value: string, from?: 'button' | 'enter' | undefined): void {
    this.lastEmitFromInput = false;
    this.emitSearch(value);

    if (from === 'button') this.emitButtonClick(value);
    if (from === 'enter') this.emitEnter(value);
  }

  private onInput(value: string): void {
    this.emitInput(value);
    this.lastEmitFromInput = true;
    setTimeout(() => this.emitSearchFromInputDebounced(this.value));
  }

  private onSearchButtonClick(): void {
    this.appInputElement.inputElement.focus();
    this.emitSearchFromButtonAndEnterDebounced(this.value, 'button');
  }

  private onEnter(): void {
    this.emitSearchFromButtonAndEnterDebounced(this.value, 'enter');
  }

  private onInputKeyDown(event: KeyboardEvent): void {
    if (event.key.toLowerCase() === 'enter') this.onEnter();
  }

  private mounted(): void {
    this.appInputElement.inputElement.addEventListener('keydown', this.onInputKeyDown);
  }

  private beforeDestroy(): void {
    this.appInputElement.inputElement.removeEventListener('keydown', this.onInputKeyDown);
  }

  private render(): VNode {
    return (
      <div class="search-bar" {...this.searchBarClasses}>
        <AppInput
          value={this.value}
          onInput={this.onInput}
          placeholder={this.placeholder}
          showClearButton={this.showClearButton}
          ref="appInputElement"
          class="search-bar__field"
        />
        {this.showSearchButton ? (
          <AppButton
            class="search-bar__button"
            onClick={this.onSearchButtonClick}
            isLoading={this.isButtonLoading}
          >
            <svg-icon name={SVGNames.search} slot={AppButtonSlots.iconLeft} class="search-bar__button-icon" />
          </AppButton>
        ) : null}
      </div>
    );
  }
}
