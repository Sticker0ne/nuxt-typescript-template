export interface IAppSearchBarEvents {
  onInput: string;
  onSearch: string;
  onEnter: string;
  onSearchButtonClick: string;
}
