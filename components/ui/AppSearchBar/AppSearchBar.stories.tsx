import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppSearchBar from '~/components/ui/AppSearchBar/AppSearchBar';

storiesOf('UI components/AppSearchBar', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    props: {
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
      showSearchButton: {
        type: Boolean,
        default: () => boolean('showSearchButton', true),
      },
      isButtonLoading: {
        type: Boolean,
        default: () => boolean('isButtonLoading', false),
      },
      debounceTime: {
        type: Number,
        default: () => number('debounceTime', 800),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        this.value = value;
        action('onInput')(value);
      },
      onSearch(value: string) {
        action('onSearch')(value);
      },
      onSearchButtonClick(value: string) {
        action('onSearchButtonClick')(value);
      },
      onEnter(value: string) {
        action('onEnter')(value);
      },
    },
    render() {
      return (
        <div style="width: 400px;">
          <AppSearchBar
            value={this.value}
            onInput={this.onInput}
            onSearch={this.onSearch}
            onSearchButtonClick={this.onSearchButtonClick}
            onEnter={this.onEnter}
            placeholder={this.placeholder}
            showClearButton={this.showClearButton}
            showSearchButton={this.showSearchButton}
            debounceTime={this.debounceTime}
            isButtonLoading={this.isButtonLoading}
          />
        </div>
      );
    },
  }))
  .add('One way binding', () => ({
    ...StoryContextAny,
    props: {
      value: {
        type: String,
        default: () => text('value', ''),
      },
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
      showSearchButton: {
        type: Boolean,
        default: () => boolean('showSearchButton', true),
      },
      isButtonLoading: {
        type: Boolean,
        default: () => boolean('isButtonLoading', false),
      },
      debounceTime: {
        type: Number,
        default: () => number('debounceTime', 800),
      },
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        action('onInput')(value);
      },
      onSearch(value: string) {
        action('onSearch')(value);
      },
      onSearchButtonClick(value: string) {
        action('onSearchButtonClick')(value);
      },
      onEnter(value: string) {
        action('onEnter')(value);
      },
    },
    render() {
      return (
        <div style="width: 400px;">
          <AppSearchBar
            value={this.value}
            onInput={this.onInput}
            onSearch={this.onSearch}
            onSearchButtonClick={this.onSearchButtonClick}
            onEnter={this.onEnter}
            placeholder={this.placeholder}
            showClearButton={this.showClearButton}
            showSearchButton={this.showSearchButton}
            debounceTime={this.debounceTime}
            isButtonLoading={this.isButtonLoading}
          />
        </div>
      );
    },
  }));
