import './AppRadioButtonsGroup.scss';
import { Component, Model, Prop, Ref, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import { nanoid } from 'nanoid';
import type {
  IAppRadioButtonsEvents,
  IAppRadioButtonItem,
} from '~/components/ui/AppRadioButtonsGroup/AppRadioButtonsGroup.types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppRadioButtonsGroup extends Vue {
  public _tsx!: WProps<this, 'disabled'> & WEvents<IAppRadioButtonsEvents>;

  @Ref() private readonly radioButtonInputs!: HTMLInputElement[];

  @Model('change', {
    required: true,
  })
  public readonly value!: string;

  private name = '';

  private emitChange(value: string): void {
    emitOn(this, 'onChange', value);
  }

  @Prop({
    type: Boolean,
    default: () => false,
  })
  public disabled!: boolean;

  @Prop({
    required: true,
    type: Array as () => IAppRadioButtonItem[],
  })
  public items!: IAppRadioButtonItem[];

  private get safeValue(): string {
    return this.value;
  }

  private set safeValue(newValue: string) {
    if (!this.disabled) this.emitChange(newValue);
    this.$nextTick(this.synchronizeValue);
  }

  private synchronizeValue(): void {
    this.radioButtonInputs.forEach(radioButtonInput => {
      radioButtonInput.checked = radioButtonInput.value === this.safeValue;
    });
  }

  private createNameForRadioButton(): void {
    this.name = nanoid();
  }

  private mounted(): void {
    this.createNameForRadioButton();
  }

  private render(): VNode {
    return (
      <div class="radio-button-group">
        {this.items.map(item => (
          <label key={item.value} class="radio-button-group__item radio-button">
            <input
              class="radio-button__input"
              domProps={{ value: item.value }}
              ref="radioButtonInputs"
              refInFor={true}
              v-model={this.safeValue}
              value={item.value}
              name={this.name}
              type="radio"
              disabled={item.disabled || this.disabled}
            />
            <span class="radio-button__wrapper">
              <span class="radio-button__place" />
              <span class="radio-button__text">{item.title}</span>
            </span>
          </label>
        ))}
      </div>
    );
  }
}
