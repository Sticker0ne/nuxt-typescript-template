import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, button, object, boolean } from '@storybook/addon-knobs';
import AppRadioButtonsGroup from './AppRadioButtonsGroup';
import { StoryContextAny } from '~/utils/StoryUtils';

storiesOf('UI components/AppRadioButtonsGroup', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    props: {
      items: {
        type: Array,
        default: () =>
          object('items', [
            { title: 'Макс', value: 'max' },
            { title: 'Мирт', value: 'mirt', disabled: true },
            { title: 'Стас', value: 'stas' },
            { title: 'представитель бекенда', value: 'back', disabled: true },
          ]),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
    },
    data() {
      return {
        value: 'max',
      };
    },
    methods: {
      ...StoryContextAny,
      onChange(value: string) {
        this.value = value;
        action('onChange')(value);
      },
    },
    render() {
      button(`set value to max`, () => {
        this.value = 'max';
      });
      button(`set value to 'back'`, () => {
        this.value = 'back';
      });
      button(`set broken value`, () => {
        this.value = '';
      });
      return (
        <AppRadioButtonsGroup
          value={this.value}
          items={this.items}
          disabled={this.disabled}
          onChange={this.onChange}
        />
      );
    },
  }))
  .add('One way binding', () => ({
    ...StoryContextAny,
    props: {
      items: {
        type: Array,
        default: () =>
          object('items', [
            { title: 'Макс', value: 'max' },
            { title: 'Мирт', value: 'mirt', disabled: true },
            { title: 'Стас', value: 'stas' },
            { title: 'представитель бекенда', value: 'back', disabled: true },
          ]),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
    },
    data() {
      return {
        value: 'max',
      };
    },
    methods: {
      ...StoryContextAny,
      onChange(value: string) {
        action('onChange')(value);
      },
    },
    render() {
      button(`set value to max`, () => {
        this.value = 'max';
      });
      button(`set value to 'back'`, () => {
        this.value = 'back';
      });
      button(`set broken value`, () => {
        this.value = '';
      });
      return (
        <AppRadioButtonsGroup
          value={this.value}
          items={this.items}
          disabled={this.disabled}
          onChange={this.onChange}
        />
      );
    },
  }));
