export interface IAppRadioButtonsEvents {
  onChange: string;
}

export interface IAppRadioButtonItem {
  title: string;
  value: string;
  disabled?: boolean;
}
