import './AppTooltip.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import type {
  IAppTooltipComponentOptions,
  IAppTooltipEvents,
  IAppTooltipPropsOptions,
} from '~/components/ui/AppTooltip/AppTooltip.types';
import { VTooltipWrapped } from '~/components/ui/AppTooltip/VTooltipWrapped';
import { SVGNames } from '~/assets/ts/constants';
import {
  AppTooltipPlacement,
  AppTooltipSlots,
  AppTooltipTrigger,
} from '~/components/ui/AppTooltip/AppTooltip.types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppTooltip extends Vue {
  public _tsx!: WProps<
    this,
    'customOptions' | 'showCloseBtn' | 'disabled' | 'open' | 'placement' | 'trigger' | 'offset' | 'container'
  > &
    WEvents<IAppTooltipEvents>;

  @Prop({ default: () => ({}), required: false })
  public readonly customOptions!: IAppTooltipPropsOptions;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly open!: boolean;

  @Prop({
    default: () => AppTooltipPlacement.auto,
    required: false,
  })
  public readonly placement!: AppTooltipPlacement;

  @Prop({ default: () => ['hover'], required: false })
  public readonly trigger!: AppTooltipTrigger[];

  @Prop({ default: () => 5, required: false })
  public readonly offset!: number;

  @Prop({ default: () => '.tooltip__btn', required: false })
  public readonly container!: string;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly showCloseBtn!: boolean;

  private overrideDefaultOptions: IAppTooltipComponentOptions = {
    popoverArrowClass: 'tooltip__arrow',
    popoverInnerClass: 'tooltip__inner',
    popoverWrapperClass: 'tooltip__wrapper',
  };

  private onHandlers: Record<string, () => any> = {
    show: () => emitOn(this, 'onShow'),
    hide: () => emitOn(this, 'onHide'),
    resize: () => emitOn(this, 'onResize'),
    dispose: () => emitOn(this, 'onDispose'),
    'update:open': () => emitOn(this, 'onUpdate'),
    'apply-show': () => emitOn(this, 'onApplyShow'),
    'apply-hide': () => emitOn(this, 'onApplyHide'),
    'auto-hide': () => emitOn(this, 'onAutoHide'),
    'close-directive': () => emitOn(this, 'onCloseDirective'),
    'close-group': () => emitOn(this, 'onCloseGroup'),
  };

  private get joinedTriggerArray(): string {
    const array = this.customOptions.trigger || this.trigger || [];
    return array.length ? array.join(' ') : '';
  }

  private get resultOptions(): IAppTooltipComponentOptions {
    const {
      open,
      disabled,
      offset,
      placement,
      container,
      overrideDefaultOptions,
      customOptions,
      joinedTriggerArray,
    } = this;
    return {
      ...overrideDefaultOptions,
      open,
      disabled,
      offset,
      placement,
      container,
      ...customOptions,
      trigger: joinedTriggerArray,
    };
  }

  private render(): VNode {
    return (
      <VTooltipWrapped
        open={this.resultOptions.open}
        disabled={this.resultOptions.disabled}
        container={this.resultOptions.container}
        placement={this.resultOptions.placement}
        trigger={this.resultOptions.trigger}
        handleResize={this.resultOptions.handleResize}
        delay={this.resultOptions.delay}
        offset={this.resultOptions.offset}
        autoHide={this.resultOptions.autoHide}
        openGroup={this.resultOptions.openGroup}
        boundariesElement={this.resultOptions.boundariesElement}
        openClass={this.resultOptions.openClass}
        popoverClass={this.resultOptions.popoverClass}
        popoverBaseClass={this.resultOptions.popoverBaseClass}
        popoverArrowClass={this.resultOptions.popoverArrowClass}
        popoverInnerClass={this.resultOptions.popoverInnerClass}
        popoverWrapperClass={this.resultOptions.popoverWrapperClass}
        popperOptions={this.resultOptions.popperOptions}
        class="tooltip"
        {...{
          on: this.onHandlers,
        }}
      >
        <div class="tooltip__btn">{this.$slots[AppTooltipSlots.activator]}</div>
        <template slot="popover" class="tooltip__popover">
          <div class="tooltip__content">{this.$slots[AppTooltipSlots.content]}</div>
          {this.showCloseBtn ? (
            <button v-close-popover class="tooltip__btn-close">
              <svg-icon name={SVGNames.cross} class="tooltip__btn-icon" />
            </button>
          ) : null}
        </template>
      </VTooltipWrapped>
    );
  }
}
