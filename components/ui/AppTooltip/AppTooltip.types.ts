import { vTooltipProps } from '~/components/ui/AppTooltip/VTooltipWrapped';

export type AppTooltipTrigger = 'click' | 'hover' | 'focus' | 'manual';

export enum AppTooltipSlots {
  activator = 'activator',
  content = 'content',
}

export enum AppTooltipPlacement {
  auto = 'auto',
  autoStart = 'auto-start',
  autoEnd = 'auto-end',
  left = 'left',
  leftStart = 'left-start',
  leftEnd = 'left-end',
  right = 'right',
  rightStart = 'right-start',
  rightEnd = 'right-end',
  top = 'top',
  topStart = 'top-start',
  topEnd = 'top-end',
  bottom = 'bottom',
  bottomStart = 'bottom-start',
  bottomEnd = 'bottom-end',
}

export type IAppTooltipComponentOptions = vTooltipProps;

export interface IAppTooltipEvents {
  onShow: void;
  onHide: void;
  onUpdate: void;
  onResize: void;
  onDispose: void;
  onApplyShow: void;
  onApplyHide: void;
  onAutoHide: void;
  onCloseDirective: void;
  onCloseGroup: void;
}

export interface IAppTooltipPropsOptions extends Omit<IAppTooltipComponentOptions, 'placement' | 'trigger'> {
  placement?: AppTooltipPlacement;
  trigger?: AppTooltipTrigger[];
}
