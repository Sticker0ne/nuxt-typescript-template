import { VPopover, VClosePopover } from 'v-tooltip';
import { ofType } from 'vue-tsx-support';
import { Vue } from 'nuxt-property-decorator';
Vue.directive('close-popover', VClosePopover);

export type vTooltipProps = {
  open?: boolean;
  disabled?: boolean;
  placement?: string;
  delay?: number;
  trigger?: string;
  offset?: number;
  container?: string;
  boundariesElement?: HTMLElement;
  popperOptions?: Record<string, unknown>;
  popoverClass?: string;
  popoverBaseClass?: string;
  popoverWrapperClass?: string;
  popoverArrowClass?: string;
  popoverInnerClass?: string;
  autoHide?: boolean;
  handleResize?: boolean;
  openGroup?: boolean;
  openClass?: string;
};

type vTooltipEvents = {
  show: void;
  hide: void;
  resize: void;
  dispose: void;
  'update:open': void;
  'apply-show': void;
  'apply-hide': void;
  'auto-hide': void;
  'close-directive': void;
  'close-group': void;
};

export const VTooltipWrapped = ofType<vTooltipProps, vTooltipEvents>().convert({
  ...VPopover,
  name: 'vTooltipWrapped',
} as any);
