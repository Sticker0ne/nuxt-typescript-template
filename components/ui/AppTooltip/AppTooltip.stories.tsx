import { storiesOf } from '@storybook/vue';
import { withKnobs, boolean, text, number, select, object, button } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { StoryContextAny } from '~/utils/StoryUtils';
import { getI18nForStorybook } from '~/utils/StorybookUtils';
import AppButton from '~/components/ui/AppButton/AppButton';
import {
  AppTooltipPlacement,
  AppTooltipSlots,
  IAppTooltipComponentOptions,
} from '~/components/ui/AppTooltip/AppTooltip.types';
import AppTooltip from '~/components/ui/AppTooltip/AppTooltip';

storiesOf('UI components/AppTooltip', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      customOptions: {
        type: Object,
        default: () =>
          object('customOptions', {
            open: true,
            offset: 10,
            disabled: false,
            placement: AppTooltipPlacement.top,
            delay: 100,
            trigger: ['click', 'focus'],
            popoverClass: 'tooltip custom-class',
            handleResize: true,
            openClass: 'was-opened',
            boundariesElement: 'body',
          }),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showCloseBtn: {
        type: Boolean,
        default: () => boolean('showCloseBtn', true),
      },
      placement: {
        type: String,
        default: () => select('placement', AppTooltipPlacement, AppTooltipPlacement.bottom),
      },
      trigger: {
        type: Array,
        default: () => object('trigger', ['click', 'focus']),
      },
      offset: {
        type: Number,
        default: () => number('offset', 5),
      },
      container: {
        type: String,
        default: () => text('container', '.tooltip__btn'),
      },
    },
    data() {
      return {
        isApplyCustomOptions: false,
        openProps: false,
      };
    },
    computed: {
      ...StoryContextAny,
      applyCustomOptions(): IAppTooltipComponentOptions {
        if (this.isApplyCustomOptions) return this.customOptions;
        return {};
      },
    },
    methods: {
      onShow() {
        action('onShow')();
      },
      onHide() {
        action('onHide')();
      },
      onResize() {
        action('onResize')();
      },
      onDispose() {
        action('onDispose')();
      },
      onUpdate() {
        action('onUpdate')();
      },
      onApplyShow() {
        action('onApplyShow')();
      },
      onApplyHide() {
        action('onApplyHide')();
      },
      onAutoHide() {
        action('onAutoHide')();
      },
      onCloseDirective() {
        action('onCloseDirective')();
      },
      onCloseGroup() {
        action('onCloseGroup')();
      },
    },
    render() {
      button(`toggle custom options`, () => {
        this.isApplyCustomOptions = !this.isApplyCustomOptions;
      });

      button(`toggle open props`, () => {
        this.openProps = !this.openProps;
      });

      return (
        <div style="width: 100%; height: 100%;padding: 160px 200px; ">
          <AppTooltip
            customOptions={this.applyCustomOptions}
            open={this.openProps}
            disabled={this.disabled}
            offset={this.offset}
            placement={this.placement}
            trigger={this.trigger}
            showCloseBtn={this.showCloseBtn}
            onShow={this.onShow}
            onHide={this.onHide}
            onResize={this.onResize}
            onDispose={this.onDispose}
            onUpdate={this.onUpdate}
            onApplyShow={this.onApplyShow}
            onApplyHide={this.onApplyHide}
            onAutoHide={this.onAutoHide}
            onCloseDirective={this.onCloseDirective}
            onCloseGroup={this.onCloseGroup}
          >
            <AppButton slot={AppTooltipSlots.activator}> Show Tooltip</AppButton>
            <div slot={AppTooltipSlots.content}>
              <p>its Tooltip!</p>
              <p>My name is Tooltip and i fit in multiple lines!</p>
            </div>
          </AppTooltip>
        </div>
      );
    },
  }));
