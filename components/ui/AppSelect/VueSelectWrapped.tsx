import vSelect from 'vue-select';
import { ofType } from 'vue-tsx-support';
import 'vue-select/dist/vue-select.css';

interface ISelectItem {
  id: string;
  title: string;
}

type vSelectProps = {
  options?: ISelectItem[];
  reduce?: (item: ISelectItem) => string;
  label?: string;
  value?: string | string[];
  disabled?: boolean;
  clearable?: boolean;
  searchable?: boolean;
  multiple?: boolean;
  placeholder?: string;
  closeOnSelect?: boolean;
  tabindex?: number;
  components?: any;
};

type vSelectEvents = {
  onInput: string | string[] | null;
  onSearch: (inputValue: string, loading: (value: boolean) => void) => void;
};

interface IVSelectAttributes {
  ref: string;
  role: string;
  class: string;
}

export interface IOpenIndicatorSlotAttributes {
  attributes: IVSelectAttributes;
}

export interface IVSelectSpinnerSlotParams {
  loading: boolean;
}

type vSelectScopedSlots = {
  'no-options'?: void;
  'open-indicator'?: IOpenIndicatorSlotAttributes;
  spinner?: IVSelectSpinnerSlotParams;
  option?: ISelectItem;
};

export const VueSelectWrapped = ofType<vSelectProps, vSelectEvents, vSelectScopedSlots>().convert({
  ...vSelect,
  name: 'vSelectWrapped',
} as any);
