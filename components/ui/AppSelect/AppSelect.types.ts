export interface IAppSelectOnSearchParams {
  searchString: string;
  toggleLoadingFunction(isLoading: boolean): void;
}

export interface IAppSelectItem {
  id: string;
  title: string;
}

export interface IAppSelectEvents {
  onSelect: string[];
  onSearch: IAppSelectOnSearchParams;
}
