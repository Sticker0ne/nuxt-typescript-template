import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, button, object, boolean, text, number } from '@storybook/addon-knobs';
import debounce from 'lodash.debounce';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppSelect from '~/components/ui/AppSelect/AppSelect';
import { IAppSelectOnSearchParams } from '~/components/ui/AppSelect/AppSelect.types';
import { getI18nForStorybook } from '~/utils/StorybookUtils';

storiesOf('UI components/AppSelect', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      items: {
        type: Array,
        default: () =>
          object('items', [
            { title: 'Макс', id: '1' },
            { title: 'Мирт', id: '2' },
            { title: 'Стас', id: '4' },
            { title: 'Юля', id: '5' },
            { title: 'Кирилл', id: '6' },
            { title: 'Настя', id: '7' },
            { title: 'Лера', id: '8' },
            { title: 'Ангелина', id: '9' },
            { title: 'Мишаня', id: '10' },
          ]),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      clearable: {
        type: Boolean,
        default: () => boolean('clearable', false),
      },
      searchable: {
        type: Boolean,
        default: () => boolean('searchable', false),
      },
      multiple: {
        type: Boolean,
        default: () => boolean('multiple', false),
      },
      placeholder: {
        type: String,
        default: () => text('placeholder', ''),
      },
      noOptions: {
        type: String,
        default: () => text('noOptions', ''),
      },
      tabindex: {
        type: Number,
        default: () => number('tabindex', 0),
      },
    },
    data() {
      return {
        value: ['1'],
      };
    },
    methods: {
      ...StoryContextAny,
      onSelect(value: string[]) {
        this.value = value;
        action('onSelect')(value);
      },
      onSearch(params: IAppSelectOnSearchParams) {
        action('onSearch')(params);
      },
    },
    render() {
      button(`set value to ['2']`, () => {
        this.value = ['2'];
      });
      button(`set value to ['2','3']`, () => {
        this.value = ['2', '3'];
      });
      button(`clear`, () => {
        this.value = [];
      });
      button(`set broken value`, () => {
        this.value = ['ololo'];
      });
      return (
        <AppSelect
          value={this.value}
          onSelect={this.onSelect}
          onSearch={this.onSearch}
          items={this.items}
          disabled={this.disabled}
          clearable={this.clearable}
          searchable={this.searchable}
          multiple={this.multiple}
          placeholder={this.placeholder}
          noOptions={this.noOptions}
          tabindex={this.tabindex}
        />
      );
    },
  }))
  .add('One way binding', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      items: {
        type: Array,
        default: () =>
          object('items', [
            { title: 'Макс', id: '1' },
            { title: 'Мирт', id: '2' },
            { title: 'Стас', id: '4' },
            { title: 'Юля', id: '5' },
            { title: 'Кирилл', id: '6' },
            { title: 'Настя', id: '7' },
            { title: 'Лера', id: '8' },
            { title: 'Ангелина', id: '9' },
            { title: 'Мишаня', id: '10' },
          ]),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      clearable: {
        type: Boolean,
        default: () => boolean('clearable', false),
      },
      searchable: {
        type: Boolean,
        default: () => boolean('searchable', false),
      },
      multiple: {
        type: Boolean,
        default: () => boolean('multiple', false),
      },
      placeholder: {
        type: String,
        default: () => text('placeholder', ''),
      },
      noOptions: {
        type: String,
        default: () => text('noOptions', ''),
      },
      tabindex: {
        type: Number,
        default: () => number('tabindex', 0),
      },
    },
    data() {
      return {
        value: ['1'],
      };
    },
    methods: {
      ...StoryContextAny,
      onSelect(value: string[]) {
        action('onSelect')(value);
      },
      onSearch(params: IAppSelectOnSearchParams) {
        action('onSearch')(params);
      },
    },
    render() {
      button(`set value to ['2']`, () => {
        this.value = ['2'];
      });
      button(`set value to ['2','3']`, () => {
        this.value = ['2', '3'];
      });
      button(`clear`, () => {
        this.value = [];
      });
      button(`set broken value`, () => {
        this.value = ['ololo'];
      });
      return (
        <AppSelect
          value={this.value}
          onSelect={this.onSelect}
          onSearch={this.onSearch}
          items={this.items}
          disabled={this.disabled}
          clearable={this.clearable}
          searchable={this.searchable}
          multiple={this.multiple}
          placeholder={this.placeholder}
          noOptions={this.noOptions}
          tabindex={this.tabindex}
        />
      );
    },
  }))
  .add('With loading', () => ({
    i18n: getI18nForStorybook(),
    ...StoryContextAny,
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      clearable: {
        type: Boolean,
        default: () => boolean('clearable', false),
      },
      multiple: {
        type: Boolean,
        default: () => boolean('multiple', false),
      },
      placeholder: {
        type: String,
        default: () => text('placeholder', ''),
      },
      tabindex: {
        type: Number,
        default: () => number('tabindex', 0),
      },
    },
    data() {
      return {
        value: [],
        items: [],
        debouncedOnSearch: debounce(this.onSearch, 1500),
        noOptions: 'Введите что-нибудь',
      };
    },
    methods: {
      ...StoryContextAny,
      onSelect(value: string[]) {
        this.value = value;
        action('onSelect')(value);
      },
      async onSearch(params: IAppSelectOnSearchParams) {
        this.noOptions = 'Введите что-нибудь';
        if (!params.searchString) return;
        this.items = [];
        params.toggleLoadingFunction(true);
        const items = await this.fetchItems(params.searchString);
        params.toggleLoadingFunction(false);
        this.items = items;
        this.noOptions = '';
      },
      fetchItems(searchString: string): Promise<any> {
        /* eslint-disable-next-line */
        console.log(searchString);
        let resolveFunc: any;
        const prom = new Promise(resolve => {
          resolveFunc = resolve;
        });

        setTimeout(() => {
          resolveFunc([
            { title: 'Макс', id: '1' },
            { title: 'Мирт', id: '2' },
            { title: 'Стас', id: '4' },
            { title: 'Юля', id: '5' },
            { title: 'Кирилл', id: '6' },
            { title: 'Настя', id: '7' },
            { title: 'Лера', id: '8' },
            { title: 'Ангелина', id: '9' },
            { title: 'Мишаня', id: '10' },
          ]);
        }, 2000);

        return prom;
      },
    },
    render() {
      button(`set value to ['2']`, () => {
        this.value = ['2'];
      });
      button(`set value to ['2','3']`, () => {
        this.value = ['2', '3'];
      });
      button(`clear`, () => {
        this.value = [];
      });
      button(`set broken value`, () => {
        this.value = ['ololo'];
      });
      return (
        <AppSelect
          value={this.value}
          onSelect={this.onSelect}
          onSearch={this.debouncedOnSearch}
          items={this.items}
          disabled={this.disabled}
          clearable={this.clearable}
          searchable={true}
          multiple={this.multiple}
          placeholder={this.placeholder}
          noOptions={this.noOptions}
          tabindex={this.tabindex}
        />
      );
    },
  }));
