import './AppSelect.scss';
import { Component, Model, Prop, Vue } from 'nuxt-property-decorator';
import type { CreateElement, VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import {
  IOpenIndicatorSlotAttributes,
  IVSelectSpinnerSlotParams,
  VueSelectWrapped,
} from '~/components/ui/AppSelect/VueSelectWrapped';
import type { IAppSelectEvents, IAppSelectItem } from '~/components/ui/AppSelect/AppSelect.types';
import { SVGNames } from '~/assets/ts/constants';
import Loader from '~/components/common/Loader/Loader';
import { LoaderSpinnerSizes } from '~/components/common/Loader/Loader.types';
import type { IElemClasses } from '~/types/global-types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppSelect extends Vue {
  public _tsx!: WProps<
    this,
    | 'disabled'
    | 'placeholder'
    | 'clearable'
    | 'searchable'
    | 'multiple'
    | 'tabindex'
    | 'noOptions'
    | 'checkboxValue'
  > &
    WEvents<IAppSelectEvents>;

  @Model('select', {
    default: () => [],
    required: true,
  })
  public value!: string[];

  private emitSelect(value: string[]): void {
    if (this.disabled) return;
    emitOn(this, 'onSelect', value);
  }

  private emitSearch(searchString: string, toggleLoadingFunction: (isLoading: boolean) => void): void {
    if (this.disabled) return;
    emitOn(this, 'onSearch', { searchString, toggleLoadingFunction });
  }

  private get safeValue(): string[] {
    return this.value;
  }

  private set safeValue(newValue: string[]) {
    this.emitSelect(newValue);
  }

  @Prop({ type: Array, default: () => [], required: true })
  public readonly items!: IAppSelectItem[];

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly checkboxValue!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly clearable!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly searchable!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly multiple!: boolean;

  @Prop({ default: () => 'Select something', required: false })
  public readonly placeholder!: string;

  @Prop({ default: () => '', required: false })
  public readonly noOptions!: string;

  @Prop({ default: () => 0, required: false })
  public readonly tabindex!: number;

  private get noOptionsTitle(): string {
    if (this.noOptions) return this.noOptions;
    return this.$i18n.t('components.uiComponents.appSelect.noOptionsTittle').toString();
  }

  private get placeholderTitle(): string {
    if (this.placeholder) return this.placeholder;
    return this.$i18n.t('components.uiComponents.appSelect.placeholderTittle').toString();
  }

  private onInput(value: string | string[] | null): void {
    if (value === null) this.safeValue = [];
    else if (typeof value === 'string') this.safeValue = [value];
    else if (Array.isArray(value)) this.safeValue = value;
  }

  private onSearch(searchString: string, toggleLoadingFunction: (isLoading: boolean) => void): void {
    this.emitSearch(searchString, toggleLoadingFunction);
  }

  private deselectComponent = {
    render: (createElement: CreateElement) => createElement('svg-icon', { props: { name: SVGNames.cross } }),
  };

  private get selectClasses(): IElemClasses {
    return {
      class: {
        'select--chosen-multiple': this.multiple && this.value.length > 0,
        'select--checkbox-value': this.checkboxValue,
      },
    };
  }

  private render(): VNode {
    return (
      <div>
        <VueSelectWrapped
          class="select"
          {...this.selectClasses}
          value={this.safeValue}
          options={this.items}
          label="title"
          onInput={this.onInput}
          reduce={item => item.id}
          disabled={this.disabled}
          searchable={this.searchable}
          multiple={this.multiple}
          clearable={this.clearable}
          placeholder={this.placeholderTitle}
          closeOnSelect={!this.multiple}
          tabindex={this.tabindex}
          components={{ Deselect: this.deselectComponent }}
          scopedSlots={{
            'no-options': () => <span>{this.noOptionsTitle}</span>,
            spinner: (params: IVSelectSpinnerSlotParams) =>
              params.loading ? (
                <Loader
                  class="select__loader"
                  spinnerSize={LoaderSpinnerSizes.small}
                  fullscreen={false}
                  showOverlay={false}
                  noCatchFocus
                />
              ) : null,
            option: (item: IAppSelectItem) => {
              return (
                <div class="select-option">
                  {this.checkboxValue ? <div class="select-option__icon" /> : null}
                  <div class="select-option__value">{item.title}</div>
                </div>
              );
            },
            'open-indicator': (payload: IOpenIndicatorSlotAttributes) => {
              return (
                <span class={payload.attributes.class}>
                  <span class="open-indicator">
                    <svg-icon name={SVGNames.arrow} />
                  </span>
                </span>
              );
            },
          }}
          onSearch={this.onSearch}
        />
      </div>
    );
  }
}
