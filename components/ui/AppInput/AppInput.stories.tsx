import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, button, select, number } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppInput from '~/components/ui/AppInput/AppInput';
import { AppInputModes } from '~/components/ui/AppInput/AppInput.types';

storiesOf('UI components/AppInput', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    props: {
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        this.value = value;
        action('onInput')(value);
      },
      onBlur() {
        action('onBlur')();
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppInput
          value={this.value}
          onInput={this.onInput}
          onBlur={this.onBlur}
          disabled={this.disabled}
          placeholder={this.placeholder}
          showClearButton={this.showClearButton}
        />
      );
    },
  }))
  .add('One way binding', () => ({
    ...StoryContextAny,
    props: {
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        action('onInput')(value);
      },
      onBlur() {
        action('onBlur')();
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppInput
          value={this.value}
          onInput={this.onInput}
          onBlur={this.onBlur}
          disabled={this.disabled}
          placeholder={this.placeholder}
          showClearButton={this.showClearButton}
        />
      );
    },
  }))
  .add('Modes', () => ({
    ...StoryContextAny,
    props: {
      placeholder: {
        type: String,
        default: () => text('placehoder', 'Type smth...'),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
      mode: {
        type: String,
        default: () => select('mode', AppInputModes, AppInputModes.text),
      },
      floatPrecision: {
        type: Number,
        default: () => number('floatPrecision', 2),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        this.value = value;
        action('onInput')(value);
      },
      onBlur() {
        action('onBlur')();
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppInput
          value={this.value}
          onInput={this.onInput}
          onBlur={this.onBlur}
          disabled={this.disabled}
          placeholder={this.placeholder}
          showClearButton={this.showClearButton}
          mode={this.mode}
          floatPrecision={this.floatPrecision}
        />
      );
    },
  }))
  .add('MaxLength', () => ({
    ...StoryContextAny,
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
      maxLength: {
        type: Number,
        default: () => number('maxLength', 5),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        this.value = value;
        action('onInput')(value);
      },
      onBlur() {
        action('onBlur')();
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppInput
          value={this.value}
          onInput={this.onInput}
          onBlur={this.onBlur}
          disabled={this.disabled}
          showClearButton={this.showClearButton}
          maxLength={this.maxLength}
        />
      );
    },
  }))
  .add('Mask string', () => ({
    ...StoryContextAny,
    props: {
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      showClearButton: {
        type: Boolean,
        default: () => boolean('showClearButton', true),
      },
      mask: {
        default: () => text('mask', '+7 (###) ###-##-##'),
      },
    },
    data() {
      return {
        value: '',
      };
    },
    methods: {
      ...StoryContextAny,
      onInput(value: string) {
        this.value = value;
        action('onInput')(value);
      },
      onBlur() {
        action('onBlur')();
      },
    },
    render() {
      button(`set value to 'smth'`, () => {
        this.value = 'smth';
      });
      button(`set value to 'smth else'`, () => {
        this.value = 'smth else';
      });
      button(`clear`, () => {
        this.value = '';
      });
      return (
        <AppInput
          value={this.value}
          onInput={this.onInput}
          onBlur={this.onBlur}
          disabled={this.disabled}
          showClearButton={this.showClearButton}
          mask={this.mask}
        />
      );
    },
  }));
