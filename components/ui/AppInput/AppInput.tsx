import { Component, Model, Prop, Ref, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import {
  AppInputModes,
  IAppInputEvents,
  IAppInputMask,
  IAppInputSelectionGroup,
} from '~/components/ui/AppInput/AppInput.types';
import { SVGNames } from '~/assets/ts/constants';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppInput extends Vue {
  public _tsx!: WProps<
    this,
    'disabled' | 'showClearButton' | 'placeholder' | 'mode' | 'floatPrecision' | 'maxLength' | 'mask',
    'inputElement'
  > &
    WEvents<IAppInputEvents>;

  @Ref() public readonly inputElement!: HTMLInputElement;

  @Model('input', {
    default: () => '',
    required: true,
  })
  public value!: string;

  private emitChange(value: string): void {
    if (this.disabled) return;
    emitOn(this, 'onInput', value);
  }

  @Prop({ type: String, default: () => AppInputModes.text, required: false })
  public readonly mode!: AppInputModes;

  @Prop({ default: () => 2, required: false })
  public readonly floatPrecision!: number;

  @Prop({ default: () => 0, required: false })
  public readonly maxLength!: number;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({ type: Boolean, default: () => true, required: false })
  public showClearButton!: boolean;

  @Prop({ default: () => '', required: false })
  public readonly placeholder!: string;

  @Prop({ default: () => null, required: false })
  public readonly mask!: IAppInputMask | string | null;

  private emitBlur(): void {
    if (this.blurOccurred) emitOn(this, 'onBlur');
  }

  protected get safeValue(): string {
    return this.value;
  }

  protected set safeValue(newValue: string) {
    if (this.valueSatisfiedModeFilter(newValue) && this.valueSatisfiedLengthFilter(newValue))
      this.emitChange(newValue);

    this.$nextTick(this.synchronizeValue);
  }

  private get calculatedMaskDirective(): IAppInputMask | null {
    if (!this.mask) return null;
    if (typeof this.mask === 'string')
      return {
        mask: this.mask,
        tokens: {
          '#': { pattern: /\d/ },
          X: { pattern: /[0-9a-zA-Z]/ },
          S: { pattern: /[a-zA-Z]/ },
          A: { pattern: /[a-zA-Z]/, transform: v => v.toLocaleUpperCase() },
          a: { pattern: /[a-zA-Z]/, transform: v => v.toLocaleLowerCase() },
          '!': { escape: true },
        },
      };

    return this.mask;
  }

  protected get inputDynamicDirectives(): any[] {
    if (this.calculatedMaskDirective) return [{ name: 'mask', value: this.calculatedMaskDirective }];
    return [];
  }

  private selectionPositionGroup: IAppInputSelectionGroup = {
    selectionStart: null,
    selectionEnd: null,
    valueLengthDiff: 0,
  };

  private blurOccurred = false;

  private valueSatisfiedLengthFilter(value: string): boolean {
    if (!this.maxLength || this.mode !== AppInputModes.text) return true;

    const maxLength = Math.max(this.safeValue.length, this.maxLength);
    return value.length <= maxLength;
  }

  private valueSatisfiedModeFilter(value: string): boolean {
    if (this.mode === AppInputModes.text) return true;
    if (!value.length) return true;

    const integerRegexp = /^((-(?!0)\d*)|([1-9]\d*))$/;
    const positiveIntegerRegexp = /^([1-9]\d*)$/;
    const positiveFloatRegexp = new RegExp(`^(([1-9]|0(?![0-9]))\\d*[.,]?\\d{0,${this.floatPrecision}})$`);
    const floatRegexp = new RegExp(
      `^((([1-9]|0(?![0-9])|-[1-9]|-0(?![0-9]))\\d*[.,]?\\d{0,${this.floatPrecision}})|-)$`,
    );

    if (this.mode === AppInputModes.integer) return !!value.match(integerRegexp);
    if (this.mode === AppInputModes.positiveInteger) return !!value.match(positiveIntegerRegexp);
    if (this.mode === AppInputModes.float) return !!value.match(floatRegexp);
    if (this.mode === AppInputModes.positiveFloat) return !!value.match(positiveFloatRegexp);

    return false;
  }

  private normalizeNumberValue(): void {
    if (this.mode === AppInputModes.text) return;

    const normalizedSafeValue = this.safeValue.replace(/,/g, '.');

    if (this.mode === AppInputModes.integer || this.mode === AppInputModes.positiveInteger) {
      const parsedValue = Number.parseInt(normalizedSafeValue);
      this.safeValue = parsedValue.toString();
      return;
    }

    if (this.mode === AppInputModes.float || this.mode === AppInputModes.positiveFloat) {
      const parsedValue = Number.parseFloat(normalizedSafeValue).toFixed(this.floatPrecision);
      this.safeValue = parsedValue.toString();
    }
  }

  private storeSelectionPosition(value: string): void {
    const { selectionEnd } = this.inputElement;
    this.selectionPositionGroup = {
      selectionStart: selectionEnd,
      selectionEnd,
      valueLengthDiff: value.length - this.safeValue.length,
    };
  }

  protected onInputBlur(): void {
    this.blurOccurred = true;
    this.normalizeNumberValue();
    setTimeout(this.emitBlur);
  }

  protected onInput(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.storeSelectionPosition(value);
    this.safeValue = value;
  }

  protected clearValue(): void {
    this.blurOccurred = false;
    this.safeValue = '';
    this.focusOnElement();
  }

  private focusOnElement(): void {
    this.inputElement.focus();
  }

  private synchronizeValue(): void {
    if (this.inputElement.value === this.safeValue) return;

    this.inputElement.value = this.safeValue;
    const newSelectionPosition =
      (this.selectionPositionGroup.selectionEnd || 0) - this.selectionPositionGroup.valueLengthDiff;

    this.inputElement.selectionEnd = newSelectionPosition;
    this.inputElement.selectionStart = newSelectionPosition;
  }

  protected get shouldShowClearButton(): boolean {
    return this.showClearButton && this.safeValue.length > 0 && !this.disabled;
  }

  protected render(): VNode {
    return (
      <div class="form-element form-element--input">
        <input
          value={this.safeValue}
          onInput={this.onInput}
          ref="inputElement"
          disabled={this.disabled}
          placeholder={this.placeholder}
          class="form-element__field"
          onBlur={this.onInputBlur}
          {...{ directives: this.inputDynamicDirectives }}
        />
        {this.shouldShowClearButton ? (
          <div class="form-element__button-clear" onClick={this.clearValue}>
            <svg-icon name={SVGNames.cross} class="form-element__clear-icon" />
          </div>
        ) : null}
      </div>
    );
  }
}
