export interface IAppInputEvents {
  onInput: string;
  onBlur: void;
}

export interface IAppInputSelectionGroup {
  selectionStart: number | null;
  selectionEnd: number | null;
  valueLengthDiff: number;
}

interface IAppInputMaskToken {
  pattern?: RegExp;
  transform?: (value: string) => string;
  escape?: boolean;
}

export interface IAppInputMask {
  mask: string;
  tokens?: Record<string, IAppInputMaskToken>;
}

export enum AppInputModes {
  positiveInteger = 'positiveInteger',
  integer = 'integer',
  positiveFloat = 'positiveFloat',
  float = 'float',
  text = 'text',
}
