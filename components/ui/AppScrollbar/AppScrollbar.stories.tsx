import { storiesOf } from '@storybook/vue';
import { withKnobs, boolean, object, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppScrollbar from '~/components/ui/AppScrollbar/AppScrollbar';
import { IAppScrollbarOptions } from '~/components/ui/AppScrollbar/AppScrollbar.types';

storiesOf('UI components/AppScrollbar', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    ...StoryContextAny,
    props: {
      options: {
        type: Object,
        default: () =>
          object('options', {
            handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
            wheelSpeed: 1,
            wheelPropagation: false,
            swipeEasing: true,
            minScrollbarLength: 40,
            maxScrollbarLength: 100,
            scrollingThreshold: 1000,
            useBothWheelAxes: false,
            suppressScrollX: false,
            suppressScrollY: false,
            scrollXMarginOffset: 0,
            scrollYMarginOffset: 0,
          }),
      },
      tag: {
        type: String,
        default: () => text('tag', 'div'),
      },
      watchOptions: {
        type: Boolean,
        default: () => boolean('watchOptions', false),
      },
    },
    computed: {
      ...StoryContextAny,
      computedScrollOptions(): IAppScrollbarOptions | null {
        if (!this.options) return null;
        return this.options;
      },
    },
    render() {
      return (
        <AppScrollbar
          options={this.computedScrollOptions}
          watchOptions={this.watchOptions}
          tag={this.tag}
          style="max-height: 400px; max-width: 500px;"
          onPsScrollY={action('onPsScrollY')}
          onPsScrollX={action('onPsScrollX')}
          onPsScrollUp={action('onPsScrollUp')}
          onPsScrollDown={action('onPsScrollDown')}
          onPsScrollLeft={action('onPsScrollLeft')}
          onPsScrollRight={action('onPsScrollRight')}
          onPsYReachStart={action('onPsYReachStart')}
          onPsYReachEnd={action('onPsYReachEnd')}
          onPsXReachStart={action('onPsXReachStart')}
          onPsXReachEnd={action('onPsXReachEnd')}
        >
          <div style="">
            {
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sagittis vitae et leo duis ut diam. Nulla posuere sollicitudin aliquam ultrices sagittis. Vel fringilla est ullamcorper eget nulla facilisi. Feugiat in fermentum posuere urna nec tincidunt praesent semper feugiat. Vulputate odio ut enim blandit volutpat maecenas volutpat blandit. Vitae proin sagittis nisl rhoncus mattis rhoncus urna neque. Pharetra diam sit amet nisl suscipit adipiscing bibendum. Convallis posuere morbi leo urna molestie at elementum eu. Lacus suspendisse faucibus interdum posuere lorem ipsum dolor.'
            }
          </div>
        </AppScrollbar>
      );
    },
  }));
