import { PerfectScrollbar } from 'vue2-perfect-scrollbar';
import { ofType } from 'vue-tsx-support';

type psOptions = {
  handlers?: string[];
  wheelSpeed?: number;
  wheelPropagation?: boolean;
  swipeEasing?: boolean;
  minScrollbarLength?: number;
  maxScrollbarLength?: number;
  scrollingThreshold?: number;
  useBothWheelAxes?: boolean;
  suppressScrollX?: boolean;
  suppressScrollY?: boolean;
  scrollXMarginOffset?: number;
  scrollYMarginOffset?: number;
};

type IPsProps = {
  options?: psOptions;
  tag?: string;
  watchOptions?: boolean;
};

type psEvents = {
  'onPs-scroll-y': void;
  'onPs-scroll-x': void;
  'onPs-scroll-up': void;
  'onPs-scroll-down': void;
  'onPs-scroll-left': void;
  'onPs-scroll-right': void;
  'onPs-y-reach-start': void;
  'onPs-y-reach-end': void;
  'onPs-x-reach-start': void;
  'onPs-x-reach-end': void;
};

export const PerfectScrollbarWrapped = ofType<IPsProps, psEvents>().convert({
  ...PerfectScrollbar,
  name: 'wrappedPerfectScrollBar',
} as any);
