import './AppScrollbar.scss';
import { Component, Prop, Ref, Vue } from 'nuxt-property-decorator';
import { emitOn } from 'vue-tsx-support';
import type { VNode } from 'vue';

import type {
  IAppScrollbarEvents,
  IAppScrollbarOptions,
} from '~/components/ui/AppScrollbar/AppScrollbar.types';
import { PerfectScrollbarWrapped } from '~/components/ui/AppScrollbar/PerfectScrollbarWrapped';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppScrollbar extends Vue {
  public _tsx!: WProps<
    this,
    'options' | 'tag' | 'watchOptions' | 'enableEvents',
    'updatePS' | 'updatePSTwice'
  > &
    WEvents<IAppScrollbarEvents>;

  @Ref() private readonly perfectScrollbar!: { ps: { update: () => void } };

  @Prop({ default: () => null, required: false })
  public options!: IAppScrollbarOptions;

  @Prop({ default: () => 'div', required: false })
  public tag!: string;

  @Prop({ default: () => false, required: false })
  public watchOptions!: boolean;

  @Prop({ default: () => false, required: false })
  public enableEvents!: boolean;

  public updatePS(): void {
    this.perfectScrollbar?.ps?.update();
  }

  public updatePSTwice(): void {
    this.perfectScrollbar?.ps?.update();
    setTimeout(() => {
      this.perfectScrollbar?.ps?.update();
    });
  }

  private disableTabIndex(): void {
    const thumbX = document.getElementsByClassName('ps__thumb-x');
    const thumbY = document.getElementsByClassName('ps__thumb-y');

    if (thumbX && thumbX.length) {
      thumbX[0].setAttribute('tabindex', '-1');
    }

    if (thumbY && thumbY.length) {
      thumbY[0].setAttribute('tabindex', '-1');
    }
  }

  private get computedOn(): Record<string, () => any> {
    return this.enableEvents
      ? {
          'ps-scroll-y': () => emitOn(this, 'onPsScrollY'),
          'ps-scroll-x': () => emitOn(this, 'onPsScrollX'),
          'ps-scroll-up': () => emitOn(this, 'onPsScrollUp'),
          'ps-scroll-down': () => emitOn(this, 'onPsScrollDown'),
          'ps-scroll-left': () => emitOn(this, 'onPsScrollLeft'),
          'ps-scroll-right': () => emitOn(this, 'onPsScrollRight'),
          'ps-y-reach-start': () => emitOn(this, 'onPsYReachStart'),
          'ps-y-reach-end': () => emitOn(this, 'onPsYReachEnd'),
          'ps-x-reach-start': () => emitOn(this, 'onPsXReachStart'),
          'ps-x-reach-end': () => emitOn(this, 'onPsXReachEnd'),
        }
      : {};
  }

  private mounted(): void {
    this.disableTabIndex();
  }

  private get scrollOptions(): IAppScrollbarOptions {
    return {
      scrollYMarginOffset: 5,
      minScrollbarLength: 30,
      ...this.options,
    };
  }

  private render(): VNode {
    return (
      <PerfectScrollbarWrapped
        ref="perfectScrollbar"
        options={this.scrollOptions}
        watchOptions={this.watchOptions}
        tag={this.tag}
        {...{
          on: this.computedOn,
        }}
      >
        {this.$slots.default}
      </PerfectScrollbarWrapped>
    );
  }
}
