export interface IAppScrollbarOptions {
  handlers?: string[];
  wheelSpeed?: number;
  wheelPropagation?: boolean;
  swipeEasing?: boolean;
  minScrollbarLength?: number;
  maxScrollbarLength?: number;
  scrollingThreshold?: number;
  useBothWheelAxes?: boolean;
  suppressScrollX?: boolean;
  suppressScrollY?: boolean;
  scrollXMarginOffset?: number;
  scrollYMarginOffset?: number;
}

export interface IAppScrollbarEvents {
  onPsScrollY: void;
  onPsScrollX: void;
  onPsScrollUp: void;
  onPsScrollDown: void;
  onPsScrollLeft: void;
  onPsScrollRight: void;
  onPsScrollStart: void;
  onPsScrollEnd: void;
  onPsYReachStart: void;
  onPsYReachEnd: void;
  onPsXReachStart: void;
  onPsXReachEnd: void;
}
