import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, number, boolean } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import AppPaginationBar from '~/components/ui/AppPaginationBar/AppPaginationBar';

storiesOf('UI components/AppPaginationBar', module)
  .addDecorator(withKnobs)
  .add('Two way binding', () => ({
    ...StoryContextAny,
    props: {
      totalPages: {
        type: Number,
        default: () => number('totalPages', 22),
      },
      isLoading: {
        type: Boolean,
        default: () => boolean('isLoading', false),
      },
    },
    data() {
      return {
        currentPage: 2,
      };
    },
    methods: {
      ...StoryContextAny,
      onPageChange(value: number) {
        this.currentPage = value;
        action('onPageChange')(value);
      },
    },
    render() {
      return (
        <AppPaginationBar
          currentPage={this.currentPage}
          totalPages={this.totalPages}
          isLoading={this.isLoading}
          onPageChange={this.onPageChange}
        />
      );
    },
  }))
  .add('One way binding', () => ({
    ...StoryContextAny,
    props: {
      totalPages: {
        type: Number,
        default: () => number('totalPages', 22),
      },
      currentPage: {
        type: Number,
        default: () => number('currentPage', 2),
      },
      isLoading: {
        type: Boolean,
        default: () => boolean('isLoading', false),
      },
    },
    methods: {
      ...StoryContextAny,
      onPageChange(value: number) {
        action('onPageChange')(value);
      },
    },
    render() {
      return (
        <AppPaginationBar
          currentPage={this.currentPage}
          totalPages={this.totalPages}
          isLoading={this.isLoading}
          onPageChange={this.onPageChange}
        />
      );
    },
  }));
