import './AppPaginationBar.scss';
import { Component, Model, Prop, Vue, Watch } from 'nuxt-property-decorator';
import { emitOn } from 'vue-tsx-support';
import type { VNode } from 'vue';
import { nanoid } from 'nanoid';
import type { WEvents, WProps } from '~/types/tsx-element-types';
import type {
  IAppPaginationBarButtonItem,
  IAppPaginationBarEvents,
} from '~/components/ui/AppPaginationBar/AppPaginationBar.types';
import { AppPaginationBarButtonTypes } from '~/components/ui/AppPaginationBar/AppPaginationBar.types';
import AppButton from '~/components/ui/AppButton/AppButton';
import { AppButtonColors, AppButtonSlots } from '~/components/ui/AppButton/AppButton.types';
import AppDropdown from '~/components/ui/AppDropdown/AppDropdown';
import { SVGNames } from '~/assets/ts/constants';

@Component
export default class AppPaginationBar extends Vue {
  public _tsx!: WProps<this, 'isLoading'> & WEvents<IAppPaginationBarEvents>;

  @Prop({ default: () => 1, required: true })
  public readonly totalPages!: number;

  @Prop({ type: Boolean, default: () => false })
  public readonly isLoading!: boolean;

  @Model('pageChange', { default: () => 1, required: true })
  public readonly currentPage!: number;

  private emitPageChange(pageNumber: number): void {
    emitOn(this, 'onPageChange', pageNumber);
  }

  private readonly maxSeenPages = 7;
  private readonly sideAndDropdownButtonsAmount = 4;
  private readonly showDropdownOffset = 4;

  private get showNextButton(): boolean {
    return this.currentPage < this.totalPages;
  }

  private get showAnyDropdown(): boolean {
    return this.totalPages > this.maxSeenPages;
  }

  private get showLeftDropdown(): boolean {
    if (!this.showAnyDropdown) return false;
    return this.currentPage > this.showDropdownOffset;
  }

  private get showRightDropdown(): boolean {
    if (!this.showAnyDropdown) return false;

    return this.currentPage < this.totalPages - this.maxSeenPages + this.showDropdownOffset;
  }

  private get middleButtonNumbers(): number[] {
    // Если не нужно показывать дропдауны, то есть кол-во кнопок меньше или равно максимальному
    if (!this.showAnyDropdown) {
      const buttonNumbers: number[] = [];
      for (let i = 2; i < this.totalPages; i++) buttonNumbers.push(i);
      return buttonNumbers;
    }

    // Если дропдаун только слева
    if (this.showLeftDropdown && !this.showRightDropdown) {
      const buttonNumbers: number[] = [];
      for (
        let i = this.totalPages - (this.maxSeenPages - (this.sideAndDropdownButtonsAmount - 1));
        i < this.totalPages;
        i++
      )
        buttonNumbers.push(i);
      return buttonNumbers;
    }

    // Если дропдаун только справа
    if (!this.showLeftDropdown && this.showRightDropdown) {
      const buttonNumbers: number[] = [];
      for (let i = 2; i < this.maxSeenPages - 1; i++) buttonNumbers.push(i);
      return buttonNumbers;
    }

    // Если дропдаун и слева и справа
    if (this.showLeftDropdown && this.showRightDropdown) {
      const middleButtonsAmount = this.maxSeenPages - this.sideAndDropdownButtonsAmount; // Всего средних кнопок
      const partButtonsAmount = (middleButtonsAmount - 1) / 2; // Кнопок слева или справа от выбранной

      const buttonNumbers: number[] = [];
      for (let i = this.currentPage - partButtonsAmount; i <= this.currentPage + partButtonsAmount; i++)
        buttonNumbers.push(i);
      return buttonNumbers;
    }

    return [];
  }

  private get middleButtonItems(): IAppPaginationBarButtonItem[] {
    return this.middleButtonNumbers.map(this.buildNumberButtonItem);
  }

  private get leftDropdownNumbers(): number[] {
    if (!this.showLeftDropdown) return [];
    const numbers: number[] = [];
    for (let i = 2; i < this.middleButtonNumbers[0]; i++) numbers.push(i);

    return numbers;
  }

  private get rightDropdownNumbers(): number[] {
    if (!this.showRightDropdown) return [];
    const numbers: number[] = [];
    const lastMiddleButtonNumber = this.middleButtonNumbers[this.middleButtonNumbers.length - 1];
    for (let i = lastMiddleButtonNumber + 1; i < this.totalPages; i++) numbers.push(i);

    return numbers;
  }

  private get firstButtonItem(): IAppPaginationBarButtonItem {
    return this.buildNumberButtonItem(1);
  }

  private get lastButtonItem(): IAppPaginationBarButtonItem {
    return this.buildNumberButtonItem(this.totalPages);
  }

  private get leftDropdownButtonItem(): IAppPaginationBarButtonItem | null {
    if (!this.leftDropdownNumbers.length) return null;
    return this.buildDropdownButtonItem(this.leftDropdownNumbers, 'left');
  }

  private get rightDropdownButtonItem(): IAppPaginationBarButtonItem | null {
    if (!this.rightDropdownNumbers.length) return null;
    return this.buildDropdownButtonItem(this.rightDropdownNumbers, 'right');
  }

  private get totalButtonItems(): IAppPaginationBarButtonItem[] {
    const buttonItems: IAppPaginationBarButtonItem[] = [];

    buttonItems.push(this.firstButtonItem);
    if (this.leftDropdownButtonItem) buttonItems.push(this.leftDropdownButtonItem);
    buttonItems.push(...this.middleButtonItems);
    if (this.rightDropdownButtonItem) buttonItems.push(this.rightDropdownButtonItem);
    buttonItems.push(this.lastButtonItem);

    return buttonItems;
  }

  private buildNumberButtonItem(buttonNumber: number): IAppPaginationBarButtonItem {
    const active = this.currentPage === buttonNumber;
    return {
      buttonType: AppPaginationBarButtonTypes.number,
      buttonNumber,
      active,
      loading: active && this.isLoading,
    };
  }

  private buildDropdownButtonItem(
    dropdownNumbers: number[],
    side: 'left' | 'right',
  ): IAppPaginationBarButtonItem {
    return {
      buttonType: AppPaginationBarButtonTypes.dropdown,
      active: false,
      loading: false,
      dropdownNumbers,
      left: side === 'left',
      right: side === 'right',
    };
  }

  private onNextButtonClick(): void {
    this.emitPageChange(Math.min(this.totalPages, this.currentPage + 1));
  }

  @Watch('totalPages')
  private onTotalPagesChange(): void {
    this.emitPageChange(1);
  }

  private buildButtonElementFromButtonItem(buttonItem: IAppPaginationBarButtonItem): VNode | null {
    if (buttonItem.buttonType === AppPaginationBarButtonTypes.number) {
      const color = buttonItem.active ? AppButtonColors.red : AppButtonColors.white;
      return (
        <AppButton
          color={color}
          isLoading={buttonItem.loading}
          key={`pagination-bar-button-${nanoid()}`}
          onClick={() => this.emitPageChange(buttonItem.buttonNumber || 1)}
        >
          {buttonItem.buttonNumber}
        </AppButton>
      );
    }

    if (buttonItem.buttonType === AppPaginationBarButtonTypes.dropdown && buttonItem.dropdownNumbers) {
      const dropdownItems = buttonItem.dropdownNumbers.map(buttonNumber => ({
        id: buttonNumber.toString(),
        title: buttonNumber.toString(),
      }));

      return (
        <AppDropdown
          style="display: inline-block; width: 42px"
          items={dropdownItems}
          buttonTitle="..."
          scrollHeight={'200px'}
          key={`pagination-bar-dropdown-right-${buttonItem.right}-left-${buttonItem.left}`}
          onItemClick={item => this.emitPageChange(Number.parseInt(item.id))}
        />
      );
    }

    return null;
  }

  private render(): VNode {
    return (
      <div class="pagination-bar">
        {this.totalButtonItems.map(this.buildButtonElementFromButtonItem)}
        {this.showNextButton ? (
          <AppButton class="arrow-button" color={AppButtonColors.white} onClick={this.onNextButtonClick}>
            <svg-icon name={SVGNames.arrowSolid} slot={AppButtonSlots.iconLeft} />
          </AppButton>
        ) : null}
      </div>
    );
  }
}
