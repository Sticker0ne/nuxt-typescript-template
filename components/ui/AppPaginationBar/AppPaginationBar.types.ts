export interface IAppPaginationBarEvents {
  onPageChange: number;
}

export enum AppPaginationBarButtonTypes {
  number = 'number',
  dropdown = 'dropdown',
}

export interface IAppPaginationBarButtonItem {
  buttonType: AppPaginationBarButtonTypes;
  buttonNumber?: number;
  loading: boolean;
  active: boolean;
  dropdownNumbers?: number[];
  left?: boolean;
  right?: boolean;
}
