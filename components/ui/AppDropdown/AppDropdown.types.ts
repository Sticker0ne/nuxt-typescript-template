export interface IAppDropdownItem {
  id: string;
  title: string;
  selected?: boolean;
  disabled?: boolean;
}

export interface IAppDropdownEvents {
  onItemClick: IAppDropdownItem;
  onOpen: void;
  onClose: void;
}

export enum IAppDropdownStates {
  opened = 'opened',
  closed = 'closed',
}
