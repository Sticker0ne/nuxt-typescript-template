import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { withKnobs, object, boolean, text } from '@storybook/addon-knobs';
import { StoryContextAny } from '~/utils/StoryUtils';
import { IAppDropdownItem } from '~/components/ui/AppDropdown/AppDropdown.types';
import AppDropdown from '~/components/ui/AppDropdown/AppDropdown';

storiesOf('UI components/AppDropdown', module)
  .addDecorator(withKnobs)
  .add('Simple', () => ({
    ...StoryContextAny,
    props: {
      items: {
        type: Array,
        default: () =>
          object('items', [
            { id: '1', title: 'one' },
            { id: '2', title: 'two', selected: true },
            { id: '3', title: 'three', selected: false, disabled: true },
            { id: '4', title: 'four', disabled: true },
            { id: '5', title: 'five' },
          ]),
      },
      disabled: {
        type: Boolean,
        default: () => boolean('disabled', false),
      },
      loading: {
        type: Boolean,
        default: () => boolean('loading', false),
      },
      buttonTitle: {
        type: String,
        default: () => text('buttonTitle', '...'),
      },
      scrollHeight: {
        type: String,
        default: () => text('scrollHeight', '128px'),
      },
    },
    methods: {
      ...StoryContextAny,
      onOpen() {
        action('onOpen')();
      },
      onClose() {
        action('onClose')();
      },
      onItemClick(item: IAppDropdownItem) {
        action('onItemClick')(item);
      },
    },
    render() {
      return (
        <AppDropdown
          items={this.items}
          buttonTitle={this.buttonTitle}
          isLoading={this.loading}
          disabled={this.disabled}
          scrollHeight={this.scrollHeight}
          onOpen={this.onOpen}
          onClose={this.onClose}
          onItemClick={this.onItemClick}
        />
      );
    },
  }));
