import './AppDropdown.scss';
import { Component, Prop, Vue } from 'nuxt-property-decorator';
import type { VNode } from 'vue';
import { emitOn } from 'vue-tsx-support';
import {
  IAppDropdownEvents,
  IAppDropdownItem,
  IAppDropdownStates,
} from '~/components/ui/AppDropdown/AppDropdown.types';
import type { IElemClasses, IElemStyles } from '~/types/global-types';
import AppScrollbar from '~/components/ui/AppScrollbar/AppScrollbar';
import type { IAppScrollbarOptions } from '~/components/ui/AppScrollbar/AppScrollbar.types';
import Loader from '~/components/common/Loader/Loader';
import { LoaderSpinnerSizes } from '~/components/common/Loader/Loader.types';
import type { WEvents, WProps } from '~/types/tsx-element-types';

@Component
export default class AppDropdown extends Vue {
  public _tsx!: WProps<this, 'disabled' | 'buttonTitle' | 'isLoading' | 'scrollHeight'> &
    WEvents<IAppDropdownEvents>;

  @Prop({ type: Array, default: () => [], required: true })
  public readonly items!: IAppDropdownItem[];

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly disabled!: boolean;

  @Prop({ type: Boolean, default: () => false, required: false })
  public readonly isLoading!: boolean;

  @Prop({ default: () => '', required: false })
  public readonly buttonTitle!: string;

  @Prop({ default: () => '200px', required: false })
  public readonly scrollHeight!: string;

  private emitItemClick(item: IAppDropdownItem): void {
    if (this.disabled) return;
    emitOn(this, 'onItemClick', item);
  }

  private emitOpen(): void {
    if (this.disabled) return;
    emitOn(this, 'onOpen');
  }

  private emitClose(): void {
    if (this.disabled) return;
    emitOn(this, 'onClose');
  }

  private state: IAppDropdownStates = IAppDropdownStates.closed;
  private scrollOptions: IAppScrollbarOptions = {
    wheelPropagation: false,
  };

  private get showItems(): boolean {
    return this.state === IAppDropdownStates.opened && !this.isLoading;
  }

  private get scrollStyles(): IElemStyles {
    return {
      height: this.scrollHeight,
    };
  }

  private open(): void {
    if (this.state === IAppDropdownStates.opened) return;
    if (this.isLoading) return;

    this.state = IAppDropdownStates.opened;
    this.emitOpen();
  }

  private close(): void {
    if (this.state === IAppDropdownStates.closed) return;

    this.state = IAppDropdownStates.closed;
    this.emitClose();
  }

  private buildItemClasses(item: IAppDropdownItem): IElemClasses {
    return {
      class: {
        'dropdown__item--disabled': item.disabled || false,
        'dropdown__item--selected': item.selected || false,
      },
    };
  }

  private onHeadBtnClick(): void {
    if (this.disabled) return;

    this.state === IAppDropdownStates.closed ? this.open() : this.close();
  }

  private onItemClick(item: IAppDropdownItem): void {
    if (item.disabled) return;
    this.emitItemClick(item);
    this.close();
  }

  private render(): VNode {
    return (
      <div class="dropdown">
        <button
          disabled={this.disabled}
          onClick={this.onHeadBtnClick}
          v-click-outside={this.close}
          class="dropdown__button"
        >
          {this.isLoading ? (
            <Loader spinnerSize={LoaderSpinnerSizes.small} noCatchFocus showOverlay={false} />
          ) : (
            this.buttonTitle
          )}
        </button>
        <div class="dropdown__list">
          {this.showItems ? (
            <AppScrollbar options={this.scrollOptions} style={this.scrollStyles} class="dropdown__scroll">
              {this.items.map(item => (
                <button
                  class="dropdown__item"
                  disabled={item.disabled}
                  {...this.buildItemClasses(item)}
                  onClick={() => this.onItemClick(item)}
                >
                  {item.title}
                </button>
              ))}
            </AppScrollbar>
          ) : null}
        </div>
      </div>
    );
  }
}
