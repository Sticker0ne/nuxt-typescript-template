import fs from 'fs';
import dotenv from 'dotenv';

export const buildDotenvConfig = (): Record<string, string> => {
  createEnvDevIfNecessary();

  const baseEnv = dotenv.parse(fs.readFileSync('./env/.env.base'));
  const devEnv = dotenv.parse(fs.readFileSync('./env/.env.dev'));
  const prodEnv = dotenv.parse(fs.readFileSync('./env/.env.prod'));

  const nodeEnv = process.env.NODE_ENV || 'development';

  let resultEnv = baseEnv;
  resultEnv = nodeEnv === 'production' ? { ...resultEnv, ...prodEnv } : resultEnv;
  resultEnv = nodeEnv === 'development' ? { ...resultEnv, ...devEnv } : resultEnv;

  /* eslint-disable-next-line */
  console.info(`Dotenv config was generated in ${nodeEnv} mode\n\n${JSON.stringify(resultEnv, null, 4)}`);

  return resultEnv;
};

const createEnvDevIfNecessary = (): void => {
  if (fs.existsSync('./env/.env.dev')) {
    return;
  }

  const filePayload = '#DEVELOPMENT LOCAL ENVIRONMENTS OVERRIDING\n\n';
  fs.writeFileSync('./env/.env.dev', filePayload);
};
