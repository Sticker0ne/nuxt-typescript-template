export enum SVGNames {
  cross = 'cross',
  arrow = 'arrow',
  arrowSolid = 'arrowSolid',
  plus = 'plus',
  download = 'download',
  edit = 'edit',
  search = 'search',
}
