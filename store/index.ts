import { Getters, Actions, Module, Mutations } from 'vuex-smart-module';
import { UserModule } from '~/store/modules/user/user.store';

class MainState {}

class MainGetters extends Getters<MainState> {}

class MainMutations extends Mutations<MainState> {}

class MainActions extends Actions<MainState, MainGetters, MainMutations, MainActions> {}

export const mainModule = new Module({
  state: MainState,
  getters: MainGetters,
  mutations: MainMutations,
  actions: MainActions,
  modules: { UserModule },
});

export const { state, getters, mutations, actions, modules, plugins } = mainModule.getStoreOptions();
