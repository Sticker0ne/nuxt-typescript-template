import * as t from 'io-ts';
import { optional, validatorFromEnum } from '~/utils/StoreUtils';

export enum UserGenders {
  maleKey = 'МУЖЧИНА',
  femaleKey = 'ЖЕНЩИНА',
}

export const UserLegsV = t.type({ left: t.boolean, right: t.boolean });
export type UserLegs = t.TypeOf<typeof UserLegsV>;

const UserV = t.type(
  {
    id: t.string,
    name: t.string,
    gender: validatorFromEnum(UserGenders, 'UserGenders'),
    age: t.number,
    isSuperman: t.boolean,
    legs: optional(UserLegsV),
  },
  'User',
);
export type User = t.TypeOf<typeof UserV>;

export const UserListV = t.array(UserV, 'UserList');
export type UserList = t.TypeOf<typeof UserListV>;
