import { Getters, Actions, Module, Mutations } from 'vuex-smart-module';
import { Store } from 'vuex';
import { UserList, UserListV } from '~/store/modules/user/user.types';
import { decodeToPromise } from '~/utils/StoreUtils';

class UserState {
  public users: UserList = [];
}

class UserGetters extends Getters<UserState> {}

class UserMutations extends Mutations<UserState> {
  public setUsers(payload: UserList): void {
    this.state.users = payload;
  }
}

class UserActions extends Actions<UserState, UserGetters, UserMutations, UserActions> {
  private store!: Store<any>;

  public $init(store: Store<any>): void {
    this.store = store;
  }

  public async getUsers(): Promise<UserList> {
    const userListRaw = await this.store.$axios.$get('/users');
    const userList = await decodeToPromise(UserListV, userListRaw, 'store.getUsers()');
    this.mutations.setUsers(userList);
    return userList;
  }
}

export const UserModule = new Module({
  state: UserState,
  getters: UserGetters,
  mutations: UserMutations,
  actions: UserActions,
});
