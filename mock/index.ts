/* eslint-disable @typescript-eslint/no-var-requires */

const { nanoid } = require('nanoid');
const jsonServer = require('json-server');
const server = jsonServer.create();
const port = 2000;

const middlewares = jsonServer.defaults();
server.use(middlewares);

server.listen(port, () => {
  console.log('JSON Server is running on port ' + port);
});

function buildRouteName(name: string): string {
  return '/api' + name;
}

server.post(buildRouteName('/upload'), (request: any, response: any) => {
  response.status(200).jsonp({
    id: nanoid(),
    name: 'name.png',
    size: 4563,
    url: 'http://www.snowbd.ru/fs/a_desctop/1410_pic.jpg',
  });
});

server.get(buildRouteName('/users'), (req: any, res: any) => {
  res.status(200).jsonp([
    {
      id: '1-id',
      name: 'sergo',
      gender: 'МУЖЧИНА',
      age: 23,
      isSuperman: false,
    },
    {
      id: '2-id',
      name: 'julia',
      gender: 'ЖЕНЩИНА',
      age: 18,
      isSuperman: false,
    },
    {
      id: '3-id',
      name: 'superman',
      gender: 'МУЖЧИНА',
      age: 122,
      isSuperman: true,
      legs: {
        left: true,
        right: false,
      },
    },
  ]);
});
