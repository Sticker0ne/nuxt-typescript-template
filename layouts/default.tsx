import { Component, Vue } from 'nuxt-property-decorator';
import { VNode } from 'vue';
import { EventPrintMessage, IEventPrintMessage } from '~/utils/EventBusUtils';
import TheNavigator from '~/components/global/TheNavigator/TheNavigator';
import TheHeader from '~/components/global/TheHeader/TheHeader';
import TheFooter from '~/components/global/TheFooter/TheFooter';

@Component({})
export default class DefaultLayout extends Vue {
  private onEventPrintMessage(payload: IEventPrintMessage): void {
    /* eslint-disable-next-line */
    console.log('%c%s', `color: ${payload.color};`, payload.message);
  }

  private mounted(): void {
    this.$startWatchWidth();
    EventPrintMessage.$subscribe(this.onEventPrintMessage);
  }

  private beforeDestroy(): void {
    EventPrintMessage.$unsubscribe(this.onEventPrintMessage);
  }

  private render(): VNode {
    return (
      <div>
        <TheHeader />
        <main class="main">
          <div>default-layout</div>
          <TheNavigator />
          <div class="container">
            <nuxt />
          </div>
        </main>
        <TheFooter />
      </div>
    );
  }
}
